import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

import org.bson.types.ObjectId;
import org.json.JSONArray;
import org.json.JSONObject;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

public class DBContainerAccount {
	private boolean mConnected;
	private DB mDB;

	public DBContainerAccount() {
	}

	private boolean checkConnected() {
		return mConnected;
	}

	public void initialize( DB db )  {
		Logs.LogEvent("Initializing Account DB");
		mDB = db;
		mConnected = true;
	}
	
	String makeUniqueAlias( String id, String alias ) {
		DBCollection coll = mDB.getCollection("userconfig");
		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put("accountAlias", alias );
		DBCursor result = coll.find(searchQuery);
		if( result.hasNext() ) {
			DBObject myResult = result.next();
			if( result.hasNext() == false ) {
				if( id.length() > 0 ) {
					if( id.equals( myResult.get( "id" ).toString() ) ) {
						return alias;
					}
					else {
						return alias + "1";
					}
				}
				else {
					return alias + "1";
				}
			}
			else {
				searchQuery = new BasicDBObject();
				searchQuery.put("accountAlias", java.util.regex.Pattern.compile( alias ) );
				result = coll.find(searchQuery);
				return alias + (1+result.length());
			}
		}
		else {
			return alias;
		}
	}

	public boolean userAliasIsUnique( String id, String alias ) {
		DBCollection coll = mDB.getCollection("userconfig");
		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put("accountAlias", alias );
		DBCursor result = coll.find(searchQuery);
		if( result.length() > 1 ) {
			Logs.LogEvent( "Warning, multiple instances detected of " + alias );
			return false;
		}
		else if( result.length() == 1 ) {
			if( id.length() > 0 ) {
				DBObject myResult = result.next();
				if( id.equals( myResult.get( "id" ).toString() ) ) {
					return true;
				}
				else {
					return false;
				}
			}
			else {
				return false;
			}
		}
		return true;
	}
	
	public boolean upgradeAnonymousAccount( String accountToken, String accountId, String accountPassword, String accountAlias, String accountDOB, String accountNewsLetter ) {
		DBCollection coll = mDB.getCollection("accounts");
		BasicDBObject searchQuery = new BasicDBObject();
		try {
			searchQuery.put("_id", new ObjectId( accountToken ));
			DBObject result = coll.findOne(searchQuery);
			if( result != null ) {
				if( result.get("privs").toString().equals( "anon" ) ) {
					result.put( "anonid",  result.get("accountId").toString() );
					result.put( "accountId", accountId );
					byte[] mySalt = ContainerConfig.getProperty( "passwordhash" ).getBytes();
					byte[] myHash = ContainerPassword.hash( (accountId + accountPassword).toCharArray(),  mySalt );
					String myHashString = ContainerPassword.intoString( myHash );
					result.put( "accountPassword", myHashString );
					result.put( "privs", "new" );
					result.put( "upgradedDate", new Date() );
					coll.save( result );
					ContainerMail.sendMail( accountId, "1", accountToken );
					DBCollection usercoll = mDB.getCollection("userconfig");
					searchQuery = new BasicDBObject();
					searchQuery.put("id", accountToken );
					result = usercoll.findOne(searchQuery);
					accountAlias = makeUniqueAlias( accountToken, accountAlias );
					if( result != null ) {
						Logs.LogEvent("Updating anon entry for " + accountToken );
						Logs.LogEvent("Found " + result.toString() );
						result.removeField( "accountAlias" );
						result.put( "accountAlias", accountAlias );
						result.removeField( "accountdob" );
						result.put( "accountdob", accountDOB );
						result.removeField( "accountnewsletter" );
						result.put( "accountnewsletter", accountNewsLetter );
						Logs.LogEvent("Updated to " + result.toString() );
						usercoll.save( result );	
					}
					else {
						BasicDBObject document = new BasicDBObject();
						document = new BasicDBObject();
						document.put("id", accountToken);
						document.put( "accountAlias", accountAlias );
						document.put( "accountdob", accountDOB );
						document.put( "accountnewsletter", accountNewsLetter );
						usercoll.insert(document);
					}
					return true;
				}
			}
		}
		catch( Exception e ) {
			e.printStackTrace();
		}
		return false;
	}
	
	JSONObject findWildCardUserInfo( String privileges, JSONObject detail )
	{
		JSONObject replyData = new JSONObject();
		JSONObject userData;
		Iterator<String> entries;

		DBCollection coll = mDB.getCollection("userconfig");
		DBCursor results = coll.find( science.db.getQuery( detail.getString("field"), detail.getString("value") ) );
		JSONArray userInfo = new JSONArray();
		while (results.hasNext()) {
			DBObject result = results.next();
			userData = new JSONObject();
			entries = result.keySet().iterator();
			while( entries.hasNext() ) {
				String entry = entries.next().toString();
				if( ( entry.equals( "_id" ) == false ) &&
						( entry.equals( "points" ) == false ) ) {
					userData.put( entry, result.get( entry ) );
				}
			}
			userInfo.put( userData );
		}
		replyData.put( "users", userInfo );
		return replyData;
	}
		
	JSONObject searchUserInfo( String accountToken, String privileges, JSONObject detail ) 
	{
		JSONObject replyData = new JSONObject();
		Iterator<String> entries;
		if( privileges.equals("admin") == false ) {
			replyData = findWildCardUserInfo( privileges, detail );
		}
		else {
			if( detail.getString("field").equals("accountId") ) {
				DBCollection coll = mDB.getCollection("accounts");
				BasicDBObject searchQuery = new BasicDBObject();
				try {
					searchQuery.put("accountId", detail.getString( "value") );
					DBObject result = coll.findOne(searchQuery);
					if( result != null ) {
						entries = result.keySet().iterator();
						while( entries.hasNext() ) {
							String entry = entries.next().toString();
							if( entry.equals( "_id" ) == false && 
									entry.equals( "accountPassword" ) == false ) {
								replyData.put( entry, result.get( entry ) );
							}
						}
						coll = mDB.getCollection("userconfig");
						searchQuery = new BasicDBObject();
						searchQuery.put("id", result.get("_id").toString());
						result = coll.findOne(searchQuery);
						if( result != null ) {
							entries = result.keySet().iterator();
							while( entries.hasNext() ) {
								String entry = entries.next().toString();
								if( entry.equals( "_id" ) == false ) {
									replyData.put( entry, result.get( entry ) );
								}
							}
						}						
					}
				}
				catch( Exception e ) {
					e.printStackTrace();
				}
			}
			else {
				replyData = findWildCardUserInfo( privileges, detail );
			}			
		}
		return replyData;
	}

	public boolean accountExists( String accountId ) {
		if( checkConnected() ) {
			DBCollection coll = mDB.getCollection("accounts");
			BasicDBObject searchQuery = new BasicDBObject();
			try {
				searchQuery.put("accountId", accountId );
				DBObject result = coll.findOne(searchQuery);
				if( result != null ) {
					return true;
				}
				return false;
			}
			catch( Exception e ) {
				return false;
			}
		}
		return false;
	}
	
	public String getAccountPrivs( String accountToken ) {
		DBCollection coll = mDB.getCollection("accounts");
		BasicDBObject searchQuery = new BasicDBObject();
		try {
			searchQuery.put("_id", new ObjectId( accountToken ));
			DBObject result = coll.findOne(searchQuery);
			if( result != null ) {
				return result.get("privs").toString();
			}
		}
		catch( Exception e ) {
			return "";
		}
		return "";
	}
	
	public boolean validateAccount( String accountToken ) {
		if( checkConnected() ) {
			DBCollection coll = mDB.getCollection("accounts");
			BasicDBObject searchQuery = new BasicDBObject();
			try {
				searchQuery.put("_id", new ObjectId( accountToken ));
				DBObject result = coll.findOne(searchQuery);
				if( result != null ) {
					if( result.get("privs").toString().equals( "new" ) ) {
						result.put( "privs", "user" );
						coll.save( result );
						return true;
					}
				}
				return false;
			}
			catch( Exception e ) {
				return false;
			}
		}
		return false;		
	}
	
	public boolean validateUserAccountToken( String accountToken ) {
		if( checkConnected() ) {
			DBCollection coll = mDB.getCollection("accounts");
			BasicDBObject searchQuery = new BasicDBObject();
			try {
				searchQuery.put("_id", new ObjectId( accountToken ));
				DBObject result = coll.findOne(searchQuery);
				if( result != null ) {
					if( result.get("privs").toString().equals( "anon" ) ) {
						return true;
					}
					if( result.get("privs").toString().equals( "user" ) ) {
						return true;
					}
					if( result.get("privs").toString().equals( "dev" ) ) {
						return true;
					}
					if( result.get("privs").toString().equals( "admin" ) ) {
						return true;
					}
				}
			}
			catch( Exception e ) {
				e.printStackTrace();
			}
		}
		return false;
	}

	public boolean validateAccountTokenByType( String accountToken, String privilege ) {
		if( checkConnected() ) {
			DBCollection coll = mDB.getCollection("accounts");
			BasicDBObject searchQuery = new BasicDBObject();
			try {
				searchQuery.put("_id", new ObjectId( accountToken ));
				DBObject result = coll.findOne(searchQuery);
				if( result != null ) {
					if( privilege.equals( "anon" ) ) {
						if( result.get("privs").toString().equals( "anon" ) ) {
							return true;
						}
					}
					else if( privilege.equals( "user" ) ) {
						if( result.get("privs").toString().equals( "user" ) ) {
							return true;
						}
						if( result.get("privs").toString().equals( "dev" ) ) {
							return true;
						}
						if( result.get("privs").toString().equals( "admin" ) ) {
							return true;
						}
					}
					else if( privilege.equals( "dev" ) ) {
						if( result.get("privs").toString().equals( "dev" ) ) {
							return true;
						}
						if( result.get("privs").toString().equals( "admin" ) ) {
							return true;
						}
					}
					else if( privilege.equals( "admin" ) ) {
						if( result.get("privs").toString().equals( "admin" ) ) {
							return true;
						}
					}
				}
				return false;
			}
			catch( Exception e ) {
				return false;
			}
		}
		return false;		
	}
	
	public String getAccountIdFromToken( String accountToken ) {
		String response = "";
		DBCollection coll = mDB.getCollection("accounts");
		BasicDBObject searchQuery = new BasicDBObject();
		try {
			searchQuery.put("_id", new ObjectId( accountToken ));
			DBObject result = coll.findOne(searchQuery);
			if( result != null ) {
				response = result.get("accountId").toString();				
			}
		}
		catch( Exception e ) {
			return response;
		}
		return response;
	}
	
	public String getAccountAliasFromToken( String accountToken ) {
		String response = "";
		DBCollection coll = mDB.getCollection("userconfig");
		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put("id", accountToken);
		DBObject result = coll.findOne(searchQuery);
		if( result != null ) {
			if( result.keySet().contains("accountAlias") ) {
				response = result.get("accountAlias").toString();
			}		
			if( response.length() == 0 ) {
				coll = mDB.getCollection("accounts");
				searchQuery = new BasicDBObject();
				searchQuery.put("_id", new ObjectId( accountToken ));
				result = coll.findOne(searchQuery);
				if( result != null ) {
					response = result.get("accountId").toString();
					int index = response.indexOf('@');
					if( index > 0 ) {
						response = response.substring(0,index);
					}
				}
			}
		}
		return response;
	}

	public String createNewAnonAccount()
	{
		DBCollection coll = mDB.getCollection("anonaccounts");
		Date myDate = new Date();
		BasicDBObject newAnon = new BasicDBObject();
		long size = coll.count();
		Calendar cal = Calendar.getInstance();
		cal.setTime(myDate);
		String anonId = "anon" + cal.get(Calendar.MONTH) + "x" + cal.get(Calendar.DAY_OF_MONTH) + "x" + size;
		newAnon.put("accountId", anonId );
		coll.save(newAnon);
		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put("accountId", anonId );
		DBObject result = coll.findOne(searchQuery);
		if( result != null ) {
			return anonId;
		}
		return null;
	}
	
	public String createAnonAccountToken() {
		if( checkConnected() ) {
			String anonId = createNewAnonAccount();
			if( anonId != null ) {
				DBCollection coll = mDB.getCollection("accounts");
				BasicDBObject document = new BasicDBObject();
				document.put("accountId", anonId);
				document.put("privs", "anon");
				document.put("createdDate", new Date());
				coll.insert(document);
				BasicDBObject searchQuery = new BasicDBObject();
				searchQuery.put("accountId", anonId);
				DBObject result = coll.findOne(searchQuery);
				if( result != null ) {
					String id = result.get("_id").toString();
					coll = mDB.getCollection("userconfig");
					document = new BasicDBObject();
					document.put("id", id);
					document.put( "accountAlias", "anonymous" );
					document.put( "accountdob", "1-1-1998" );
					document.put( "accountnewsletter", "0" );
					coll.insert(document);
					return id;
				}
			}
			return null;
		}
		else {
			Logs.LogEvent( "MongoDB not connected." );
			return null;
		}
	}
	
	public String createAccountToken( String accountId, String accountPassword, String accountAlias, String accountDOB, String accountNewsLetter ) {
		if( checkConnected() ) {
			DBCollection coll = mDB.getCollection("accounts");
			
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put("accountId", accountId);
			DBObject result = coll.findOne(searchQuery);
			if( userAliasIsUnique( "", accountAlias ) ) {
				if( result != null ) {
					return result.get("_id").toString();
				}
				else {
					BasicDBObject document = new BasicDBObject();
					document.put("accountId", accountId);
					byte[] mySalt = ContainerConfig.getProperty( "passwordhash" ).getBytes();
					byte[] myHash = ContainerPassword.hash( (accountId + accountPassword).toCharArray(),  mySalt );
					String myHashString = ContainerPassword.intoString( myHash );
					document.put("accountPassword", myHashString);
					document.put("privs", "new");
					document.put("createdDate", new Date());
					coll.insert(document);
					result = coll.findOne(searchQuery);
					if( result != null ) {
						String id = result.get("_id").toString();
						ContainerMail.sendMail( accountId, "1", id );
						coll = mDB.getCollection("userconfig");
						document = new BasicDBObject();
						document.put("id", id);
						document.put( "accountAlias", accountAlias );
						document.put( "accountdob", accountDOB );
						document.put( "accountnewsletter", accountNewsLetter );
						coll.insert(document);
						return id;
					}
				}
			}
			else {
				return "Account Alias Exists";
			}
			return null;
		}
		else {
			Logs.LogEvent( "MongoDB not connected." );
			return null;
		}
	}

	public String resendAccountToken( String accountId, String accountPassword ) {
		if( checkConnected() ) {
			DBCollection coll = mDB.getCollection("accounts");
			
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put("accountId", accountId);
			DBObject result = coll.findOne(searchQuery);
			if( result != null && result.keySet().contains( "accountId") && result.keySet().contains("accountPassword") && result.keySet().contains("privs") ) {
				if( result.get("accountId" ).toString().equals( accountId ) ) {
					byte[] mySalt = ContainerConfig.getProperty( "passwordhash" ).getBytes();
					byte[] myHash = ContainerPassword.hash( (accountId + accountPassword).toCharArray(),  mySalt );
					String myHashString = ContainerPassword.intoString( myHash );
					if( result.get("accountPassword" ).toString().equals( myHashString ) ) {
						if( result.get("privs").toString().equals( "new" ) ) {
							String id = result.get("_id").toString();
							ContainerMail.sendMail( accountId, "1", id );
							return id;
						}
					}
				}
			}
			return null;
		}
		else {
			Logs.LogEvent( "MongoDB not connected." );
			return null;
		}
	}

	public boolean lostPasswordMarkAccount( String accountToken ) {
		if( checkConnected() ) {
			DBCollection coll = mDB.getCollection("accounts");
			
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put("_id", new ObjectId( accountToken ));
			DBObject result = coll.findOne(searchQuery);
			if( result != null && result.keySet().contains("privs") && result.keySet().contains("lostpassword") ) {
				if( result.get("privs").toString().equals( "user" ) || 
					result.get("privs").toString().equals( "admin" )|| 
					result.get("privs").toString().equals( "dev" ) ) {
					if( result.get("lostpassword").toString().equals("1") ) {
						result.put("lostpassword", "2");
						coll.save(result);
						return true;
					}
				}
			}
			return false;
		}
		else {
			Logs.LogEvent( "MongoDB not connected." );
			return false;
		}
	}
	
	public String lostPasswordGetToken( String accountId ) {
		if( checkConnected() ) {
			DBCollection coll = mDB.getCollection("accounts");
			
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put("accountId", accountId);
			DBObject result = coll.findOne(searchQuery);
			if( result != null && result.keySet().contains( "accountId") && result.keySet().contains("privs") ) {
				if( result.get("accountId" ).toString().equals( accountId ) ) {
					if( result.get("privs").toString().equals( "user" ) || 
						result.get("privs").toString().equals( "admin" )|| 
						result.get("privs").toString().equals( "dev" ) ) {
						String id = result.get("_id").toString();
						ContainerMail.sendMail( accountId, "2", id );
						result.put("lostpassword", "1");
						coll.save(result);
						return id;
					}
				}
			}
			return null;
		}
		else {
			Logs.LogEvent( "MongoDB not connected." );
			return null;
		}
	}

	public boolean validateDeviceToken( String deviceToken ) {
		if( checkConnected() ) {
			DBCollection coll = mDB.getCollection("devices");
			BasicDBObject searchQuery = new BasicDBObject();
			try {
				searchQuery.put("_id", new ObjectId( deviceToken ));
				DBObject result = coll.findOne(searchQuery);
				if( result != null ) {
					return true;
				}
				return false;
			}
			catch( Exception e ) {
				Logs.LogEvent( "Error " + e.toString() );
				return false;
			}
		}
		return false;		
	}

	public String createDeviceToken( String deviceId ) {
		if( checkConnected() ) {
			DBCollection coll = mDB.getCollection("devices");
			
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put("duid", deviceId);
			DBObject result = coll.findOne(searchQuery);
			if( result != null ) {
				return result.get("_id").toString();
			}
			else {
				BasicDBObject document = new BasicDBObject();
				document.put("duid", deviceId);
				document.put("createdDate", new Date());
				coll.insert(document);
				result = coll.findOne(searchQuery);
				if( result != null ) {
					return result.get("_id").toString();
				}
			}
			return null;
		}
		else {
			Logs.LogEvent( "MongoDB not connected." );
			return null;
		}
	}

	public JSONObject loginUserAnon( String token ) {		
		JSONObject response = new JSONObject();
		DBCollection coll = mDB.getCollection("accounts");
		
		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put("_id", new ObjectId( token ) );
		DBObject result = coll.findOne(searchQuery);
		if( result != null ) {
			String privs = result.get("privs").toString();
			if( privs.equals( "anon" ) ) {
				response.put( "token", result.get("_id").toString() );
				response.put( "privs", result.get("privs").toString() );
				int logins = 0;
				if( result.keySet().contains("logins") ) {
					logins = Integer.parseInt( result.get("logins").toString() );
				}
				result.put( "logins", logins + 1 );
				result.put("lastLogin", new Date());
				coll.save( result );
			}
		}
		return response;
	}
	
	public void validateUserData( String id, String accountId ) {
		String accountAlias;
		accountAlias = accountId;
		int index = accountAlias.indexOf('@');
		if( index > 0 ) {
			accountAlias = accountAlias.substring(0,index);
		}
		accountAlias = makeUniqueAlias( id, accountAlias );
		DBCollection coll = mDB.getCollection("userconfig");
		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put("id", id );
		DBObject result = coll.findOne(searchQuery);
		Logs.LogEvent( "account is missing user config " + accountId + " getting new alias " + accountAlias );
		if( result != null ) {
			boolean changed = false;
			if( result.keySet().contains("accountAlias") == false ) {
				result.put( "accountAlias", accountAlias );
				changed = true;
			}
			if( result.keySet().contains("accountdob") == false ) {
				result.put( "accountdob", "1-1-1998" );
				changed = true;
			}
			if( result.keySet().contains("accountnewletter") == false ) {
				result.put( "accountnewsletter", "0" );
				changed = true;
			}
			if( changed == true ) {
				coll.save( result );
			}
		}
		else {
			BasicDBObject document = new BasicDBObject();
			document.put("id", id);
			document.put( "accountAlias", accountAlias );
			document.put( "accountdob", "1-1-1998" );
			document.put( "accountnewsletter", "0" );
			coll.insert(document);			
		}
		
	}
	
	public JSONObject loginUserWeb( String accountId, String accountPassword ) {
		if( checkConnected() ) {
			DBCollection coll = mDB.getCollection("accounts");
			
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put("accountId", accountId);
			DBObject result = coll.findOne(searchQuery);
			if( result != null ) {
				String privs = result.get("privs").toString();
				JSONObject response = new JSONObject();
				if( privs.equals( "user" ) ||
						privs.equals( "admin" ) ||
						privs.equals( "dev" ) ) {						
					byte[] mySalt = ContainerConfig.getProperty( "passwordhash" ).getBytes();
					if( ContainerPassword.isExpectedPassword( (accountId+accountPassword).toCharArray(), mySalt, ContainerPassword.intoBytes( result.get( "accountPassword").toString() ) ) ) {
						validateUserData( result.get("_id").toString(), accountId );
						response.put( "token", result.get("_id").toString() );
						response.put( "privs", result.get("privs").toString() );
						int logins = 0;
						if( result.keySet().contains("logins") ) {
							logins = Integer.parseInt( result.get("logins").toString() );
						}
						result.put( "logins", logins + 1 );
						result.put("lastLogin", new Date());
						if( result.keySet().contains( "lostpassword") ) {
							result.removeField( "lostpassword" );
						}
						coll.save( result );
						return response;
					}
				}
				else if( privs.equals( "banned" ) ) {
					response.put( "token", result.get("_id").toString() );
					response.put( "privs", result.get("privs").toString() );
					return response;
				}
				else if( privs.equals( "new" ) ) {
					response.put( "privs", result.get("privs").toString() );
					return response;
				}
				Logs.LogEvent( "Invalid Password - " + accountId );
				return null;
			}
			else {
				Logs.LogEvent( "Invalid UserName - " + accountId );
			}
			return null;
		}
		else {
			Logs.LogEvent( "MongoDB not connected." );
			return null;
		}
	}

}
