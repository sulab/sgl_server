import org.json.JSONObject;
import org.vertx.java.core.buffer.Buffer;
import org.vertx.java.core.http.HttpServerRequest;

public class MessageSGLAccountUpdate {
	private String validateMessage( JSONObject jsonRequestBody ) {
		if( jsonRequestBody.has("accounttoken") ) {
			if( jsonRequestBody.has("params") ) {
				String configType = jsonRequestBody.getString( "params" );
				String[] fields = ContainerParams.getSettingFields( configType );
				for( int i = 0; i < fields.length; i++ ) {
					if( jsonRequestBody.has( fields[i] ) == false ) {
						return "Missing element " + fields[i];
					}
				}
				return null;					
			}
			else {
				return "Missing params";
			}
			
		}
		else {
			return "Missing account token";
		}
	}

	public MessageSGLAccountUpdate( HttpServerRequest req, Buffer body )  {
		String errorInfo = null;
		try { 
			JSONObject jsonRequest = new JSONObject( body.toString() ); 
			JSONObject jsonReplyBody = new JSONObject();
			JSONObject jsonRequestBody = jsonRequest.getJSONObject( "body" );
			if( jsonRequestBody.has( "_t") && jsonRequestBody.getString( "_t" ).equals( "sglaccount.change" ) ) {
				errorInfo = validateMessage( jsonRequestBody );
				String developerId = "";
				String developerSecret = "";
				if( jsonRequestBody.has("developerid") ) {
					developerId = jsonRequestBody.getString( "developerid" );
				}
				else {
					errorInfo = "Missing developer id";					
				}
				if( jsonRequestBody.has("developersecret") ) {
					developerSecret = jsonRequestBody.getString( "developersecret" );
				}
				else {
					errorInfo = "Missing developer id";					
				}
				if( science.db.validateDeveloperSecret( developerId, developerSecret ) ) {
					if( errorInfo == null ) {
						String configType = jsonRequestBody.getString( "params" );
						String privilege = ContainerParams.getPrivileges( configType );
						String accountToken = jsonRequestBody.getString( "accounttoken" );
						if( science.db.accounts.validateAccountTokenByType( accountToken, privilege ) == true ) {
							// String[] properties = { "accounttoken" };
							// science.analytics.trackEvent( accountToken, "Write Settings", timeStamp, FieldMapper.getFields(jsonRequestBody, properties) );
							errorInfo = science.db.settings.writeSettings( accountToken, configType, jsonRequestBody );
							if( errorInfo == null ) {
								jsonReplyBody.put( "_t", "sglaccount.changed" );
								jsonReplyBody.put( "write", "1" );
							}
						}
						else {
							// if the token does not validate just say we wrote it to obfuscate replay attacks
							jsonReplyBody.put( "_t", "params.set" );
							jsonReplyBody.put( "write", "1" );
						}
					}
				}
				else {
					errorInfo = "verify your developer credentials";
				}
			}
			else {
				errorInfo = "Missing _t";
			}
			if( errorInfo != null && errorInfo.length() > 0 ) {
				req.response().setStatusCode(400);
				Logs.LogEvent( errorInfo );
				jsonReplyBody.put( "_t", "sglaccount.change_error" );
				jsonReplyBody.put( "info", errorInfo );
			}
			JSONObject jsonReply = new JSONObject().put( "_t", "msg" );
			JSONObject jsonHeader = jsonRequest.getJSONObject( "header" );
			jsonReply.put( "header", jsonHeader );
			jsonReply.put( "body", jsonReplyBody );
			Logs.LogEvent( "JSON Reply is " + jsonReply.toString() );
			req.response().headers().set("Content-Type", "application/json; charset=UTF-8");
			req.response().end(jsonReply.toString());
		}
		catch( org.json.JSONException e ) {
			e.printStackTrace();
			Logs.LogEvent( "JSON error is " + errorInfo );
			req.response().setStatusCode(400);
			req.response().end( "{ jsonfailure: true }");
		}
	}

}
