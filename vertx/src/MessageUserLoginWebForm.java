import org.json.JSONObject;
import org.vertx.java.core.http.HttpServerRequest;

public class MessageUserLoginWebForm {
	public MessageUserLoginWebForm( HttpServerRequest req, String accountId, String accountPassword )  {
		Logs.LogEvent("Login Web User" );
		String errorInfo = null;
		try { 
			JSONObject jsonReplyBody = new JSONObject();
			JSONObject jsonRequestBody = new JSONObject();
			jsonRequestBody.put( "accountid", accountId );
			String timeStamp = "";
			if( accountId != null && accountId.length() > 0 ) {
				Logs.LogEvent("Login Web User exists" );
				if( science.db.accounts.accountExists( accountId ) == true ) {
					JSONObject accountToken = science.db.accounts.loginUserWeb( accountId, accountPassword );
					if( accountToken != null && accountToken.length() > 0 ) {
						String[] properties = { "accountid" };
						science.analytics.trackEvent( accountId, "Web Login", timeStamp, FieldMapper.getFields(jsonRequestBody, properties) );
						jsonReplyBody.put( "_t", "webuser.login" );
						jsonReplyBody.put( "account", accountToken );
					}
					else {
						errorInfo = "DB Failure";
					}
				}
				else {
					errorInfo = "DB Failure";
				}
			}
			if( errorInfo != null && errorInfo.length() > 0 ) {
				req.response().setStatusCode(400);
				Logs.LogEvent( errorInfo );
				jsonReplyBody.put( "_t", "webuser.login_error" );
				jsonReplyBody.put( "info", errorInfo );
			}
			JSONObject jsonReply = new JSONObject().put( "_t", "msg" );
			JSONObject jsonHeader = new JSONObject();
			jsonHeader.put( "_t", "hdr" );
			jsonHeader.put( "cid", "0" );
			jsonReply.put( "header", jsonHeader );
			jsonReply.put( "body", jsonReplyBody );
			Logs.LogEvent( "JSON Reply is " + jsonReply.toString() );
			req.response().headers().set("Content-Type", "application/json; charset=UTF-8");
			req.response().end(jsonReply.toString());
		}
		catch( org.json.JSONException e ) {
			e.printStackTrace();
			Logs.LogEvent( "JSON error is " + errorInfo );
			req.response().setStatusCode(400);
			req.response().headers().set("Content-Type", "application/json; charset=UTF-8");
			req.response().end( "{ jsonfailure: true }");
		}
	}

}
