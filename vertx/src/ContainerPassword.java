import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.nio.charset.Charset;
import java.security.MessageDigest;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import org.apache.commons.codec.binary.Base64;

public class ContainerPassword {
	private static final int ITERATIONS = 100;
	private static final int KEY_LENGTH = 128;

    public static String intoString(byte[] data) {
    	return Base64.encodeBase64String(data);
    }
    
    public static byte[] intoBytes( String data ) {
    	return Base64.decodeBase64(data);
    }
    
	public static byte[] hash(char[] password, byte[] salt ) {
		PBEKeySpec spec = new PBEKeySpec(password, salt, ITERATIONS, KEY_LENGTH);
		Arrays.fill(password, Character.MIN_VALUE);
		try {
			SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
			return skf.generateSecret(spec).getEncoded();
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			throw new AssertionError("Error while hashing a password: " + e.getMessage(), e);
		} finally {
			spec.clearPassword();
		}
	}	

	public static boolean isExpectedPassword(char[] password, byte[] salt, byte[] expectedHash) {
		byte[] pwdHash = hash(password, salt);
		Arrays.fill(password, Character.MIN_VALUE);
		if (pwdHash.length != expectedHash.length) return false;
		for (int i = 0; i < pwdHash.length; i++) {
			if (pwdHash[i] != expectedHash[i]) return false;
		}
		return true;
	}

	public static boolean compareSecrets( String developerSecret, String unSaltedSecret ) {
		return developerSecret.equals( unSaltedSecret );
/*		try {
			byte[] bytesOfMessage = unSaltedSecret.getBytes("UTF-8");

			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] thedigest = md.digest(bytesOfMessage);
			String digestResult = new String(thedigest,0,thedigest.length,Charset.forName("UTF-8"));
			if( digestResult.equals(developerSecret) == true ) {
				return true;
			}
		}
		catch( Exception e ) {
			e.printStackTrace();
		}
		return false;
*/
	}
}
