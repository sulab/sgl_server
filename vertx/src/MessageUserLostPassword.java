import org.json.JSONObject;
import org.vertx.java.core.buffer.Buffer;
import org.vertx.java.core.http.HttpServerRequest;

public class MessageUserLostPassword {
	public MessageUserLostPassword( HttpServerRequest req, Buffer body )  {
		Logs.LogEvent("Lost Web Password" );
		String errorInfo = null;
		try { 
			JSONObject jsonRequest = new JSONObject( body.toString() ); 
			JSONObject jsonReplyBody = new JSONObject();
			JSONObject jsonRequestBody = jsonRequest.getJSONObject( "body" );
			if( jsonRequestBody.has( "_t") && jsonRequestBody.getString( "_t" ).equals( "webuser.reset" ) ) {
				String accountId = "";
				if( jsonRequestBody.has("accountid") ) {
					accountId = jsonRequestBody.getString( "accountid" );
				}
				else {
					errorInfo = "Missing account id";					
				}
				if( accountId != null && accountId.length() > 0 ) {
					if( science.db.accounts.accountExists( accountId ) == true ) {
						String accountToken = science.db.accounts.lostPasswordGetToken( accountId );
						if( accountToken != null && accountToken.length() > 0 ) {
							jsonReplyBody.put( "_t", "webuser.resetted" );
							jsonReplyBody.put("privs", "user");
						}
						else {
							errorInfo = "DB Failure";
						}
					}
					else {
						errorInfo = "account does not exist";
					}
				}
				else {
					errorInfo = "account missing";
				}
			}
			else {
				errorInfo = "Missing _t";
			}
			if( errorInfo != null && errorInfo.length() > 0 ) {
				req.response().setStatusCode(400);
				Logs.LogEvent( errorInfo );
				jsonReplyBody.put( "_t", "webuser.reset_error" );
				jsonReplyBody.put( "info", errorInfo );
			}
			JSONObject jsonReply = new JSONObject().put( "_t", "msg" );
			JSONObject jsonHeader = jsonRequest.getJSONObject( "header" );
			jsonReply.put( "header", jsonHeader );
			jsonReply.put( "body", jsonReplyBody );
			Logs.LogEvent( "JSON Reply is " + jsonReply.toString() );
			req.response().end(jsonReply.toString());
		}
		catch( org.json.JSONException e ) {
			e.printStackTrace();
			Logs.LogEvent( "JSON error is " + errorInfo );
			req.response().setStatusCode(400);
			req.response().end( "{ jsonfailure: true }");
		}
	}

}
