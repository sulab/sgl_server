import org.vertx.java.core.http.HttpServerRequest;
import org.json.*;

public class MessagePingGet {
	
	public MessagePingGet ( HttpServerRequest req, String[] myArguments )  {
		try { 
			JSONObject jsonReplyBody = new JSONObject();
			jsonReplyBody.put( "_t", "science.ping" );
			JSONObject jsonReply = new JSONObject().put( "_t", "msg" );
			JSONObject jsonHeader = new JSONObject().put( "_t", "hdr" );
			jsonReply.put( "header", jsonHeader );
			jsonReply.put( "body", jsonReplyBody );
			Logs.LogEvent( "JSON Reply is " + jsonReply.toString() );
			req.response().headers().set("Content-Type", "application/json; charset=UTF-8");
			req.response().end(jsonReply.toString());
		}
		catch( org.json.JSONException e ) {
			e.printStackTrace();
			req.response().setStatusCode(400);
			req.response().end( "{ jsonfailure: true }");
		}
	}
}
