import org.json.JSONObject;
import org.vertx.java.core.buffer.Buffer;
import org.vertx.java.core.http.HttpServerRequest;

public class MessageItemInfo {
	private String validateMessage( JSONObject jsonRequestBody ) {
		if( jsonRequestBody.has("accounttoken") ) {
			if( jsonRequestBody.has("info") ) {
				if( jsonRequestBody.has("itemid") || jsonRequestBody.getString( "info" ).equals( "site" ) || jsonRequestBody.getString( "info" ).equals( "game" ) ) {
					return null;
				}
				else {
					if( jsonRequestBody.has("params") ) {
						return null;
					}
					else {
						return "Missing itemid";
					}
				}
			}
			else {
				return "Missing info";
			}
			
		}
		else {
			return "Missing account token";
		}
	}

	public MessageItemInfo( HttpServerRequest req, Buffer body )  {
		String errorInfo = null;
		try { 
			JSONObject jsonRequest = new JSONObject( body.toString() ); 
			JSONObject jsonReplyBody = new JSONObject();
			JSONObject jsonRequestBody = jsonRequest.getJSONObject( "body" );
			if( jsonRequestBody.has( "_t") && jsonRequestBody.getString( "_t" ).equals( "info.get" ) ) {
				String privileges = "";
				errorInfo = validateMessage( jsonRequestBody );
				if( errorInfo == null ) {
					String infoType = jsonRequestBody.getString( "info" );
					String accountToken = jsonRequestBody.getString( "accounttoken" );
					String itemId = "";
					JSONObject params = new JSONObject();
					if( jsonRequestBody.has("itemid") ) {
						itemId = jsonRequestBody.getString( "itemid" );
					}
					if( science.db.accounts.validateAccountTokenByType( accountToken, "admin" ) == true ) {
						privileges = "admin";
					}
					else if( science.db.accounts.validateAccountTokenByType( accountToken, "dev" ) == true ) {
						privileges = "dev";
					}
					else if( science.db.accounts.validateAccountTokenByType( accountToken, "user" ) == true ) {
						privileges = "user";
					}
					else if( science.db.accounts.validateAccountTokenByType( accountToken, "anon" ) == true ) {
						privileges = "anon";
					}
					// String[] properties = { "accounttoken" };
					// science.analytics.trackEvent( accountToken, "Write Settings", timeStamp, FieldMapper.getFields(jsonRequestBody, properties) );
					if( jsonRequestBody.has("params") ) {
						params = jsonRequestBody.getJSONObject( "params" );
					}
					JSONObject settings = science.db.readInfo( accountToken, privileges, infoType, itemId, params );
					jsonReplyBody.put( "_t", "info.get" );
					jsonReplyBody.put( "info", settings );
				}
			}
			else {
				errorInfo = "Missing _t";
			}
			if( errorInfo != null && errorInfo.length() > 0 ) {
				req.response().setStatusCode(400);
				Logs.LogEvent( errorInfo );
				jsonReplyBody.put( "_t", "info.get_error" );
				jsonReplyBody.put( "info", errorInfo );
			}
			JSONObject jsonReply = new JSONObject().put( "_t", "msg" );
			JSONObject jsonHeader = jsonRequest.getJSONObject( "header" );
			jsonReply.put( "header", jsonHeader );
			jsonReply.put( "body", jsonReplyBody );
			Logs.LogEvent( "JSON Reply is " + jsonReply.toString() );
			req.response().headers().set("Content-Type", "application/json; charset=UTF-8");
			req.response().end(jsonReply.toString());
		}
		catch( org.json.JSONException e ) {
			e.printStackTrace();
			Logs.LogEvent( "JSON error is " + errorInfo );
			req.response().setStatusCode(400);
			req.response().end( "{ jsonfailure: true }");
		}
	}

}
