import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

import org.vertx.java.core.buffer.Buffer;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import org.bson.types.ObjectId;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;

import org.json.*;

public class DBContainer {

	private boolean mConnected;
	private DB mDB;
	private MongoClient mMongo;
	public DBContainerAction actions;
	public DBContainerAccount accounts;
	public DBContainerSettings settings;
	public DBContainerQuest quests;
	public DBContainerGame games;
	public DBContainerForums forums;
	public DBContainerThreads threads;
	public DBContainerOAuth oauth;
	
	public DBContainer() {
	}

	public static boolean needsEncoding(Object a) {
		if( a instanceof JSONArray ) {
			return true;
		}
		if( a instanceof JSONObject ) {
			return true;
		}
		return false;
	}
	
	public static DBObject encode(Object a) {
        if (a instanceof JSONObject) {
            return encode((JSONObject)a);
        } else {
            return encode((JSONArray)a);
        }		
	}
	
	public static DBObject encode(JSONArray a) {
        BasicDBList result = new BasicDBList();
        try {
            for (int i = 0; i < a.length(); ++i) {
                Object o = a.get(i);
                if (o instanceof JSONObject) {
                    result.add(encode((JSONObject)o));
                } else if (o instanceof JSONArray) {
                    result.add(encode((JSONArray)o));
                } else {
                    result.add(o);
                }
            }
            return result;
        } catch (JSONException je) {
            return null;
        }
    }

    public static DBObject encode(JSONObject o) {
        BasicDBObject result = new BasicDBObject();
        try {
            Iterator<String> i = o.keys();
            while (i.hasNext()) {
                String k = (String)i.next();
                Object v = o.get(k);
                if (v instanceof JSONArray) {
                    result.put(k, encode((JSONArray)v));
                } else if (v instanceof JSONObject) {
                    result.put(k, encode((JSONObject)v));
                } else {
                    result.put(k, v);
                }
            }
            return result;
        } catch (JSONException je) {
            return null;
        }
    }
    
    private boolean checkConnected() {
		return mConnected;
	}

	public void initialize()  {
		Logs.LogEvent("Initializing Mongo");
	    try {
			mMongo = new MongoClient("localhost", 27017);
			mDB = mMongo.getDB(ContainerConfig.getProperty("db"));
			mConnected = true;
			actions = new DBContainerAction();
			actions.initialize( mDB );
			accounts = new DBContainerAccount();
			accounts.initialize( mDB );
			settings = new DBContainerSettings();
			settings.initialize( mDB );
			quests = new DBContainerQuest();
			quests.initialize( mDB );
			games = new DBContainerGame();
			games.initialize( mDB );
			forums = new DBContainerForums();
			forums.initialize( mDB );
			threads = new DBContainerThreads();
			threads.initialize( mDB );
			oauth = new DBContainerOAuth();
			oauth.initialize( mDB );
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (MongoException e) {
			e.printStackTrace();
		}
	}


	public boolean validateDeveloperSecret( String developerId, String developerSecret ) {
		DBCollection coll = mDB.getCollection("devconfig");
		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put("id", developerId);
		DBObject result = coll.findOne(searchQuery);
		if( result != null ) {
			String unSaltedSecret = result.get("devsecret").toString();
			return ContainerPassword.compareSecrets( developerSecret, unSaltedSecret );
		}		
		return false;
	}
	
	public boolean postLeaderboard( String accountToken, String gameToken, String gameScore ) {
		int count = Integer.parseInt( ContainerConfig.getProperty("scorecount") );
		int currentGameScore = Integer.parseInt( gameScore );
		String type = getLeaderboardType(gameToken);
		if( type.equals( "none") == false ) {
			String accountAlias = accounts.getAccountAliasFromToken( accountToken );
			DBCollection coll = mDB.getCollection("leaderboard");
			BasicDBObject searchQuery = new BasicDBObject();
			List<BasicDBObject> obj = new ArrayList<BasicDBObject>();
			obj.add(new BasicDBObject("accounttoken", accountToken));
			obj.add(new BasicDBObject("gametoken", gameToken));
			searchQuery.put("$and", obj);
			BasicDBObject sortQuery = new BasicDBObject();
			if( type.equals( "descending") ) {
				sortQuery.put( "gamescore", -1 );
			}
			else {				
				sortQuery.put( "gamescore", 1 );
			}
			DBCursor result = coll.find(searchQuery).sort(sortQuery);
			BasicDBObject newScore = new BasicDBObject();
			newScore.put( "gamescore",  Integer.parseInt( gameScore ) );
			newScore.put( "gametoken", gameToken );
			newScore.put( "accounttoken", accountToken );
			newScore.put( "accountalias", accountAlias );
			
			if( result.count() >= count ) {
				DBObject deleteentry = null;
				while( result.hasNext() ) {
					DBObject entry = result.next();
					if( type.equals( "descending") ) {
						if( currentGameScore < Integer.parseInt( entry.get("gamescore").toString() ) ) {
							deleteentry = entry;
						}
					}
					if( type.equals( "ascending") ) {
						if( currentGameScore > Integer.parseInt( entry.get("gamescore").toString() ) ) {
							deleteentry = entry;
						}
					}
				}	
				if( deleteentry != null ) {
					coll.remove( deleteentry );
					coll.save(newScore);
					return true;
				}
			}
			else {
				coll.save(newScore);
				return true;
			}
		}
		return false;
	}
	
	public String getLeaderboardType( String gameToken ) {
		DBCollection coll = mDB.getCollection("gameconfig");
		BasicDBObject searchQuery = new BasicDBObject();
		try {
			searchQuery.put("_id", new ObjectId( gameToken ));
			DBObject result = coll.findOne(searchQuery);
			if( result != null ) {
				if( result.keySet().contains("leaderboard") ) {
					return result.get("leaderboard").toString();
				}
			}
		}
		catch( Exception e ) {
			Logs.LogEvent( "Error " + e.toString() );
		}
		return "none";		
	}
	
	public JSONArray getLeaderboard( String accountToken, String gameToken, JSONObject params ) {
		int count = Integer.parseInt( ContainerConfig.getProperty("scorecount") );
		JSONArray reply = new JSONArray();
		boolean selfLeaderboard = false;
		String type = getLeaderboardType(gameToken);
		if( type.equals( "none") == false ) {
			if( params.has( "accounttoken" ) ) {
				if( params.getString("accounttoken").equals(accountToken)) {
					selfLeaderboard = true;
				}
			}
			if( params.has( "count" ) ) {
				count = Integer.parseInt( params.getString("count"));
			}
			DBCollection coll = mDB.getCollection("leaderboard");
			BasicDBObject searchQuery = new BasicDBObject();
			if( selfLeaderboard == true ) {
				List<BasicDBObject> obj = new ArrayList<BasicDBObject>();
				obj.add(new BasicDBObject("accounttoken", accountToken));
				obj.add(new BasicDBObject("gametoken", gameToken));
				searchQuery.put("$and", obj);
			}
			else {
				searchQuery.put("gametoken", gameToken);
			}
			BasicDBObject sortQuery = new BasicDBObject();
			if( type.equals( "descending") ) {
				sortQuery.put( "gamescore", 1 );
			}
			else {				
				sortQuery.put( "gamescore", -1 );
			}
			
			DBCursor result = coll.find(searchQuery).sort(sortQuery).limit(count);
			while( result.hasNext() ) {
				JSONObject score = new JSONObject();
				DBObject entry = result.next();
				score.put( "gamescore", entry.get("gamescore").toString());
				score.put( "accounttoken", entry.get("accounttoken").toString());
				score.put( "accountalias", entry.get("accountalias").toString());
				reply.put(score);
			}
		}
		return reply;
	}
	
	JSONObject readUserInfo( String accountToken, String privileges, String itemId, JSONObject params ) {
		JSONObject replyData = new JSONObject();
		boolean self = false;
		if( params != null ) {
			if( params.has( "users") ) {
				JSONArray userList = params.getJSONArray( "users" );
				JSONArray userResults = new JSONArray();
				for( int i = 0; i < userList.length(); i ++ ) {
					String userId = userList.getString(i);
					JSONObject userResult = readUserInfo( accountToken, privileges, userId, null );
					userResult.put( "userid",  userId );
					userResults.put( userResult );
				}
				replyData.put( "results",  userResults );
				return replyData;
			}
		}
		if( accountToken.equals( itemId ) ) {
			self = true;
		}
		if( self == true || privileges.equals( "admin" )) {
			DBCollection coll = mDB.getCollection("accounts");
			BasicDBObject searchQuery = new BasicDBObject();
			try {
				searchQuery.put("_id", new ObjectId( accountToken ));
				DBObject result = coll.findOne(searchQuery);
				if( result != null ) {
					replyData.put( "email", result.get( "accountId" ) );
				}
			}
			catch( Exception e ) {
				Logs.LogEvent( "Error " + e.toString() );
			}
		}
		DBCollection coll = mDB.getCollection("userconfig");
		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put("id", itemId);
		DBObject result = coll.findOne(searchQuery);
		if( result != null ) {
			String [] myFields = ContainerParams.getInfoFields( "user" );
			for( int i = 0; i < myFields.length; i ++ ) {
				replyData.put( myFields[i], result.get( myFields[i] ) );
			}
		}
		replyData.put( "actions", science.db.actions.getActionsDataForUser( itemId ) );
		replyData.put( "quests", science.db.quests.getQuestDataForUser(itemId) );
		return replyData;
	}
	
	JSONObject readActionInfo( String accountToken, String privileges, String itemId, JSONObject params ) {
		return actions.getActionById( itemId );
	}
	
	JSONObject readSiteInfo( String accountToken, String privileges, String itemId ) {
		return settings.readSiteSettings();			
	}
	
	JSONObject readInfo( String accountToken, String privileges, String infoType, String itemId, JSONObject params )
	{
		if( checkConnected() ) {
			if( infoType.equals( "user") ) {
				return readUserInfo( accountToken, privileges, itemId, params );
			}
			if( infoType.equals( "game") ) {
				return science.db.games.readGameInfo( accountToken, privileges, itemId, params );				
			}
			if( infoType.equals( "action") ) {
				return readActionInfo( accountToken, privileges, itemId, params );				
			}
			if( infoType.equals( "site") ) {
				return readSiteInfo( accountToken, privileges, itemId );				
			}
		}		
		return new JSONObject();
	}

	DBObject getQuery(String where, String whereValue)
	{
		return new BasicDBObject(where, Pattern.compile("^.*" + whereValue + ".*$", Pattern.CASE_INSENSITIVE));
	}
	
	JSONObject searchGameInfo( String accountToken, String privileges, JSONObject detail ) 
	{
		return null;
	}

	JSONObject searchInfo( String accountToken, String privileges, String searchType, JSONObject detail ) 
	{
		if( searchType.equals( "user") ) {
			return science.db.accounts.searchUserInfo( accountToken, privileges, detail );
		}
		if( searchType.equals( "game") ) {
			return searchGameInfo( accountToken, privileges, detail );
		}
		return new JSONObject();
	}
	
	public boolean validateTag( JSONObject myTag )
	{
		JSONObject settings = science.db.settings.readSiteSettings();
		if( myTag.has( "name") ) {
			if( settings != null ) {
				JSONArray siteTags = settings.getJSONArray( "tags");
				if( siteTags != null ) {
					for( int i = 0; i < siteTags.length(); i ++ ) {
						if( siteTags.getJSONObject(i).getString( "name" ).equals( myTag.getString( "name" ) ) == true ) {
							return true;
						}
					}
				}
			}
		}		
		return false;
	}
	
	public void putMessage( Buffer body ) {
		if( ContainerConfig.getProperty( "transactions" ).equals( "1" ) ) {
			try { 
				DBCollection coll = mDB.getCollection("transactions");
				BasicDBObject document = new BasicDBObject();
				document.put("contents", body.toString() );
				document.put("createdDate", new Date());
				coll.insert(document);		
			}
			catch( Exception e ) {
				Logs.LogEvent( "Error " + e.toString() );
			}
		}
	}
	
	public JSONObject getConfig( String deviceToken ) {
		JSONObject jsonReply = new JSONObject();
		jsonReply.put( "levels", ContainerConfig.getProperty("levelversion") );
		return jsonReply;
	}

	public void testMongo()
	{
		Logs.LogEvent("Mongo Test Underway");
		try {
 
		/**** Connect to MongoDB ****/
		// Since 2.10.0, uses MongoClient
		MongoClient mongo = new MongoClient("localhost", 27017);
	 
		/**** Get database ****/
		// if database doesn't exists, MongoDB will create it for you
		DB db = mongo.getDB("testdb");
	 
		/**** Get collection / table from 'testdb' ****/
		// if collection doesn't exists, MongoDB will create it for you
		DBCollection table = db.getCollection("user");
	 
		/**** Insert ****/
		// create a document to store key and value
		BasicDBObject document = new BasicDBObject();
		document.put("name", "mkyong");
		document.put("age", 30);
		document.put("createdDate", new Date());
		table.insert(document);
	 
		/**** Find and display ****/
		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put("name", "mkyong");
	 
		DBCursor cursor = table.find(searchQuery);
	 
		while (cursor.hasNext()) {
			Logs.LogEvent(cursor.next().toString());
		}
	 
		/**** Update ****/
		// search document where name="mkyong" and update it with new values
		BasicDBObject query = new BasicDBObject();
		query.put("name", "mkyong");
	 
		BasicDBObject newDocument = new BasicDBObject();
		newDocument.put("name", "mkyong-updated");
	 
		BasicDBObject updateObj = new BasicDBObject();
		updateObj.put("$set", newDocument);
	 
		table.update(query, updateObj);
	 
		/**** Find and display ****/
		BasicDBObject searchQuery2 
		    = new BasicDBObject().append("name", "mkyong-updated");
	 
		DBCursor cursor2 = table.find(searchQuery2);
	 
		while (cursor2.hasNext()) {
			Logs.LogEvent(cursor2.next().toString());
		}
	 
		/**** Done ****/
		Logs.LogEvent("Done");
	 
	    } catch (UnknownHostException e) {
			e.printStackTrace();
	    } catch (MongoException e) {
			e.printStackTrace();
	    }
	}
}
