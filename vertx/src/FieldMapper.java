import java.util.Iterator;
import java.util.Set;

import org.json.*;

public class FieldMapper {
	public static String[] getFields( JSONObject data, String[] properties ) {
		String[] fields, superfields;
		int index;
		if( data.has( "superproperties") ) {
			JSONObject superproperties = data.getJSONObject( "superproperties" );
			Set<String> keys = superproperties.keySet();
			if( keys.size() > 0 ) {
				index = 0;
				superfields = new String[2*keys.size()];
				Iterator<String> iter = keys.iterator();
				while (iter.hasNext()) {
					String myKey = iter.next();
					String myValue = superproperties.getString( myKey);
					superfields[index] = myKey;
					superfields[index + 1] = myValue;
					index += 2;
				}
			}
			else {
				superfields = new String[0];
			}
		}
		else {
			superfields = new String[0];
		}
		index = 0;
		if( data.has( "buckets") ) {
			JSONObject buckets = data.getJSONObject( "buckets" );
			Set<String> keys = buckets.keySet();
			if( keys.size() > 0 ) {
				fields = new String[superfields.length + 2*(properties.length + keys.size())];
				Iterator<String> iter = keys.iterator();
				while (iter.hasNext()) {
					String myKey = iter.next();
					String myValue = buckets.getString( myKey);
					fields[index] = myKey;
					fields[index + 1] = myValue;
					index += 2;
				}
			}
			else {				
				fields = new String[superfields.length + 2*(properties.length)];
			}
		}
		else {
			fields = new String[superfields.length + 2*(properties.length)];
		}
		for( int i = 0; i < properties.length; i ++ ) {
			fields[index + 2*i] = properties[i];
			if( data.has( properties[i] ) ) {
				fields[index + 2*i + 1] = data.getString( properties[i] );
			}
			else {
				fields[index + 2*i + 1] = "_" + properties[i];
			}
		}
		for( int i = 0; i < superfields.length; i ++ ) {
			fields[index + 2*properties.length + i] = superfields[i];
		}
		return fields;
	}	
}
