import org.bson.types.ObjectId;
import org.json.JSONArray;
import org.json.JSONObject;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

public class DBContainerForums {
	private DB mDB;

	public DBContainerForums() {
	}

	public void initialize( DB db )  {
		Logs.LogEvent("Initializing Forum DB");
		mDB = db;
	}

	public void createForum( String category, String name, String description, String ownerId ) {
		DBCollection catcoll = mDB.getCollection("forumcategories");
		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put( "title",  category );
		DBObject result = catcoll.findOne(searchQuery);
		if( result != null ) {
			String categoryId = result.get( "_id" ).toString();
			DBCollection coll = mDB.getCollection("forumdata");
			searchQuery = new BasicDBObject();
			searchQuery.put( "title", name );
			result = coll.findOne(searchQuery);
			if( result != null ) {
				Logs.LogEvent( "Category " + category + " already has a forum " + name );
			}
			else {
				BasicDBObject newForum = new BasicDBObject();
				newForum.put( "title", name );
				newForum.put( "description", description);
				newForum.put( "categoryId",  categoryId );
				newForum.put( "state",  "visible" );
				if( ownerId != null && ownerId.length() > 0 ) {
					newForum.put( "owner",  ownerId );
				}
				coll.insert( newForum );
			}
		}
		else {
			Logs.LogEvent( "Missing category " + category + " for forum " + name );
		}
	}
	
	JSONObject updateForum( String accountToken, String privileges, String forumId, String state ) {
		JSONObject response = new JSONObject();
		if( privileges.equals( "admin" ) ) {
			DBCollection coll = mDB.getCollection("forumdata");
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put( "_id", new ObjectId( forumId ) );
			DBObject result = coll.findOne(searchQuery);
			if( result != null ) {
				if( result.get( "state" ).toString().equals( state ) == false ) {
					result.put( "state", state );
					coll.save( result );
				}
				response.put( "title", result.get( "title" ) );
				response.put( "description", result.get( "description" ) );
				response.put( "categoryId",  result.get( "categoryId" ) );
				response.put( "state",  result.get( "state" ) );
				response.put( "id",  result.get( "_id" ).toString() );
			}		
		}
		return response;
	}
	
	public void createGameForum( String gameId, String devId ) {
		String gameName = science.db.games.getGameName( gameId );
		createForum( "Games", gameName, "This is a forum for discussions about " + gameName, devId );
	}
	
	public boolean isForumOwner( String userToken, String forumId ) {
		DBCollection coll = mDB.getCollection("forumdata");
		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put( "_id", new ObjectId( forumId ) );
		DBObject result = coll.findOne(searchQuery);
		if( result != null ) {
			if( result.keySet().contains( "owner" ) ) {
				if( result.get( "owner").toString().equals( userToken ) ) {
					return true;
				}
			}
		}
		return false;
	}
	
	public void createLiveGameForums() {
		try {
			Logs.LogEvent( "creating live forums" );
			JSONObject games = science.db.games.readGameInfo( "",  "",  "",  new JSONObject() );
			JSONArray liveGames = games.getJSONArray( "games" );
			Logs.LogEvent( "creating live forums for " + games.toString() );
			for( int i = 0; i < liveGames.length(); i ++ ) {
				JSONObject myGame = liveGames.getJSONObject(i);
				if( myGame.has( "name") ) {
					String gameName = myGame.getString( "name" );
					createForum( "Games", gameName, "This is a forum for discussions about " + gameName, myGame.getString( "devid" ) );
				}
			}
		}
		catch( Exception e ) {
			e.printStackTrace();
		}
		
	}
	
	public void createDefaultForums() {
		DBCollection coll = mDB.getCollection("forumcategories");
		try {
			BasicDBObject searchQuery = new BasicDBObject();
			DBCursor result = coll.find(searchQuery);
			if( result.count() == 0 ) {
				Logs.LogEvent( "creating forum headers" );
				for( int i = 0; i < ContainerParams.myDefaultForums.length; i ++ ) {
					BasicDBObject forumHeader = new BasicDBObject();					
					forumHeader.put( "title", ContainerParams.myDefaultForums[i][0] );
					forumHeader.put( "description", ContainerParams.myDefaultForums[i][1] );
					coll.insert( forumHeader );
					createForum( ContainerParams.myDefaultForums[i][0], ContainerParams.myDefaultForums[i][0], ContainerParams.myDefaultForums[i][1], "" );
				}
			} 
			createLiveGameForums();
		}
		catch( Exception e ) {
			e.printStackTrace();
		}
		
	}
	
	public JSONArray queryForumCategories( String privileges ) {
		JSONArray response = new JSONArray();
		createDefaultForums();
		DBCollection coll = mDB.getCollection("forumcategories");
		try {
			BasicDBObject searchQuery = new BasicDBObject();
			DBCursor result = coll.find(searchQuery);
			while( result.hasNext() ) {
				DBObject category = result.next();
				category.put( "id", category.get( "_id" ).toString() );
				category.removeField( "_id");
				response.put( category );
			}
		}
		catch( Exception e ) {
			e.printStackTrace();
		}
			
		return response;
	}
	
	public JSONArray queryForum( String privileges, String forumId, int threadStart, int threadCount ) {
		JSONArray response = new JSONArray();
		DBCollection coll = mDB.getCollection("forumdata");
		try {
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put( "_id", new ObjectId( forumId ) );
			DBObject forum = coll.findOne(searchQuery);
			if( forum != null ) {
				forum.put( "id", forumId );
				forum.removeField( "_id");
				JSONArray threads = science.db.threads.getThreadsForForum( privileges, forumId, threadStart, threadCount );
				forum.put( "threadstart",  "" + threadStart );
				forum.put( "threadcount", "" + threadCount );
				forum.put( "threads", DBContainer.encode( threads ) );
				if( privileges.equals( "admin" ) ) {
					response.put( forum );
				}
				else {
					if( forum.get( "state").toString().equals( "visible" ) ) {
						response.put( forum );
					}
				}
			}
		}
		catch( Exception e ) {
			e.printStackTrace();
		}
		return response;
	}
	
	public JSONArray queryForums( String privileges, int threadCount ) {
		JSONArray response = new JSONArray();
		createDefaultForums();
		DBCollection coll = mDB.getCollection("forumdata");
		Logs.LogEvent( "getting threads " + threadCount + " for forums " );
		try {
			BasicDBObject searchQuery = new BasicDBObject();
			DBCursor result = coll.find(searchQuery);
			while( result.hasNext() ) {
				DBObject forum = result.next();
				forum.put( "id", forum.get( "_id" ).toString() );
				forum.removeField( "_id");
				if( threadCount > 0 ) {
					JSONArray threads = science.db.threads.getThreadsForForum( privileges, forum.get( "id" ).toString(), 0, threadCount );
					forum.put( "threadstart",  "0" );
					forum.put( "threadcount", "" + threadCount );
					forum.put( "threads", DBContainer.encode( threads ) );
				}
				if( privileges.equals( "admin" ) ) {
					response.put( forum );
				}
				else {
					if( forum.get( "state").toString().equals( "visible" ) ) {
						response.put( forum );
					}
				}
			}
		}
		catch( Exception e ) {
			e.printStackTrace();
		}
		return response;
	}
	
}
