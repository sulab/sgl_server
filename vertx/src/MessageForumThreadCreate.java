import org.json.JSONArray;
import org.json.JSONObject;


import org.vertx.java.core.buffer.Buffer;
import org.vertx.java.core.http.HttpServerRequest;

public class MessageForumThreadCreate {
	private String validateMessage( JSONObject jsonRequestBody ) {
		if( jsonRequestBody.has("accounttoken") ) {
			if( jsonRequestBody.has("forumid") ) {
				if( jsonRequestBody.has("title") ) {
					if( jsonRequestBody.has("content") ) {
						return null;
					}
					else {
						return "Missing content";
					}
				}
				else {
					return "Missing title";
				}
			}
			else {
				return "Missing forumId";
			}
		}
		else {
			return "Missing account token";
		}
	}

	public MessageForumThreadCreate( HttpServerRequest req, Buffer body )  {
		String errorInfo = null;
		try { 
			JSONObject jsonRequest = new JSONObject( body.toString() ); 
			JSONObject jsonReplyBody = new JSONObject();
			JSONObject jsonRequestBody = jsonRequest.getJSONObject( "body" );
			if( jsonRequestBody.has( "_t") && jsonRequestBody.getString( "_t" ).equals( "forumthread.create" ) ) {
				errorInfo = validateMessage( jsonRequestBody );
				if( errorInfo == null ) {
					String accountToken = jsonRequestBody.getString( "accounttoken" );
					String forumId = jsonRequestBody.getString( "forumid" );
					String title = jsonRequestBody.getString( "title" );
					String content = jsonRequestBody.getString( "content" );
					String privileges = science.db.accounts.getAccountPrivs( accountToken );
					// String[] properties = { "accounttoken" };
					// science.analytics.trackEvent( accountToken, "Write Settings", timeStamp, FieldMapper.getFields(jsonRequestBody, properties) );
					JSONObject newContent = science.db.threads.newThreadForForum( privileges, forumId, accountToken, title, content );
					jsonReplyBody.put( "_t", "forumthread.create" );
					jsonReplyBody.put( "content", newContent );
				}
			}
			else {
				errorInfo = "Missing _t";
			}
			if( errorInfo != null && errorInfo.length() > 0 ) {
				req.response().setStatusCode(400);
				Logs.LogEvent( errorInfo );
				jsonReplyBody.put( "_t", "forumthread.create_error" );
				jsonReplyBody.put( "info", errorInfo );
			}
			JSONObject jsonReply = new JSONObject().put( "_t", "msg" );
			JSONObject jsonHeader = jsonRequest.getJSONObject( "header" );
			jsonReply.put( "header", jsonHeader );
			jsonReply.put( "body", jsonReplyBody );
			Logs.LogEvent( "JSON Reply is " + jsonReply.toString() );
			req.response().headers().set("Content-Type", "application/json; charset=UTF-8");
			req.response().end(jsonReply.toString());
		}
		catch( org.json.JSONException e ) {
			e.printStackTrace();
			Logs.LogEvent( "JSON error is " + errorInfo );
			req.response().setStatusCode(400);
			req.response().end( "{ jsonfailure: true }");
		}
	}

}
