import org.json.JSONObject;
import org.vertx.java.core.buffer.Buffer;
import org.vertx.java.core.http.HttpServerRequest;

public class MessageOAuthValidate {
	private String validateMessage( JSONObject jsonRequestBody ) {
		if( jsonRequestBody.has("accounttoken") ) {
			if( jsonRequestBody.has("gametoken") ) {
				return null;
			}
			else {
				return "Missing game token";
			}
		}
		else {
			return "Missing account token";
		}
	}

	public MessageOAuthValidate( HttpServerRequest req, Buffer body )  {
		String errorInfo = null;
		try { 
			JSONObject jsonRequest = new JSONObject( body.toString() ); 
			JSONObject jsonReplyBody = new JSONObject();
			JSONObject jsonRequestBody = jsonRequest.getJSONObject( "body" );
			if( jsonRequestBody.has( "_t") && jsonRequestBody.getString( "_t" ).equals( "oauth.validate" ) ) {
				errorInfo = validateMessage( jsonRequestBody );
				if( errorInfo == null ) {
					String accountToken = jsonRequestBody.getString( "accounttoken" );
					String gameToken = jsonRequestBody.getString( "gametoken" );
					String result = science.db.oauth.validateOAuthToken( accountToken, gameToken );
					jsonReplyBody.put( "_t", "oauth.validated" );
					jsonReplyBody.put( "result", result );
				}
			}
			else {
				errorInfo = "Missing _t";
			}
			if( errorInfo != null && errorInfo.length() > 0 ) {
				req.response().setStatusCode(400);
				Logs.LogEvent( errorInfo );
				jsonReplyBody.put( "_t", "oauth.validate_error" );
				jsonReplyBody.put( "info", errorInfo );
			}
			JSONObject jsonReply = new JSONObject().put( "_t", "msg" );
			JSONObject jsonHeader = jsonRequest.getJSONObject( "header" );
			jsonReply.put( "header", jsonHeader );
			jsonReply.put( "body", jsonReplyBody );
			Logs.LogEvent( "JSON Reply is " + jsonReply.toString() );
			req.response().headers().set("Content-Type", "application/json; charset=UTF-8");
			req.response().end(jsonReply.toString());
		}
		catch( org.json.JSONException e ) {
			e.printStackTrace();
			Logs.LogEvent( "JSON error is " + errorInfo );
			req.response().setStatusCode(400);
			req.response().end( "{ jsonfailure: true }");
		}
	}

}
