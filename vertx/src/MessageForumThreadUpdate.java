import org.json.JSONObject;
import org.vertx.java.core.buffer.Buffer;
import org.vertx.java.core.http.HttpServerRequest;

public class MessageForumThreadUpdate {
	private String validateMessage( JSONObject jsonRequestBody ) {
		if( jsonRequestBody.has("accounttoken") ) {
			if( jsonRequestBody.has("state") ) {
				if( jsonRequestBody.has( "threadid" ) ) {
					return null;
				}
				else {
					return "needs thread id";
				}
			}
			else {
				return "needs state visible, hidden, lock or unlock";
			}
		}
		else {
			return "Missing account token";
		}
	}

	public MessageForumThreadUpdate( HttpServerRequest req, Buffer body )  {
		String errorInfo = null;
		try { 
			JSONObject jsonRequest = new JSONObject( body.toString() ); 
			JSONObject jsonReplyBody = new JSONObject();
			JSONObject jsonRequestBody = jsonRequest.getJSONObject( "body" );
			if( jsonRequestBody.has( "_t") && jsonRequestBody.getString( "_t" ).equals( "forumthread.update" ) ) {
				errorInfo = validateMessage( jsonRequestBody );
				if( errorInfo == null ) {
					String accountToken = jsonRequestBody.getString( "accounttoken" );
					String threadId = "";
					String state = "";
					String privileges = science.db.accounts.getAccountPrivs( accountToken );
					state = jsonRequestBody.getString( "state" );
					if( state.equals( "visible" ) || state.equals( "hidden" ) || state.equals( "lock" ) || state.equals( "unlock" ) ) {
						threadId = jsonRequestBody.getString( "threadid" );
						JSONObject thread = science.db.threads.updateThread( accountToken, privileges, threadId, state );
						jsonReplyBody.put( "thread", thread );
						// String[] properties = { "accounttoken" };
						// science.analytics.trackEvent( accountToken, "Write Settings", timeStamp, FieldMapper.getFields(jsonRequestBody, properties) );
						jsonReplyBody.put( "_t", "forumthread.update" );
					}
					else {
						errorInfo = "state must be hidden or visible, not " + state;
					}
				}
			}
			else {
				errorInfo = "Missing _t";
			}
			if( errorInfo != null && errorInfo.length() > 0 ) {
				req.response().setStatusCode(400);
				Logs.LogEvent( errorInfo );
				jsonReplyBody.put( "_t", "forumthread.update_error" );
				jsonReplyBody.put( "info", errorInfo );
			}
			JSONObject jsonReply = new JSONObject().put( "_t", "msg" );
			JSONObject jsonHeader = jsonRequest.getJSONObject( "header" );
			jsonReply.put( "header", jsonHeader );
			jsonReply.put( "body", jsonReplyBody );
			Logs.LogEvent( "JSON Reply is " + jsonReply.toString() );
			req.response().headers().set("Content-Type", "application/json; charset=UTF-8");
			req.response().end(jsonReply.toString());
		}
		catch( org.json.JSONException e ) {
			e.printStackTrace();
			Logs.LogEvent( "JSON error is " + errorInfo );
			req.response().setStatusCode(400);
			req.response().end( "{ jsonfailure: true }");
		}
	}

}
