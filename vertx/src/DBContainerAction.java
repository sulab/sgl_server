import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.bson.types.ObjectId;
import org.json.JSONArray;
import org.json.JSONObject;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

public class DBContainerAction {
	private DB mDB;

	public DBContainerAction() {
	}

	public void initialize( DB db )  {
		Logs.LogEvent("Initializing Action DB");
		mDB = db;
	}

	public JSONObject findGameAction( String gameid, String name ) {
		JSONObject action = new JSONObject();
		try {
			DBCollection coll = mDB.getCollection("actionconfig");
			BasicDBObject searchQuery = new BasicDBObject();
			List<BasicDBObject> obj = new ArrayList<BasicDBObject>();
			obj.add(new BasicDBObject("gameid", gameid));
			obj.add(new BasicDBObject("name", name));
			searchQuery.put("$and", obj);
			DBObject result = coll.findOne(searchQuery);
			if( result != null ) {
				if( result.keySet().contains( "active") && result.get( "active" ).toString().equals( "1" ) ) {
					Iterator<String> entries = result.keySet().iterator();
					while( entries.hasNext() ) {
						String entry = entries.next().toString();
						action.put( entry, result.get( entry ) );
					}
				}
			}
			else {
				Logs.LogEvent( "not found " + gameid + ", name " + name );
			}
		}
		catch( Exception e ) {
			e.printStackTrace();
		}
		return action;
	}
	
	public JSONArray readSiteActions() {
		JSONArray actions = new JSONArray();
		DBCollection coll = mDB.getCollection("actionconfig");
		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put("gameid", "0");
		try {
			DBCursor results = coll.find(searchQuery);
			while (results.hasNext()) {
				DBObject result = results.next();
				JSONObject action = new JSONObject();
				Iterator<String> entries = result.keySet().iterator();
				while( entries.hasNext() ) {
					String entry = entries.next().toString();
					action.put( entry, result.get( entry ).toString() );				
				}
				actions.put(action);
			}
		}
		catch( Exception e ) {
			e.printStackTrace();
		}
		return actions;
	}
	
	public void writeSiteActions( JSONArray actions ) {
		try {
			DBCollection coll = mDB.getCollection("actionconfig");
			BasicDBObject searchQuery = new BasicDBObject();
			for( int i = 0; i < actions.length(); i ++ ) {
				JSONObject action = actions.getJSONObject(i);
				List<BasicDBObject> obj = new ArrayList<BasicDBObject>();
				obj.add(new BasicDBObject("gameid", "0"));
				obj.add(new BasicDBObject("name", action.get("name").toString()));
				searchQuery.put("$and", obj);
				DBObject result = coll.findOne(searchQuery);
				if( result != null ) {
					Iterator<String> entries = action.keySet().iterator();
					while( entries.hasNext() ) {
						String entry = entries.next().toString();
						result.put( entry, action.get( entry ).toString() );
					}
					coll.save( result );
				}
				else {
					if( action.has( "gameid" ) == false ) {
						action.put( "gameid",  "0" );
					}
					DBObject myAction = DBContainer.encode( action );
					coll.insert( myAction );
				}
			}
		}
		catch( Exception e ) {
			e.printStackTrace();
		}
	}
	
	public String createGameAction( JSONObject action ) {
		String result = "";
		JSONObject newAction;
		newAction = findGameAction( action.getString( "gameid" ), action.getString( "name" ) );
		if( newAction.keySet().isEmpty() ) {
			for( int i = 0; i < ContainerParams.myActionFields.length; i ++ ) {
				String field = ContainerParams.myActionFields[i];
				if( action.has( field ) == false ) {
					return "Action is missing " + field;
				}
			}
			DBCollection coll = mDB.getCollection("actionconfig");
			BasicDBObject document = new BasicDBObject();
			Iterator<String> entries = action.keySet().iterator();
			while( entries.hasNext() ) {
				String entry = entries.next().toString();
				document.put( entry, action.get( entry ) );
			}
			coll.insert( document );
			JSONObject gameActivity = findGameAction( action.getString( "gameid"), action.getString( "name") );
			if( gameActivity.has( "_id" ) ) {
				result = gameActivity.get("_id").toString();
			}
		}
		return result;
	}

	public boolean checkAccountActivities( DBObject activity, JSONObject params ) {
		boolean validated = false;
		if( params == null ) {
			return true;
		}
		if( params.has( "filters") ) {
			JSONObject filters = params.getJSONObject( "filters" );
			Iterator<String> myKeys = filters.keys();
			while( myKeys.hasNext() ) {
				String key = myKeys.next();
				if( activity.keySet().contains(key) ) {
					if( activity.get( key ).toString().equals(filters.get(key).toString() ) == false ) {
						return false;
					}
				}
			}
			validated = true;
		}
		else {
			validated = true;
		}
		if( params.has( "range" ) ) {
			int from = 0;
			int to = 0;
			Date currentDate = new Date();
			JSONObject range = params.getJSONObject( "range" );
			if( range.has( "from" ) ) {
				from = Integer.parseInt( range.getString("from") );
			}
			if( range.has( "to" ) ) {
				to = Integer.parseInt( range.getString("to") );
			}
			if( from != 0 ) {
				long fromTime = currentDate.getTime() + from*60*1000;
				long toTime = currentDate.getTime() + to*60*1000;
				DateFormat formatter = new SimpleDateFormat("EEE MMM d HH:mm:ss z yyyy");
				try { 
					Date eventDate = formatter.parse(activity.get("createdDate").toString());
					long eventTime = eventDate.getTime();
					if( fromTime < eventTime && toTime >= eventTime ) {
						validated = true;
					}
					else {
						validated = false;
					}
				}
				catch( Exception e ) {
					Logs.LogEvent( "Exception " + e.toString() );
				}
			}
		}
		return validated;
	}
	
	public String checkExpiredActivity( DBObject activity ) {
		try {
			String expiry = activity.get("expiry").toString();
			if( expiry.length() > 0 ) {
				Date currentDate = new Date();
				int expiryDays = Integer.parseInt( expiry );
				long expiryTime = currentDate.getTime() - expiryDays*24*60*60*1000; 
				DateFormat formatter = new SimpleDateFormat("EEE MMM d HH:mm:ss z yyyy");
				Date eventDate = formatter.parse(activity.get("createdDate").toString());
				long eventTime = eventDate.getTime();
				if( expiryTime > eventTime ) {
					return "1";
				}
			}		
		}
		catch (Exception e) {
		}
		return "0";
	}
	
	public JSONArray readAccountActivities( String accountToken, JSONObject params ) {
		JSONArray result = new JSONArray();
		String accountId = science.db.accounts.getAccountIdFromToken( accountToken );
		if( accountId.length() > 0 ) {
			DBCollection coll = mDB.getCollection("activity");
			DBObject searchQuery = new BasicDBObject();
			searchQuery.put("accountId", accountId);
			Logs.LogEvent( "time from " + searchQuery.toString() );
			DBCursor results = coll.find(searchQuery);
			try { 
				while (results.hasNext()) {
					DBObject activity = results.next();
					activity.removeField( "_id");
					if( checkAccountActivities( activity, params ) == true ) {
						activity.put( "expired", checkExpiredActivity( activity ) );
						result.put( activity );
					}
				}
			}
			catch( Exception e ) {
				Logs.LogEvent( "Error " + e.toString() );
				return result;
			}
		}
		return result;
	}

	public int addUserPoints( String accountId, JSONObject myActivity ) {
		int newLevel = -1;
		JSONObject settings = science.db.settings.readSiteSettings();
		JSONArray points = settings.getJSONArray( "points" );
		int maxLevels = Integer.parseInt( settings.getString( "maxlevels") );
		JSONObject levels = settings.getJSONObject( "levelcurve" );
		for( int i = 0; i < points.length(); i ++ ) {
			JSONObject pointSet = points.getJSONObject(i);
			if( pointSet.getString("name").equals(myActivity.getString("points"))) {
				int pointValue = Integer.parseInt( pointSet.getString( "value" ) );
				DBCollection coll = mDB.getCollection("userconfig");
				BasicDBObject searchQuery = new BasicDBObject();
				searchQuery.put("id", accountId);
				DBObject result = coll.findOne(searchQuery);
				if( result != null ) {
					int myExperience = 0;
					int myLevel = 0;
					if( result.keySet().contains( "points" ) ) {
						myExperience = Integer.parseInt( result.get( "points" ).toString() );
					}
					if( result.keySet().contains( "level" ) ) {
						myLevel = Integer.parseInt( result.get( "level" ).toString() );
					}
					myExperience += pointValue;
					result.put( "points",  "" + myExperience );
					for( int j = 0; j < maxLevels; j ++ ) {
						int nextValue = Integer.parseInt( levels.getString( "" + j ) );
						if( myExperience >= nextValue ) {
							newLevel = j;
						}
					}
					if( newLevel > myLevel ) {
						result.put( "level",  "" + newLevel );
					}
					else {
						newLevel = -1;						
					}
					coll.save( result );
				}
			}
		}
		return newLevel;
	}

	public JSONObject findSiteAction( String name ) {
		return findGameAction( "0", name );
	}
	
	public JSONObject findAction( String name, String gameId ) {
		if( gameId == null || gameId.equals( "0" ) || gameId.length() == 0 ) { 
			return findSiteAction( name );
		}
		else {
			return findGameAction( gameId, name );
		}
	}
	
	public boolean logActionForActivity( String accountToken, String gameId, String actionId ) {
		Logs.LogEvent( "Logging " + gameId + ", " + actionId + " for user " + accountToken );
		try { 
			DBCollection coll = mDB.getCollection("actiondata");
			BasicDBObject searchQuery = new BasicDBObject();
			List<BasicDBObject> obj = new ArrayList<BasicDBObject>();
			obj.add(new BasicDBObject("userid", accountToken));
			obj.add(new BasicDBObject("gameid", gameId));
			obj.add(new BasicDBObject("actionid", actionId));
			searchQuery.put("$and", obj);
			DBObject result = coll.findOne(searchQuery);
			if( result == null ) {
				Logs.LogEvent( "Empty search, creating new log activity" );
				BasicDBObject questItem = new BasicDBObject();
				questItem.put( "userid", accountToken );
				questItem.put( "gameid", gameId );
				questItem.put( "actionid",  actionId );
				coll.save( questItem );
			}
		}
		catch( Exception e ) {
			e.printStackTrace();
		}
		return true;
	}
	
	public JSONArray getActionsDataForUser( String accountToken ) {
		JSONArray actions = new JSONArray();
		DBCollection coll = mDB.getCollection("actiondata");
		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put("userid", accountToken);
		DBCursor results = coll.find(searchQuery);
		try { 
			while (results.hasNext()) {
				DBObject activityObject = results.next();
				activityObject.removeField( "_id");
				JSONObject activity = new JSONObject();
				activity.put( "gameid", activityObject.get( "gameid" ).toString() );
				activity.put( "actionid", activityObject.get( "actionid" ).toString() );
				actions.put( activity );
			}
		}
		catch( Exception e ) {
			e.printStackTrace();
		}
		return actions;
	}
	
	public boolean createActivity( String accountToken, String activity, String gameId, JSONObject details ) {
		String accountId = science.db.accounts.getAccountIdFromToken( accountToken );
		String accountAlias = science.db.accounts.getAccountAliasFromToken( accountToken );
		if( accountId.length() > 0 ) {
			JSONObject myActivity = findAction( activity, gameId );
			if( myActivity.has("name") ) {
				DBCollection coll = mDB.getCollection("activity");
				BasicDBObject document = new BasicDBObject();
				document.put("accountId", accountId);
				document.put("accountAlias", accountAlias);
				document.put("gameId", gameId);
				document.put("details", details.toString());
				document.put("createdDate", new Date());
				Iterator<String> myKeys = myActivity.keys();
				while( myKeys.hasNext() ) {
					String key = myKeys.next();
					if( key.equals( "_id") == false ) {
						document.put( key, myActivity.get(key).toString() );
					}
					else {
						document.put( "actionid", myActivity.get(key).toString() );
					}
				}
				coll.insert(document);
				logActionForActivity( accountToken, gameId, myActivity.get( "_id" ).toString() );
				int newLevel = addUserPoints( accountToken, myActivity );
				if( newLevel > 0 ) {
					document = new BasicDBObject();
					document.put("accountId", accountId);
					document.put("accountAlias", accountAlias);
					document.put("details", "" + newLevel );
					document.put("createdDate", new Date());
					myActivity = findSiteAction( "levelup" );
					if( myActivity.has("name") ) {
						myKeys = myActivity.keys();
						while( myKeys.hasNext() ) {
							String key = myKeys.next();
							if( key.equals( "_id") == false ) {
								document.put( key, myActivity.get(key).toString() );
							}
							else {
								document.put( "actionid", myActivity.get(key).toString() );
							}
						}
					}
					else {
						document.put("name", "levelup");						
					}
					coll.insert(document);
				}
				return true;
			}
		}
		return false;
	}

	public JSONObject getActionById( String actionid ) {
		JSONObject action = new JSONObject();
		try {
			DBCollection coll = mDB.getCollection("actionconfig");
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put("_id", new ObjectId( actionid ));
			DBObject result = coll.findOne(searchQuery);
			if( result != null ) {
				Iterator<String> entries = result.keySet().iterator();
				while( entries.hasNext() ) {
					String entry = entries.next().toString();
					action.put( entry, result.get( entry ) );
				}			
			}
			else {
				Logs.LogEvent( "action not found " + actionid );
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			Logs.LogEvent( "action id " + actionid + " throws an error" );
		}
		return action;
	}
	
	public JSONArray getActions(String actionids) {
		JSONArray results = new JSONArray();
		JSONArray myActions = new JSONArray( actionids );
		for( int i = 0; i < myActions.length(); i ++ ) {
			results.put( getActionById(myActions.getString(i) ) );
		}

		return results;
	}

	public JSONObject readActionSettings( JSONObject configData ) {
		JSONObject replyData = new JSONObject();
		DBCollection coll = mDB.getCollection("actionconfig");
		try {
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put("_id", new ObjectId( configData.getString( "_id" ) ) );
			DBObject result = coll.findOne(searchQuery);
			if( result != null ) {
				Iterator<String> entries = result.keySet().iterator();
				while( entries.hasNext() ) {
					String entry = entries.next().toString();
					if( entry.equals( "_id" ) == false ) {
						replyData.put( entry, result.get( entry ) );
					}
					else {
						replyData.put( "_id", result.get( entry ).toString() );
					}
				}
			}
		}
		catch( Exception e ) {
			e.printStackTrace();
		}
		return replyData;
	}
	
	public JSONObject insertGameAction( String id, JSONObject configData ) {
		JSONObject newGameAction = new JSONObject();
		for( int i = 0; i < ContainerParams.myActionFields.length; i ++ ) {
			String field = ContainerParams.myActionFields[i];
			newGameAction.put( field, configData.get( field ) );
		}
		newGameAction.put( "gameid", configData.get( "gameid" ) );
		
		String myKey = science.db.actions.createGameAction( newGameAction );
		if( myKey.length() > 0 ) {
			newGameAction.put( "_id",  myKey );
		}
		else {
			return new JSONObject();
		}
		return newGameAction;
	}
	
	public String writeActionSettings( JSONObject configData ) {
		DBCollection coll = mDB.getCollection("actionconfig");
		try {
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put("_id", new ObjectId( configData.getString( "_id" ) ) );
			DBObject result = coll.findOne(searchQuery);
			if( result != null ) {
				for( int i = 0; i < ContainerParams.myActionFields.length; i ++ ) {
					String field = ContainerParams.myActionFields[i];
					if( configData.has( field ) ) {
						result.put( field, configData.getString( field ) );
					}
					else {
						return "missing setting for  " + field + " on action " + configData.getString( "_id" );
					}
				}
				coll.save( result );
				Logs.LogEvent( "writing " + result.toString() + " to settings " );
				return null;
			}
		}
		catch( Exception e ) {
			e.printStackTrace();
		}
		return "could not write action " + configData.getString( "_id" );
	}
	
}
