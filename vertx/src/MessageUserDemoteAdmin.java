import org.json.JSONObject;
import org.vertx.java.core.buffer.Buffer;
import org.vertx.java.core.http.HttpServerRequest;

public class MessageUserDemoteAdmin {
	private String validateMessage( JSONObject jsonRequestBody )
	{
		if( jsonRequestBody.has("adminToken") ) {
			if( jsonRequestBody.has("details") ) {
				return null;
			}
			else {
				return "Missing account details";
			}
		}
		else {
			return "Missing admin token";
		}
	}

	public MessageUserDemoteAdmin( HttpServerRequest req, Buffer body )  {
		Logs.LogEvent("Demote Admin User" );
		String errorInfo = null;
		try { 
			JSONObject jsonRequest = new JSONObject( body.toString() ); 
			JSONObject jsonReplyBody = new JSONObject();
			JSONObject jsonRequestBody = jsonRequest.getJSONObject( "body" );
			if( jsonRequestBody.has( "_t") && jsonRequestBody.getString( "_t" ).equals( "admin.demote" ) ) {
				errorInfo = validateMessage( jsonRequestBody );
				if( errorInfo == null ) {
					String accountToken = jsonRequestBody.get( "adminToken" ).toString();
					JSONObject details = jsonRequestBody.getJSONObject("details");
					if( science.db.accounts.validateAccountTokenByType( accountToken, "admin" ) == true ) {
						JSONObject account = science.db.accounts.searchUserInfo( accountToken, "admin", details );
						if( account != null && account.length() > 0 ) {
							jsonReplyBody.put( "_t", "admin.demoted" );
							jsonReplyBody.put( "account", account );
						}
						else {
							errorInfo = "DB Failure";
						}
					}
					else {
						errorInfo = "DB Failure";
					}
				}
			}
			if( errorInfo != null && errorInfo.length() > 0 ) {
				req.response().setStatusCode(400);
				Logs.LogEvent( errorInfo );
				jsonReplyBody.put( "_t", "admin.demote_error" );
				jsonReplyBody.put( "info", errorInfo );
			}
			JSONObject jsonReply = new JSONObject().put( "_t", "msg" );
			JSONObject jsonHeader = new JSONObject();
			jsonHeader.put( "_t", "hdr" );
			jsonHeader.put( "cid", "0" );
			jsonReply.put( "header", jsonHeader );
			jsonReply.put( "body", jsonReplyBody );
			Logs.LogEvent( "JSON Reply is " + jsonReply.toString() );
			req.response().headers().set("Content-Type", "application/json; charset=UTF-8");
			req.response().end(jsonReply.toString());
		}
		catch( org.json.JSONException e ) {
			e.printStackTrace();
			Logs.LogEvent( "JSON error is " + errorInfo );
			req.response().setStatusCode(400);
			req.response().headers().set("Content-Type", "application/json; charset=UTF-8");
			req.response().end( "{ jsonfailure: true }");
		}
	}

}
