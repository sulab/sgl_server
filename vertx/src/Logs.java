import org.vertx.java.core.logging.Logger;


public class Logs {

	static Logger myLogger = null;
	
	public static void initialize( Logger logger )  {
		LogEvent("Initializing Logging" );
		myLogger = logger;
	}

	public static void LogEvent( String myEvent )
	{
		if( myLogger != null ) {
			myLogger.info( myEvent );
		}
		else {
			System.out.println( myEvent );
		}
	}
}
