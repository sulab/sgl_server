import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;

public class ContainerDeveloperServer {
	public static String[] mySettingFields = {
			"createdeveloperaccount",
			"logindeveloperaccount",
			"confirmdevelopertoken",
			"updatedeveloperaccount",
			"secretcode"
			};
	
	public static String httpPostDeveloperMessage( String myUrl, String contents ) {
		String result = "";
		byte[] out = contents.getBytes(StandardCharsets.UTF_8);
		int length = out.length;
		HttpURLConnection http;
		try {
			URL url = new URL(myUrl);
			URLConnection con = url.openConnection();
			http = (HttpURLConnection)con;
			http.setRequestMethod("POST"); // PUT is another valid option
			http.setDoOutput(true);
			
			http.setFixedLengthStreamingMode(length);
			http.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
			http.connect();
			try(OutputStream os = http.getOutputStream()) {
			    os.write(out);
			}
			catch( Exception e ) {
				Logs.LogEvent( "Exception in post output! " + e.toString() );
				e.printStackTrace();				
			}
			InputStream is = url.openStream();
			int i;
			char c;
	  		try {
				 while((i=is.read())!=-1)
				 {
					// converts integer to character
					c=(char)i;
					result += c;
				 }
			 } finally {
			  is.close();
			}		
			Logs.LogEvent( "received reply " + result + " from " + myUrl );
		}
		catch( Exception e ) {
			Logs.LogEvent( "Exception in post open connection! " + e.toString() );
			e.printStackTrace();
		}
		return result;
	}
	
	public static String httpGetDeveloperMessage( String url ) {
		return sendMessage( url );
	}
	
	public static String sendMessage( String myUrl ) {
		URL url;
		String result = "";

		try {
			url = new URL( myUrl );
			InputStream is = url.openStream();
			int i;
			char c;
	  		try {
				 while((i=is.read())!=-1)
				 {
					// converts integer to character
					c=(char)i;
					result += c;
				 }
			 } finally {
			  is.close();
			}		
			Logs.LogEvent( "received reply " + result + " from " + myUrl );
		} catch ( Exception e ) {
			Logs.LogEvent( "Exception! " + e.toString() );
			e.printStackTrace();
		}
		return result;
	}
}
