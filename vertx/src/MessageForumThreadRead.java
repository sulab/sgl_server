import org.json.JSONArray;
import org.json.JSONObject;
import org.vertx.java.core.buffer.Buffer;
import org.vertx.java.core.http.HttpServerRequest;

public class MessageForumThreadRead {
	private String validateMessage( JSONObject jsonRequestBody ) {
		if( jsonRequestBody.has("accounttoken") ) {
			if( jsonRequestBody.has("forumid") ) {
				if( jsonRequestBody.has("threadid") ) {
					return null;
				}
				else {
					return "Missing threadId";
				}
			}
			else {
				return "Missing forumId";
			}
		}
		else {
			return "Missing account token";
		}
	}

	public MessageForumThreadRead( HttpServerRequest req, Buffer body )  {
		String errorInfo = null;
		try { 
			JSONObject jsonRequest = new JSONObject( body.toString() ); 
			JSONObject jsonReplyBody = new JSONObject();
			JSONObject jsonRequestBody = jsonRequest.getJSONObject( "body" );
			if( jsonRequestBody.has( "_t") && jsonRequestBody.getString( "_t" ).equals( "forumthread.get" ) ) {
				errorInfo = validateMessage( jsonRequestBody );
				if( errorInfo == null ) {
					String accountToken = jsonRequestBody.getString( "accounttoken" );
					String forumId = jsonRequestBody.getString( "forumid" );
					String threadId = jsonRequestBody.getString( "threadid" );
					String privileges = science.db.accounts.getAccountPrivs( accountToken );
					int postStart = 0;
					int postCount = 0;
					if( jsonRequestBody.has( "postcount" ) ) {
						postCount = Integer.parseInt( jsonRequestBody.getString( "postcount" ) );
					}
					if( jsonRequestBody.has( "poststart" ) ) {
						postStart = Integer.parseInt( jsonRequestBody.getString( "poststart" ) );
					}
					// String[] properties = { "accounttoken" };
					// science.analytics.trackEvent( accountToken, "Write Settings", timeStamp, FieldMapper.getFields(jsonRequestBody, properties) );
					JSONArray threads = science.db.threads.getThreadContents( privileges, forumId, threadId, postStart, postCount );
					jsonReplyBody.put( "_t", "forumthread.get" );
					jsonReplyBody.put( "title", science.db.threads.getThreadTitle( privileges, threadId ) );
					if( science.db.threads.isLockedThread( threadId ) ) {
						jsonReplyBody.put( "lock", "1" );
					}
					jsonReplyBody.put( "threads", threads );
				}
			}
			else {
				errorInfo = "Missing _t";
			}
			if( errorInfo != null && errorInfo.length() > 0 ) {
				req.response().setStatusCode(400);
				Logs.LogEvent( errorInfo );
				jsonReplyBody.put( "_t", "forumthread.get_error" );
				jsonReplyBody.put( "info", errorInfo );
			}
			JSONObject jsonReply = new JSONObject().put( "_t", "msg" );
			JSONObject jsonHeader = jsonRequest.getJSONObject( "header" );
			jsonReply.put( "header", jsonHeader );
			jsonReply.put( "body", jsonReplyBody );
			Logs.LogEvent( "JSON Reply is " + jsonReply.toString() );
			req.response().headers().set("Content-Type", "application/json; charset=UTF-8");
			req.response().end(jsonReply.toString());
		}
		catch( org.json.JSONException e ) {
			e.printStackTrace();
			Logs.LogEvent( "JSON error is " + errorInfo );
			req.response().setStatusCode(400);
			req.response().end( "{ jsonfailure: true }");
		}
	}

}
