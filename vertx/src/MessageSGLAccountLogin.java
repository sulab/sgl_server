import org.json.JSONObject;
import org.vertx.java.core.buffer.Buffer;
import org.vertx.java.core.http.HttpServerRequest;

public class MessageSGLAccountLogin {
	private String validateMessage( JSONObject jsonRequestBody )
	{
		if( jsonRequestBody.has("accountid") ) {
			if( jsonRequestBody.has("accountpassword") ) {
				return null;
			}
			else {
				return "Missing account Password";
			}
		}
		else {
			return "Missing account Name";
		}
	}

	public MessageSGLAccountLogin( HttpServerRequest req, Buffer body )  {
		Logs.LogEvent("Login SGL Developer Web User" );
		String errorInfo = null;
		try { 
			JSONObject jsonRequest = new JSONObject( body.toString() ); 
			JSONObject jsonReplyBody = new JSONObject();
			JSONObject jsonRequestBody = jsonRequest.getJSONObject( "body" );
			errorInfo = validateMessage( jsonRequestBody );
			if( jsonRequestBody.has( "_t") && jsonRequestBody.getString( "_t" ).equals( "sglaccount.login" ) ) {
				String developerId = "";
				String developerSecret = "";
				if( jsonRequestBody.has("developerid") ) {
					developerId = jsonRequestBody.getString( "developerid" );
				}
				else {
					errorInfo = "Missing developer id";					
				}
				if( jsonRequestBody.has("developersecret") ) {
					developerSecret = jsonRequestBody.getString( "developersecret" );
				}
				else {
					errorInfo = "Missing developer id";					
				}
				if( science.db.validateDeveloperSecret( developerId, developerSecret ) ) {
					if( errorInfo == null ) {
						String accountId = jsonRequestBody.get( "accountid" ).toString();
						String accountPassword = jsonRequestBody.get( "accountpassword" ).toString();
						String timeStamp = "";
						Logs.LogEvent("Login Web User exists" );
						if( science.db.accounts.accountExists( accountId ) == true ) {
							JSONObject accountToken = science.db.accounts.loginUserWeb( accountId, accountPassword );
							if( accountToken != null && accountToken.length() > 0 ) {
								String[] properties = { "accountid" };
								science.analytics.trackEvent( accountId, "Web Login", timeStamp, FieldMapper.getFields(jsonRequestBody, properties) );
								jsonReplyBody.put( "_t", "sglaccount.login" );
								jsonReplyBody.put( "account", accountToken );
							}
							else {
								errorInfo = "DB Failure";
							}
						}
						else {
							errorInfo = "DB Failure";
						}
					}
				}
				else {
					errorInfo = "verify your developer credentials";
				}
			}
			else {
				errorInfo = "missing _t of sglaccount.login";
			}
			if( errorInfo != null && errorInfo.length() > 0 ) {
				req.response().setStatusCode(400);
				Logs.LogEvent( errorInfo );
				jsonReplyBody.put( "_t", "sglaccount.change_error" );
				jsonReplyBody.put( "info", errorInfo );
			}
			JSONObject jsonReply = new JSONObject().put( "_t", "msg" );
			JSONObject jsonHeader = new JSONObject();
			jsonHeader.put( "_t", "hdr" );
			jsonHeader.put( "cid", "0" );
			jsonReply.put( "header", jsonHeader );
			jsonReply.put( "body", jsonReplyBody );
			Logs.LogEvent( "JSON Reply is " + jsonReply.toString() );
			req.response().headers().set("Content-Type", "application/json; charset=UTF-8");
			req.response().end(jsonReply.toString());
		}
		catch( org.json.JSONException e ) {
			e.printStackTrace();
			Logs.LogEvent( "JSON error is " + errorInfo );
			req.response().setStatusCode(400);
			req.response().headers().set("Content-Type", "application/json; charset=UTF-8");
			req.response().end( "{ jsonfailure: true }");
		}
	}

}
