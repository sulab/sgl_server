import org.json.JSONObject;
import org.vertx.java.core.http.HttpServerRequest;

public class MessageAccountValidateGet {
	public MessageAccountValidateGet ( HttpServerRequest req, String[] myArguments )  {
		try { 
			String token = "";
			for( int i = 0; i < myArguments.length; i ++ ) {
				if( myArguments[i].contains( "token=") ) {
					token = myArguments[i].split("=")[1];
				}
			}
			boolean validation = science.db.accounts.validateAccount( token );
			if( validation ) {
				String timeStamp = "";
				String activity = "";
				String gameId = "0";
				JSONObject details;
				details = new JSONObject();
				activity = "register";
				if( science.db.actions.createActivity( token, activity, gameId, details ) == true ) {
					String[] properties = { "accounttoken", "name" };
					JSONObject myActivity = new JSONObject();
					myActivity.put( "accounttoken", token );
					myActivity.put( "name", activity );
					science.analytics.trackEvent( token, "Activity", timeStamp, FieldMapper.getFields(myActivity, properties ) );
				}
			}
			
			req.response().end( "" + validation );
		}
		catch( org.json.JSONException e ) {
			e.printStackTrace();
			req.response().setStatusCode(400);
			req.response().end( "{ jsonfailure: true }");
		}
	}

}
