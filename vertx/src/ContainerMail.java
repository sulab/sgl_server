import java.io.InputStream;
import java.net.URL;

public class ContainerMail {
	static public void sendMail( String email, String template, String params ) {
		URL url;
		try {
			String myUrl;
			String prefix;
			if( ContainerConfig.getProperty( "SSL" ).equals( "0" ) == false ) { 
				System.setProperty("jsse.enableSNIExtension", "false");
				prefix = "https://";
			}
			else {
				prefix = "http://";
			}
			myUrl = prefix + ContainerConfig.getProperty( "site") + ContainerConfig.getProperty( "mailerphp") + "?token=cheeseybread&email="+email+"&template="+template;
			if( params != null && params.length() > 0 ) {
				myUrl += "&params="+params;
			}
			url = new URL( myUrl );
			InputStream is = url.openStream();
			int i;
			char c;
	  		try {
				 while((i=is.read())!=-1)
				 {
					// converts integer to character
					c=(char)i;
				 }
			 } finally {
			  is.close();
			}		
		} catch ( Exception e ) {
			Logs.LogEvent( "Exception! " + e.toString() );
			e.printStackTrace();
		}		
	}
}
