import org.json.JSONObject;
import org.vertx.java.core.buffer.Buffer;
import org.vertx.java.core.http.HttpServerRequest;

public class MessageItemSearch {
	private String validateMessage( JSONObject jsonRequestBody ) {
		if( jsonRequestBody.has("accounttoken") ) {
			if( jsonRequestBody.has("search") ) {
				if( jsonRequestBody.has("details")) {
					return null;
				}
				else {
					return "Missing details";
				}
			}
			else {
				return "Missing search type";
			}
		}
		else {
			return "Missing account token";
		}
	}

	public MessageItemSearch( HttpServerRequest req, Buffer body )  {
		String errorInfo = null;
		try { 
			JSONObject jsonRequest = new JSONObject( body.toString() ); 
			JSONObject jsonReplyBody = new JSONObject();
			JSONObject jsonRequestBody = jsonRequest.getJSONObject( "body" );
			if( jsonRequestBody.has( "_t") && jsonRequestBody.getString( "_t" ).equals( "info.search" ) ) {
				String privileges = "";
				errorInfo = validateMessage( jsonRequestBody );
				if( errorInfo == null ) {
					String searchType = jsonRequestBody.getString( "search" );
					String accountToken = jsonRequestBody.getString( "accounttoken" );
					JSONObject detail = null;
					detail = jsonRequestBody.getJSONObject( "details" );
					if( detail.has("field") ) {
						if( detail.has("value")) {
							if( science.db.accounts.validateAccountTokenByType( accountToken, "admin" ) == true ) {
								privileges = "admin";
							}
							else if( science.db.accounts.validateAccountTokenByType( accountToken, "dev" ) == true ) {
								privileges = "dev";
							}
							else if( science.db.accounts.validateAccountTokenByType( accountToken, "user" ) == true ) {
								privileges = "user";
							}
							else if( science.db.accounts.validateAccountTokenByType( accountToken, "anon" ) == true ) {
								privileges = "anon";
							}
							// String[] properties = { "accounttoken" };
							// science.analytics.trackEvent( accountToken, "Write Settings", timeStamp, FieldMapper.getFields(jsonRequestBody, properties) );
							JSONObject settings = science.db.searchInfo( accountToken, privileges, searchType, detail );
							jsonReplyBody.put( "_t", "info.search" );
							jsonReplyBody.put( "result", settings );
						}
					}
					else {
						errorInfo = "no search value provided in details";
					}
				}
				else {
					errorInfo = "no field provided in details";
				}
			}
			else {
				errorInfo = "Missing _t";
			}
			if( errorInfo != null && errorInfo.length() > 0 ) {
				req.response().setStatusCode(400);
				Logs.LogEvent( errorInfo );
				jsonReplyBody.put( "_t", "info.search_error" );
				jsonReplyBody.put( "info", errorInfo );
			}
			JSONObject jsonReply = new JSONObject().put( "_t", "msg" );
			JSONObject jsonHeader = jsonRequest.getJSONObject( "header" );
			jsonReply.put( "header", jsonHeader );
			jsonReply.put( "body", jsonReplyBody );
			Logs.LogEvent( "JSON Reply is " + jsonReply.toString() );
			req.response().headers().set("Content-Type", "application/json; charset=UTF-8");
			req.response().end(jsonReply.toString());
		}
		catch( org.json.JSONException e ) {
			e.printStackTrace();
			Logs.LogEvent( "JSON error is " + errorInfo );
			req.response().setStatusCode(400);
			req.response().end( "{ jsonfailure: true }");
		}
	}

}
