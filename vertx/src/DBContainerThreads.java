import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.bson.types.ObjectId;
import org.json.JSONArray;
import org.json.JSONObject;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

public class DBContainerThreads {
	private DB mDB;

	public DBContainerThreads() {
	}

	public void initialize( DB db )  {
		Logs.LogEvent("Initializing Thread DB");
		mDB = db;
	}

	public String sanitizeContent( String content ) {
		return content;
	}
	
	public void markBannedCommentsHidden( String ownerId ) {
		
	}
	
	public void hideComment( String userToken, String threadId, String commentId, String setting ) {
		
	}
	
	public boolean  makeThreadSticky( String userToken, String forumId, String threadId, String setting ) {
		String privileges = science.db.accounts.getAccountPrivs( userToken );
		if( privileges.equals( "admin" ) == false) {
			if( privileges.equals( "dev" ) == false ) {
				return false;
			}
			else {
				if( science.db.forums.isForumOwner( userToken, forumId ) == false ) {
					return false;
				}
			}
		}
		DBCollection coll = mDB.getCollection("threaddata");
		try {
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put( "_id", new ObjectId( threadId ) );
			DBObject result = coll.findOne(searchQuery);
			if( result != null ) {
				if( setting.equals( "1" ) == true ) {
					result.put( "sticky",  "1" );
					coll.save( result );
					return true;
				}
				else {
					if( result.keySet().contains( "sticky" ) ) {
						result.removeField( "sticky" );
						coll.save( result );
						return true;
					}
				}
			}
		}
		catch( Exception e ) {
			e.printStackTrace();
		}

		return false;
	}
	
	public JSONObject addThreadContent( String privileges, String forumId, String threadId, String ownerId, String content ) {
		DBCollection coll = mDB.getCollection("threadcontentdata");
		content = sanitizeContent( content );
		if( isLockedThread( threadId ) == false ) {
			BasicDBObject newThreadContent = new BasicDBObject();
			newThreadContent.put( "forumId", forumId );
			newThreadContent.put( "posterId", ownerId );
			newThreadContent.put( "threadId", threadId );
			newThreadContent.put( "state", "visible" );
			newThreadContent.put( "content", content );
			newThreadContent.put( "date", new Date() );
			coll.insert( newThreadContent );
			newThreadContent.put( "id", newThreadContent.get( "_id" ).toString() );
			newThreadContent.remove( "_id" );
			newThreadContent.put( "date", newThreadContent.get( "date" ).toString() );
			return new JSONObject( newThreadContent.toString() );
		}
		JSONObject response = new JSONObject();
		response.put( "locked", "1"); 
		return response;
	}
	
	public JSONObject newThreadForForum( String privileges, String forumId, String ownerId, String title, String content ) {
		JSONObject response = new JSONObject();
		DBCollection coll = mDB.getCollection("threaddata");
		BasicDBObject newThread = new BasicDBObject();
		newThread.put( "forumId", forumId );
		newThread.put( "ownerId", ownerId );
		newThread.put( "state", "visible" );
		newThread.put( "title", title );
		newThread.put( "date", new Date() );
		coll.insert( newThread );
		String myId = newThread.get( "_id" ).toString();
		response.put( "threadcontent", addThreadContent( privileges, forumId, myId, ownerId, content ) );
		newThread.put( "id", newThread.get( "_id" ).toString() );
		newThread.remove( "_id" );
		newThread.put( "date", newThread.get( "date" ).toString() );
		response.put( "thread", new JSONObject( newThread.toString() ) );
		return response;
	}
	
	public String getThreadTitle( String privileges, String threadId ) {
		String response = "";
		DBCollection coll = mDB.getCollection("threaddata");
		try {
			BasicDBObject searchQuery = new BasicDBObject();
			if( privileges.equals( "admin" ) == false ) {
				List<BasicDBObject> obj = new ArrayList<BasicDBObject>();
				obj.add(new BasicDBObject("_id", new ObjectId( threadId )));
				obj.add(new BasicDBObject("state", "visible"));
				searchQuery.put("$and", obj);
			}
			else {
				searchQuery.put("_id", new ObjectId( threadId ));
			}
			DBObject result = coll.findOne(searchQuery);
			if( result != null ) {
				response = result.get( "title" ).toString();
			}
		}
		catch( Exception e ) {
			e.printStackTrace();
		}
		return response;
	}
	
	boolean isLockedThread( String threadId ) {
		DBCollection coll = mDB.getCollection("threaddata");
		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put( "_id", new ObjectId( threadId ) );
		DBObject result = coll.findOne(searchQuery);
		if( result != null ) {
			if( result.keySet().contains( "lock" ) ) {
				return true;
			}
		}
		return false;
	}
	
	JSONObject updateThread( String accountToken, String privileges, String threadId, String state ) {
		JSONObject response = new JSONObject();
		if( privileges.equals( "admin" ) ) {
			DBCollection coll = mDB.getCollection("threaddata");
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put( "_id", new ObjectId( threadId ) );
			DBObject result = coll.findOne(searchQuery);
			if( result != null ) {
				if( state.equals( "lock" ) ) {
					result.put( "lock", "1" );
					coll.save( result );
				}
				else if( state.equals( "unlock" ) ) {
					if( result.keySet().contains( "lock" ) ) {
						result.removeField( "lock" );
						coll.save( result );
					}
				}
				else {
					if( result.get( "state" ).toString().equals( state ) == false ) {
						result.put( "state", state );
						coll.save( result );
					}
				}
				if( result.keySet().contains( "lock" ) ) {
					response.put( "lock",  result.get("lock") );
				}
				response.put( "title", result.get( "title" ) );
				response.put( "description", result.get( "description" ) );
				response.put( "categoryId",  result.get( "categoryId" ) );
				response.put( "state",  result.get( "state" ) );
				response.put( "id",  result.get( "_id" ).toString() );
			}		
		}
		return response;
	}
	 /* 
					if( state.equals( "visible" ) || state.equals( "hidden" ) ||  ) {
						threadId = jsonRequestBody.getString( "threadid" );
						JSONObject thread = science.db.threads.updateThread( accountToken, privileges, threadId, state );
	 */
	
	public JSONArray getThreadContents( String privileges, String forumId, String threadId, int postStart, int postCount ) {
		DBCollection coll = mDB.getCollection("threadcontentdata");
		JSONArray response = new JSONArray();
		try {
			BasicDBObject searchQuery = new BasicDBObject();
			List<BasicDBObject> obj = new ArrayList<BasicDBObject>();
			obj.add(new BasicDBObject("forumId", forumId));
			obj.add(new BasicDBObject("threadId", threadId));
			searchQuery.put("$and", obj);
			DBCursor result = coll.find(searchQuery).sort(new BasicDBObject("date", 1));
			if( postStart > 0 ) {
				result.skip( postStart );
			}
			if( postCount == 0 ) {
				response.put( "" + result.count() );
			}
			for( int i = 0; i < postCount; i ++ ) {
				if( result.hasNext() ) {
					DBObject threadContent = result.next();
					threadContent.put( "id", threadContent.get( "_id" ).toString() );
					threadContent.removeField( "_id");
					response.put( threadContent );
				}
			}
		}
		catch( Exception e ) {
			e.printStackTrace();
		}
		return response;
	}

	public JSONArray getThreadsForForum( String privileges, String forumId, int threadStart, int threadCount ) {
		JSONArray response = new JSONArray();
		DBCollection coll = mDB.getCollection("threaddata");
		Logs.LogEvent( "getting threads " + threadCount + " for forum "  + forumId );
		try {
			BasicDBObject searchQuery = new BasicDBObject();
			if( privileges.equals( "admin" ) == false ) {
				List<BasicDBObject> obj = new ArrayList<BasicDBObject>();
				obj.add(new BasicDBObject("forumId", forumId));
				obj.add(new BasicDBObject("state", "visible"));
				searchQuery.put("$and", obj);
			}
			else {
				searchQuery.put("forumId", forumId);
			}
			DBCursor result = coll.find(searchQuery);
			if( threadStart > 0 ) {
				result.skip( threadStart );
			}
			if( threadCount == 0 ) {
				response.put( "" + result.count() );
			}
			for( int i = 0; i < threadCount; i ++ ) {
				if( result.hasNext() ) {
					DBObject forum = result.next();
					forum.put( "id", forum.get( "_id" ).toString() );
					forum.put( "title", forum.get( "title" ).toString() );
					forum.put( "content",  getThreadContents( privileges, forumId, forum.get( "_id" ).toString(), 0, 10 ) );
					forum.removeField( "_id");
					response.put( forum );
				}
			}
		}
		catch( Exception e ) {
			e.printStackTrace();
		}
		return response;
	}
}
