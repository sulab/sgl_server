import org.vertx.java.core.Handler;
import org.vertx.java.core.http.HttpServer;
import org.vertx.java.core.http.HttpServerRequest;
import org.vertx.java.core.logging.Logger;
import org.vertx.java.platform.Verticle;
import org.vertx.java.core.buffer.Buffer;

public class science extends Verticle {

	public static DBContainer db;
	public static ContainerAnalytics analytics;
	public static Logs logging;
	public static ContainerConfig config;
	
	private void handlePostRequest( HttpServerRequest req, Buffer body ) {
		String[] urlsegments = req.uri().split( "/" );
		String[] urltype = urlsegments[urlsegments.length-1].split( "\\?" );
		urltype[0] = urltype[0].toUpperCase();
		science.db.putMessage( body );		 
		switch( urltype[0] ) {
			case "GAMECREATE":
				MessageGameCreate messageGameCreate = new MessageGameCreate( req, body );
				break;
			case "GAMECHANGESTATE":
				MessageGameChangeState messageGameChangeState = new MessageGameChangeState( req, body );
				break;
			case "MOBILEUSERCREATE":
				MessageUserCreateMobile messageCreateMobileUser = new MessageUserCreateMobile( req, body );
				break;
			case "WEBUSERRESENDTOKEN":
				MessageUserResendWeb messageResendWebUser = new MessageUserResendWeb( req, body );
				break;
			case "WEBUSERLOSTPASSWORD":
				MessageUserLostPassword messageLostPasswordUser = new MessageUserLostPassword( req, body );
				break;
			case "ANONUSERCREATE":
				MessageUserCreateAnon messageCreateAnonUser = new MessageUserCreateAnon( req, body );
				break;
			case "ANONUSERLOGIN":
				MessageUserLoginAnon messageLoginAnonUser = new MessageUserLoginAnon( req, body );
				break;
			case "OAUTHVALIDATE":
				MessageOAuthValidate messageOAuthValidate = new MessageOAuthValidate( req, body );
				break;
			case "OAUTHUPDATE":
				MessageOAuthUpdate messageOAuthUpdate = new MessageOAuthUpdate( req, body );
				break;
			case "WEBUSERCREATE":
				MessageUserCreateWeb messageCreateWebUser = new MessageUserCreateWeb( req, body );
				break;
			case "ACTIONCREATE":
				MessageActionCreate messageActionCreate = new MessageActionCreate( req, body );
				break;
			case "ACTIVITYCREATE":
				MessageActivityCreate messageCreateActivity = new MessageActivityCreate( req, body );
				break;
			case "ACTIVITYREAD":
				MessageActivityRead messageReadActivity = new MessageActivityRead( req, body );
				break;
			case "QUESTCREATE":
				MessageQuestCreate messageQuestCreate = new MessageQuestCreate( req, body );
				break;
			case "LEADERBOARDPOST":
				MessageLeaderboardPost messageLeaderboardPost = new MessageLeaderboardPost( req, body );
				break;
			case "LEADERBOARDGET":
				MessageLeaderboardGet messageLeaderboardGet = new MessageLeaderboardGet( req, body );
				break;
			case "WEBUSERLOGIN":
				MessageUserLoginWeb messageLoginWebUser = new MessageUserLoginWeb( req, body );
				break;
			case "WEBPARAMSGET":
				MessageParamsGet messageParamsGet = new MessageParamsGet( req, body );
				break;
			case "WEBPARAMSSET":
				MessageParamsSet messageParamsSet = new MessageParamsSet( req, body );
				break;
			case "WEBPARAMSCHANGE":
				MessageParamsChange messageParamsChange = new MessageParamsChange( req, body );
				break;
			case "ANONPROMOTE":
				MessageUserPromoteAnon messageAnonPromote = new MessageUserPromoteAnon( req, body );
				break;
			case "ADMINDEMOTE":
				MessageUserDemoteAdmin messageAdminDemote = new MessageUserDemoteAdmin( req, body );
				break;
			case "FORUMREAD":
				MessageForumRead messageForumRead = new MessageForumRead( req, body );
				break;
			case "FORUMUPDATE":
				MessageForumUpdate messageForumUpdate = new MessageForumUpdate( req, body );
				break;
			case "FORUMTHREADREAD":
				MessageForumThreadRead messageForumThreadRead = new MessageForumThreadRead( req, body );
				break;
			case "FORUMTHREADUPDATE":
				MessageForumThreadUpdate messageForumThreadUpdate = new MessageForumThreadUpdate( req, body );
				break;
			case "FORUMTHREADSTICKY":
				MessageForumThreadSticky messageForumThreadSticky = new MessageForumThreadSticky( req, body );
				break;
			case "FORUMTHREADCREATE":
				MessageForumThreadCreate messageForumThreadCreate = new MessageForumThreadCreate( req, body );
				break;
			case "FORUMTHREADREPLY":
				MessageForumThreadReply messageForumThreadReply = new MessageForumThreadReply( req, body );
				break;
			case "INFO":
				MessageItemInfo messageInfo = new MessageItemInfo( req, body );
				break;
			case "SEARCH":
				MessageItemSearch messageSearch = new MessageItemSearch( req, body );
				break;
			case "CREATESGLACCOUNT":
				MessageSGLAccountCreate messageSGLAccountCreate = new MessageSGLAccountCreate( req, body );
				break;
			case "LOGINSGLACCOUNT":
				MessageSGLAccountLogin messageSGLAccountLogin = new MessageSGLAccountLogin( req, body );
				break;
			case "UPDATESGLACCOUNT":
				MessageSGLAccountUpdate messageSGLAccountUpdate = new MessageSGLAccountUpdate( req, body );
				break;
			default:
				Logs.LogEvent( "Unknown Post From " + req.remoteAddress().getHostName() + " is " + req.absoluteURI() + " for " + urltype[0] );
				Logs.LogEvent( "Body is " + body.toString() );
				req.response().headers().set("Content-Type", "application/json; charset=UTF-8");
				req.response().end("{ maybeyou : forgotsomething }");
				break;
		}
		req.response().close();
	}
	
	private void handleMultiPartPost( HttpServerRequest req, Buffer body ) {
		String[] urlsegments = req.uri().split( "/" );
		String[] urltype = urlsegments[urlsegments.length-1].split( "\\?" );
		urltype[0] = urltype[0].toUpperCase();
		switch( urltype[0] ) {
			case "WEBUSERLOGIN":
				Logs.LogEvent("Got body for request: " + req.uri() + ": " + body.length() + " bytes" );
				Logs.LogEvent( "formAttributes " + req.formAttributes().names().toString());
				MessageUserLoginWebForm messageLoginWebUserForm = new MessageUserLoginWebForm( req, req.formAttributes().get("login-username").toString(), req.formAttributes().get("login-password").toString() );
				break;
			default:
				Logs.LogEvent( "Unknown Multipart Form Request From " + req.remoteAddress().getHostName() + " is " + req.absoluteURI() + " for " + urltype[0] );
				Logs.LogEvent( "Body is " + body.toString() );
				req.response().headers().set("Content-Type", "application/json; charset=UTF-8");
				req.response().end("{ maybeyou : forgotsomething }");
				break;
		}
		req.response().close();
	}

	private String[] getStrings( String myArgs ) {
		if( myArgs != null && myArgs.length() > 0 ) {
			if( myArgs.contains( "&" ) == true ) {
				return myArgs.split( "&" );
			}
			else {
				String[] result = { myArgs };
				return result;
			}
		}
		return null;
	}
	
	private boolean handleGetRequest( HttpServerRequest req ) {
		String[] urlsegments = req.uri().split( "/" );
		String[] urltype;
		if( req.uri().contains( "?" ) == true ) {
			urltype = urlsegments[urlsegments.length-1].split( "\\?" );
		}
		else {
			urltype = new String[] { urlsegments[urlsegments.length-1], null };
		}
		urltype[0] = urltype[0].toUpperCase();
		switch( urltype[0] ) {
			case "SCIENCEPING":
				MessagePingGet messagePingGet = new MessagePingGet( req, getStrings( urltype[1] ) );
				return true;
			case "ACCOUNTVALIDATE":
				MessageAccountValidateGet messageAccountValidateGet = new MessageAccountValidateGet( req, getStrings( urltype[1] ) );
				return true;
			case "ACCOUNTINVALIDATEPASSWORD":
				MessageAccountPasswordInvalidateGet messageAccountPasswordInvalidateGet = new MessageAccountPasswordInvalidateGet( req, getStrings( urltype[1] ) );
				return true;
			case "TOKENVALIDATE":
				MessageTokenValidateGet messageTokenValidateGet = new MessageTokenValidateGet( req, getStrings( urltype[1] ) );
				return true;
			case "SGLTOKENVALIDATE":
				MessageSGLTokenValidateGet messageSGLTokenValidateGet = new MessageSGLTokenValidateGet( req, getStrings( urltype[1] ) );
				return true;
			default:
				Logs.LogEvent( "Unknown Get From " + req.remoteAddress().getHostName() + " is " + req.absoluteURI() );
		}
		return false;
	}
	
	public void start() {
		ContainerConfig config = new ContainerConfig();
		config.initialize();
		
		logging = new Logs();
		Logger logger = container.logger();
		Logs.initialize( logger );
		
		db = new DBContainer();
		db.initialize();
		
		analytics = new ContainerAnalytics();
		analytics.initialize();
			
		HttpServer myServer = vertx.createHttpServer();
		myServer.setAcceptBacklog(1000);
		myServer.requestHandler(new Handler<HttpServerRequest>() {
			public void handle(final HttpServerRequest req) {
				try {
					req.response().headers().set("Access-Control-Allow-Origin", "*");
				    req.response().headers().set("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
				    req.response().headers().set("Access-Control-Allow-Headers", "Content-Type, Authorization");
					if( req.method().equals( "POST" ) ) {
						if( req.headers().get("Content-Type").toString().substring(0, 20).equals( "multipart/form-data;") ) {
							Logs.LogEvent("Receiving multipart request");
							req.expectMultiPart(true);
							req.bodyHandler(new Handler<Buffer>() {
								public void handle(Buffer body) {
									handleMultiPartPost( req, body );									
								}
							});
				
						}
						else {
							req.bodyHandler(new Handler<Buffer>() {
								public void handle(Buffer body) {
									Logs.LogEvent("Got request: " + req.uri() + ": " + body.length() + " bytes" );
									req.response().headers().set("Content-Type", "application/json; charset=UTF-8");
									handlePostRequest( req, body );
								}
							});
						}
					}
					else if( req.method().equals( "GET" ) ) {
						if( handleGetRequest( req ) == false ) {
							req.response().headers().set("Content-Type", "application/json; charset=UTF-8");
							req.response().end("{ mustadd : leveldatas }");
						}					
					}
					else {
						if( !req.method().equals("OPTIONS") ) {
							Logs.LogEvent("Got strange http: " + req.uri() + ": of type " + req.method() );
						}
						req.response().headers().set("Content-Type", "application/json; charset=UTF-8");
						req.response().end("{ nothing : toseehere }");					
					}
				}
				catch( Exception e ) {
					Logs.LogEvent("Got top level exception " + e.toString() );
					e.printStackTrace();
					req.response().end("{ exceptional : exception }");					
				}
			}
		});
		try { 
			if( ContainerConfig.getProperty( "SSL" ).equals( "0" ) == false ) { 
				myServer.setSSL(true);
				myServer.setKeyStorePassword("science");
				myServer.setKeyStorePath("keystore.jks");
			}
			myServer.listen(1492);
		}
		catch( Exception e ) {
			e.printStackTrace();
		}
	}
}
