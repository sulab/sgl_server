import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.bson.types.ObjectId;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

public class DBContainerOAuth {
	private DB mDB;

	public DBContainerOAuth() {
	}

	public void initialize( DB db )  {
		Logs.LogEvent("Initializing OAuth DB");
		mDB = db;
	}

	public String updateOAuthToken( String accountToken, String gameToken, String oauthToken ) {
		String response = "0";
		DBCollection coll = mDB.getCollection("oauth");
		try {
			BasicDBObject searchQuery = new BasicDBObject();
			List<BasicDBObject> obj = new ArrayList<BasicDBObject>();
			obj.add(new BasicDBObject("accountToken", accountToken));
			obj.add(new BasicDBObject("gameToken", gameToken));
			searchQuery.put("$and", obj);
			DBObject result = coll.findOne(searchQuery);
			if( result != null ) {
				result.put( "oauthToken", oauthToken );
				result.put( "upgradedDate", new Date() );
				coll.save( result );
				return oauthToken;
			}
			else {
				BasicDBObject oauth = new BasicDBObject();
				oauth.put( "accountToken", accountToken);
				oauth.put( "oauthToken", oauthToken );
				oauth.put( "gameToken", gameToken );
				oauth.put( "createdDate", new Date() );
				coll.insert( oauth );
				return oauthToken;
			}
		}
		catch( Exception e ) {
			e.printStackTrace();
		}
		return response;
	}

	public String validateOAuthToken( String accountToken, String gameToken ) {
		String response = "0";
		DBCollection coll = mDB.getCollection("oauth");
		try {
			BasicDBObject searchQuery = new BasicDBObject();
			List<BasicDBObject> obj = new ArrayList<BasicDBObject>();
			obj.add(new BasicDBObject("accountToken", accountToken));
			obj.add(new BasicDBObject("gameToken", gameToken));
			searchQuery.put("$and", obj);
			DBObject result = coll.findOne(searchQuery);
			if( result != null ) {
				response = result.get( "oauthToken" ).toString();
			}
		}
		catch( Exception e ) {
			e.printStackTrace();
		}
		return response;
	}
}
