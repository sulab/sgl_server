import org.json.JSONArray;
import org.json.JSONObject;
import org.vertx.java.core.buffer.Buffer;
import org.vertx.java.core.http.HttpServerRequest;

public class MessageForumRead {
	private String validateMessage( JSONObject jsonRequestBody ) {
		if( jsonRequestBody.has("accounttoken") ) {
			return null;
		}
		else {
			return "Missing account token";
		}
	}

	public MessageForumRead( HttpServerRequest req, Buffer body )  {
		String errorInfo = null;
		try { 
			JSONObject jsonRequest = new JSONObject( body.toString() ); 
			JSONObject jsonReplyBody = new JSONObject();
			JSONObject jsonRequestBody = jsonRequest.getJSONObject( "body" );
			if( jsonRequestBody.has( "_t") && jsonRequestBody.getString( "_t" ).equals( "forum.get" ) ) {
				errorInfo = validateMessage( jsonRequestBody );
				if( errorInfo == null ) {
					int threadCount = 0;
					int threadStart = 0;
					String accountToken = jsonRequestBody.getString( "accounttoken" );
					String forumId = "";
					String privileges = science.db.accounts.getAccountPrivs( accountToken );
					if( jsonRequestBody.has( "threadcount" ) ) {
						threadCount = Integer.parseInt( jsonRequestBody.getString( "threadcount" ) );
					}
					if( jsonRequestBody.has( "threadstart" ) ) {
						threadStart = Integer.parseInt( jsonRequestBody.getString( "threadstart" ) );
					}
					if( jsonRequestBody.has( "forumid" ) ) {
						forumId = jsonRequestBody.getString( "forumid" );
						JSONArray forum = science.db.forums.queryForum( privileges, forumId, threadStart, threadCount );
						jsonReplyBody.put( "forums", forum );
					}
					else {
						JSONArray categories = science.db.forums.queryForumCategories( privileges );
						JSONArray forums = science.db.forums.queryForums( privileges, threadCount );
						jsonReplyBody.put( "categories", categories );
						jsonReplyBody.put( "forums", forums );
					}
					// String[] properties = { "accounttoken" };
					// science.analytics.trackEvent( accountToken, "Write Settings", timeStamp, FieldMapper.getFields(jsonRequestBody, properties) );
					jsonReplyBody.put( "_t", "forum.get" );
				}
			}
			else {
				errorInfo = "Missing _t";
			}
			if( errorInfo != null && errorInfo.length() > 0 ) {
				req.response().setStatusCode(400);
				Logs.LogEvent( errorInfo );
				jsonReplyBody.put( "_t", "forum.get_error" );
				jsonReplyBody.put( "info", errorInfo );
			}
			JSONObject jsonReply = new JSONObject().put( "_t", "msg" );
			JSONObject jsonHeader = jsonRequest.getJSONObject( "header" );
			jsonReply.put( "header", jsonHeader );
			jsonReply.put( "body", jsonReplyBody );
			Logs.LogEvent( "JSON Reply is " + jsonReply.toString() );
			req.response().headers().set("Content-Type", "application/json; charset=UTF-8");
			req.response().end(jsonReply.toString());
		}
		catch( org.json.JSONException e ) {
			e.printStackTrace();
			Logs.LogEvent( "JSON error is " + errorInfo );
			req.response().setStatusCode(400);
			req.response().end( "{ jsonfailure: true }");
		}
	}

}
