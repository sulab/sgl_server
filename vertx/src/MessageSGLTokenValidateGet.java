import org.vertx.java.core.http.HttpServerRequest;

public class MessageSGLTokenValidateGet {
	public MessageSGLTokenValidateGet ( HttpServerRequest req, String[] myArguments )  {
		try { 
			String token = "";
			String secret = "";
			String developer = "";
			String privs = "user";
			boolean validation = false;
			for( int i = 0; i < myArguments.length; i ++ ) {
				if( myArguments[i].contains( "developer=") ) {
					developer = myArguments[i].split("=")[1];
				}
				if( myArguments[i].contains( "token=") ) {
					token = myArguments[i].split("=")[1];
				}
				if( myArguments[i].contains( "secret=") ) {
					secret = myArguments[i].split("=")[1];
				}
				if( myArguments[i].contains( "privs=") ) {
					privs = myArguments[i].split("=")[1];
				}
			}
			if( science.db.validateDeveloperSecret( developer, secret ) ) {
				validation = science.db.accounts.validateAccountTokenByType( token, privs );
			}
			req.response().end( "" + validation );
		}
		catch( org.json.JSONException e ) {
			e.printStackTrace();
			req.response().setStatusCode(400);
			req.response().end( "{ jsonfailure: true }");
		}
	}

}
