import org.json.JSONArray;
import org.json.JSONObject;
import org.vertx.java.core.buffer.Buffer;
import org.vertx.java.core.http.HttpServerRequest;

public class MessageActivityRead {
	public MessageActivityRead( HttpServerRequest req, Buffer body )  {
		Logs.LogEvent("Read Activity" );
		String errorInfo = null;
		try { 
			JSONObject jsonRequest = new JSONObject( body.toString() ); 
			JSONObject jsonReplyBody = new JSONObject();
			JSONObject jsonRequestBody = jsonRequest.getJSONObject( "body" );
			if( jsonRequestBody.has( "_t") && jsonRequestBody.getString( "_t" ).equals( "activity.read" ) ) {
				//String timeStamp = "";
				String accountToken = "";
				JSONArray activities;
				JSONObject params = null;
				if( jsonRequestBody.has("accounttoken") ) {
					accountToken = jsonRequestBody.getString( "accounttoken" );
				}
				else {
					errorInfo = "Missing account token";					
				}
				if( accountToken != null && accountToken.length() > 0 ) {
					if( science.db.accounts.validateUserAccountToken( accountToken ) == true ) {
						if( jsonRequestBody.has("params") ) {
							params = jsonRequestBody.getJSONObject( "params" );
						}
						activities = science.db.actions.readAccountActivities( accountToken, params );
						//String[] properties = { "accounttoken" };
						//science.analytics.trackEvent( accountToken, "Read Activities", timeStamp, FieldMapper.getFields(jsonRequestBody, properties) );
						jsonReplyBody.put( "_t", "activity.read" );
						jsonReplyBody.put( "activities", activities );
					}
					else {
						errorInfo = "token invalid";
					}
				}
			}
			else {
				errorInfo = "Missing _t";
			}
			if( errorInfo != null && errorInfo.length() > 0 ) {
				req.response().setStatusCode(400);
				Logs.LogEvent( errorInfo );
				jsonReplyBody.put( "_t", "activity.read_error" );
				jsonReplyBody.put( "info", errorInfo );
			}
			JSONObject jsonReply = new JSONObject().put( "_t", "msg" );
			JSONObject jsonHeader = jsonRequest.getJSONObject( "header" );
			jsonReply.put( "header", jsonHeader );
			jsonReply.put( "body", jsonReplyBody );
			Logs.LogEvent( "JSON Reply is " + jsonReply.toString() );
			req.response().end(jsonReply.toString());
		}
		catch( org.json.JSONException e ) {
			e.printStackTrace();
			Logs.LogEvent( "JSON error is " + errorInfo );
			req.response().setStatusCode(400);
			req.response().end( "{ jsonfailure: true }");
		}
	}
}
