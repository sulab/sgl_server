import org.json.JSONObject;
import org.vertx.java.core.buffer.Buffer;
import org.vertx.java.core.http.HttpServerRequest;

public class MessageGameChangeState {
	private String validateMessage( JSONObject jsonRequestBody ) {
		if( jsonRequestBody.has("accounttoken") ) {
			if( jsonRequestBody.has("gametoken") ) {
				if( jsonRequestBody.has("gamestate") ) {
					String accountToken = jsonRequestBody.getString( "accounttoken" );
					String gameToken = jsonRequestBody.getString( "gametoken" );
					String gameState = jsonRequestBody.getString( "gamestate" );
					if( science.db.games.validateGameChangeState( accountToken, gameToken, gameState ) ) {
						return null;
					}
					else {
						return "Unable to validate game for user and state";
					}
				}
				else {
					return "Missing game state";
				}
			}
			else {
				return "Missing game token";
			}
		}
		else {
			return "Missing account token";
		}
	}

	public MessageGameChangeState( HttpServerRequest req, Buffer body )  {
		String errorInfo = null;
		try { 
			JSONObject jsonRequest = new JSONObject( body.toString() ); 
			JSONObject jsonReplyBody = new JSONObject();
			JSONObject jsonRequestBody = jsonRequest.getJSONObject( "body" );
			if( jsonRequestBody.has( "_t") && jsonRequestBody.getString( "_t" ).equals( "game.changestate" ) ) {
				errorInfo = validateMessage( jsonRequestBody );
				if( errorInfo == null ) {
					String accountToken = jsonRequestBody.getString( "accounttoken" );
					String gameToken = jsonRequestBody.getString( "gametoken" );
					String gameState = jsonRequestBody.getString( "gamestate" );
					errorInfo = science.db.games.updateGameState( accountToken, gameToken, gameState );
					if( errorInfo == null ) {
						jsonReplyBody.put( "_t", "game.changestate" );
						jsonReplyBody.put( "write", "1" );
					}
				}
			}
			else {
				errorInfo = "Missing _t";
			}
			if( errorInfo != null && errorInfo.length() > 0 ) {
				req.response().setStatusCode(400);
				Logs.LogEvent( errorInfo );
				jsonReplyBody.put( "_t", "game.changestate_error" );
				jsonReplyBody.put( "info", errorInfo );
			}
			JSONObject jsonReply = new JSONObject().put( "_t", "msg" );
			JSONObject jsonHeader = jsonRequest.getJSONObject( "header" );
			jsonReply.put( "header", jsonHeader );
			jsonReply.put( "body", jsonReplyBody );
			Logs.LogEvent( "JSON Reply is " + jsonReply.toString() );
			req.response().headers().set("Content-Type", "application/json; charset=UTF-8");
			req.response().end(jsonReply.toString());
		}
		catch( org.json.JSONException e ) {
			e.printStackTrace();
			Logs.LogEvent( "JSON error is " + errorInfo );
			req.response().setStatusCode(400);
			req.response().end( "{ jsonfailure: true }");
		}
	}

}
