import org.json.JSONObject;
import org.vertx.java.core.buffer.Buffer;
import org.vertx.java.core.http.HttpServerRequest;

public class MessageUserLoginWeb {
	private String validateMessage( JSONObject jsonRequestBody )
	{
		if( jsonRequestBody.has("accountId") ) {
			if( jsonRequestBody.has("accountPassword") ) {
				return null;
			}
			else {
				return "Missing account Password";
			}
		}
		else {
			return "Missing account Name";
		}
	}

	public MessageUserLoginWeb( HttpServerRequest req, Buffer body )  {
		Logs.LogEvent("Login Web User" );
		String errorInfo = null;
		try { 
			JSONObject jsonRequest = new JSONObject( body.toString() ); 
			JSONObject jsonReplyBody = new JSONObject();
			JSONObject jsonRequestBody = jsonRequest.getJSONObject( "body" );
			errorInfo = validateMessage( jsonRequestBody );
			if( errorInfo == null ) {
				String accountId = jsonRequestBody.get( "accountId" ).toString();
				String accountPassword = jsonRequestBody.get( "accountPassword" ).toString();
				String timeStamp = "";
				Logs.LogEvent("Login Web User exists" );
				if( science.db.accounts.accountExists( accountId ) == true ) {
					JSONObject account = science.db.accounts.loginUserWeb( accountId, accountPassword );
					if( account != null && account.length() > 0 ) {
						String[] properties = { "accountid" };
						science.analytics.trackEvent( accountId, "Web Login", timeStamp, FieldMapper.getFields(jsonRequestBody, properties) );
						jsonReplyBody.put( "_t", "webuser.login" );
						jsonReplyBody.put( "account", account );
					}
					else {
						errorInfo = "DB Failure";
					}
				}
				else {
					errorInfo = "DB Failure";
				}
			}
			if( errorInfo != null && errorInfo.length() > 0 ) {
				req.response().setStatusCode(400);
				Logs.LogEvent( errorInfo );
				jsonReplyBody.put( "_t", "webuser.login_error" );
				jsonReplyBody.put( "info", errorInfo );
			}
			JSONObject jsonReply = new JSONObject().put( "_t", "msg" );
			JSONObject jsonHeader = new JSONObject();
			jsonHeader.put( "_t", "hdr" );
			jsonHeader.put( "cid", "0" );
			jsonReply.put( "header", jsonHeader );
			jsonReply.put( "body", jsonReplyBody );
			Logs.LogEvent( "JSON Reply is " + jsonReply.toString() );
			req.response().headers().set("Content-Type", "application/json; charset=UTF-8");
			req.response().end(jsonReply.toString());
		}
		catch( org.json.JSONException e ) {
			e.printStackTrace();
			Logs.LogEvent( "JSON error is " + errorInfo );
			req.response().setStatusCode(400);
			req.response().headers().set("Content-Type", "application/json; charset=UTF-8");
			req.response().end( "{ jsonfailure: true }");
		}
	}

}
