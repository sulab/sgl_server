import org.json.JSONObject;
import org.vertx.java.core.buffer.Buffer;
import org.vertx.java.core.http.HttpServerRequest;

public class MessageParamsGet {
	private String validateMessage( JSONObject jsonRequestBody ) {
		if( jsonRequestBody.has("accounttoken") ) {
			if( jsonRequestBody.has("params") ) {
				return null;
			}
			else {
				return "Missing params";
			}
			
		}
		else {
			return "Missing account token";
		}
	}

	public MessageParamsGet( HttpServerRequest req, Buffer body )  {
		String errorInfo = null;
		try { 
			JSONObject jsonRequest = new JSONObject( body.toString() ); 
			JSONObject jsonReplyBody = new JSONObject();
			JSONObject jsonRequestBody = jsonRequest.getJSONObject( "body" );
			if( jsonRequestBody.has( "_t") && jsonRequestBody.getString( "_t" ).equals( "params.get" ) ) {
				errorInfo = validateMessage( jsonRequestBody );
				if( errorInfo == null ) {
					String configType = jsonRequestBody.getString( "params" );
					String privilege = ContainerParams.getPrivileges( configType );
					String accountToken = jsonRequestBody.getString( "accounttoken" );
					if( science.db.accounts.validateAccountTokenByType( accountToken, privilege ) == true ) {
						// String[] properties = { "accounttoken" };
						// science.analytics.trackEvent( accountToken, "Write Settings", timeStamp, FieldMapper.getFields(jsonRequestBody, properties) );
						JSONObject settings = science.db.settings.readSettings( accountToken, configType, jsonRequestBody );
						jsonReplyBody.put( "_t", "params.get" );
						jsonReplyBody.put( "settings", settings );
					}
					else {
						jsonReplyBody.put( "_t", "params.get" );
						jsonReplyBody.put( "settings", new JSONObject() );
					}
				}
			}
			else {
				errorInfo = "Missing _t";
			}
			if( errorInfo != null && errorInfo.length() > 0 ) {
				req.response().setStatusCode(400);
				Logs.LogEvent( errorInfo );
				jsonReplyBody.put( "_t", "params.get_error" );
				jsonReplyBody.put( "info", errorInfo );
			}
			JSONObject jsonReply = new JSONObject().put( "_t", "msg" );
			JSONObject jsonHeader = jsonRequest.getJSONObject( "header" );
			jsonReply.put( "header", jsonHeader );
			jsonReply.put( "body", jsonReplyBody );
			Logs.LogEvent( "JSON Reply is " + jsonReply.toString() );
			req.response().headers().set("Content-Type", "application/json; charset=UTF-8");
			req.response().end(jsonReply.toString());
		}
		catch( org.json.JSONException e ) {
			e.printStackTrace();
			Logs.LogEvent( "JSON error is " + errorInfo );
			req.response().setStatusCode(400);
			req.response().end( "{ jsonfailure: true }");
		}
	}
}
