import java.util.Iterator;

import org.bson.types.ObjectId;
import org.json.JSONArray;
import org.json.JSONObject;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

public class DBContainerSettings {
	private boolean mConnected;
	private DB mDB;

	public DBContainerSettings() {
	}

	private boolean checkConnected() {
		return mConnected;
	}

	public void initialize( DB db )  {
		Logs.LogEvent("Initializing Settings DB");
		mDB = db;
		mConnected = true;
	}
	
	public JSONObject readUserSettings( String id ) {
		JSONObject replyData = new JSONObject();
		try { 
			DBCollection coll = mDB.getCollection("userconfig");
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put("id", id);
			DBObject result = coll.findOne(searchQuery);
			if( result != null ) {
				Iterator<String> entries = result.keySet().iterator();
				while( entries.hasNext() ) {
					String entry = entries.next().toString();
					if( entry.equals( "_id" ) == false ) {
						replyData.put( entry, result.get( entry ) );
					}
				}
			}
		}
		catch( Exception e ) {
			Logs.LogEvent( "Error " + e.toString() );
		}
		return replyData;
	}
	
	JSONObject readSettings( String accountToken, String configType, JSONObject configData )
	{
		if( checkConnected() ) {
			DBCollection coll = mDB.getCollection("accounts");
			
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put("_id", new ObjectId( accountToken ));
			DBObject result = coll.findOne(searchQuery);
			if( result != null ) {
				String id = result.get("_id").toString();
				if( configType.equals( "userConfig" ) ) {
					return readUserSettings( id );
				}
				if( configType.equals( "devConfig" ) ) {
					return readDevSettings( id );
				}
				if( configType.equals( "actionConfig" ) ) {
					return science.db.actions.readActionSettings( configData );
				}
				if( configType.equals( "questConfig" ) ) {
					return science.db.quests.readQuestSettings( configData );
				}
				if( configType.equals( "gameConfig" ) ) {
					int index = Integer.parseInt( configData.getString( "index") );					
					JSONObject settings = science.db.games.readGameSettings( id, index );
					if( settings.length() != 0 ) {
						String anonymousPlay = "0";
						if( settings.has( "anonymousplay") ) {
							anonymousPlay = settings.getString( "anonymousplay" );
						}
						if( science.db.games.showGameInfo( settings.getString( "gamestate" ), anonymousPlay, settings.getString( "devid" ), id, result.get( "privs").toString() ) ) {
							return settings;
						}
					}
				}
				if( configType.equals( "siteConfig" ) ) {
					return readSiteSettings();			
				}
			}
		}
		
		return new JSONObject();
	}

	public JSONObject readDevSettings( String id ) {
		JSONObject replyData = new JSONObject();
		DBCollection coll = mDB.getCollection("devconfig");
		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put("id", id);
		DBObject result = coll.findOne(searchQuery);
		if( result != null ) {
			Iterator<String> entries = result.keySet().iterator();
			while( entries.hasNext() ) {
				String entry = entries.next().toString();
				if( entry.equals( "_id" ) == false ) {
					replyData.put( entry, result.get( entry ) );
				}
			}
		}
		return replyData;
	}
	
	public JSONObject readSiteSettings() {
		JSONObject replyData = new JSONObject();
		DBCollection coll = mDB.getCollection("siteconfig");
		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put("settings", "site" );
		DBObject result = coll.findOne(searchQuery);
		if( result != null ) {
			replyData.put( "maxlevels", result.get( "maxlevels" ) );
			String value = result.get( "levelcurve" ).toString();
			replyData.put( "levelcurve", new JSONObject( value ) );
			value = result.get( "points" ).toString();
			replyData.put( "points", new JSONArray( value ) );
			replyData.put( "actions", science.db.actions.readSiteActions() );
			value = result.get( "tags" ).toString();
			replyData.put( "tags", new JSONArray( value ) );
		}
		else {
			replyData = ContainerParams.getDefaultSiteSettings();
			String write = writeSiteSettings( replyData );
			if( write != null ) {
				replyData = new JSONObject();
				replyData.put( "error", write);
			}
		}
		return replyData;
	}

	public String writeDevSettings( String id, JSONObject configData ) {
		DBCollection coll = mDB.getCollection("devconfig");
		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put("id", id);
		DBObject result = coll.findOne(searchQuery);
		if( result != null ) {
			if( configData.has( "devsecret") ) {
				result.put( "devsecret", configData.getString("devsecret") );
			}
			coll.save(result);
		}
		else {
			result = new BasicDBObject();
			result.put( "id", id );
			result.put( "devsecret", configData.getString("devsecret") );
			coll.insert(result);
		}
		return null;
	}
	
	public String writeUserSettings( String id, JSONObject configData ) {
		try { 
			DBCollection coll = mDB.getCollection("userconfig");
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put("id", id);
			DBObject result = coll.findOne(searchQuery);
			if( result != null ) {
				String username = configData.getString("accountAlias");
				String icon = configData.getString("icon");
				if( username.length() > 0 ) {
					if( icon.length() > 0 ) {
						if( science.db.accounts.userAliasIsUnique( id, username ) ) {
							result.put( "accountAlias", username );
							result.put( "city", configData.getString("city") );
							result.put( "country", configData.getString("country") );
							result.put( "bio", configData.getString("bio") );
							result.put( "accountdob", configData.getString("accountdob") );
							result.put( "accountnewsletter", configData.getString("accountnewsletter" ) );
							result.put( "icon", icon );
							coll.save(result);
							return null;
						}
						else {
							return "user name is not unique";
						}
					}
					else {
						return "icon missing";
					}
				}
				else {
					return "username missing";
				}
			}
		}
		catch( Exception e ) {
			Logs.LogEvent( "Error " + e.toString() );
		}
		return null;
	}
	
	public String writeSiteSettings( JSONObject configData ) {
		try { 
			DBCollection coll = mDB.getCollection("siteconfig");
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put("settings", "site" );
			DBObject result = coll.findOne(searchQuery);
			String validateSettings = ContainerParams.validateSiteSettings( configData );
			if( validateSettings != null ) {
				return validateSettings;
			}
			if( result != null ) {
				if( configData.has( "maxlevels") ) {
					result.put( "maxlevels", configData.getString("maxlevels") );
				}
				if( configData.has( "levelcurve") ) {
					result.put( "levelcurve", configData.getJSONObject("levelcurve").toString() );
				}
				if( configData.has( "points") ) {
					result.put( "points", configData.getJSONArray("points").toString() );
				}
				if( configData.has( "actions") ) {
					science.db.actions.writeSiteActions( configData.getJSONArray( "actions" ) );
				}
				if( configData.has( "tags") ) {
					result.put( "tags", configData.getJSONArray("tags").toString() );
				}
				coll.save(result);
				return null;
			}
			else {
				BasicDBObject document = new BasicDBObject();
				document.put( "settings", "site" );
				document.put( "maxlevels", configData.getString("maxlevels") );
				document.put( "levelcurve", configData.getJSONObject("levelcurve").toString() );
				document.put( "points", configData.getJSONArray("points").toString() );
				science.db.actions.writeSiteActions( configData.getJSONArray( "actions" ) );
				document.put( "tags", configData.getJSONArray("tags").toString() );
				coll.insert(document);
				return null;
			}
		}
		catch( Exception e ) {
			e.printStackTrace();
			Logs.LogEvent( "Error " + e.toString() );
		}
		return null;
	}

	public String writeSettings( String accountToken, String configType, JSONObject configData )	{
		if( checkConnected() ) {
			DBCollection coll = mDB.getCollection("accounts");
			
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put("_id", new ObjectId( accountToken ));
			DBObject result = coll.findOne(searchQuery);
			if( result != null ) {
				String userid = result.get("_id").toString();
				if( configType.equals( "userConfig" ) ) {
					return writeUserSettings( userid, configData );
				}
				if( configType.equals( "userPassword" ) ) {
					byte[] mySalt = ContainerConfig.getProperty( "passwordhash" ).getBytes();
					String oldPassword = configData.getString("oldpassword");
					String accountId = result.get("accountId").toString();
					if( (result.keySet().contains( "lostpassword") && result.get("lostpassword").toString().equals("2") ) ||
						(ContainerPassword.isExpectedPassword( (accountId + oldPassword).toCharArray(), mySalt, ContainerPassword.intoBytes( result.get( "accountPassword").toString() ) ) ) ) {
						String newPassword = configData.getString("newpassword");
						byte[] myHash = ContainerPassword.hash( (accountId + newPassword).toCharArray(),  mySalt );
						String myHashString = ContainerPassword.intoString( myHash );
						result.put("accountPassword", myHashString);
						result.removeField( "lostpassword" );
						coll.save(result);
						return null;
					}
					return "invalid password";
				}
				if( configType.equals( "userEmail" ) ) {
					String oldemail = configData.getString("oldemail");
					String newemail = configData.getString("newemail");
					if( result.get("accountId").equals( oldemail ) ) {
						result.put("accountId", newemail);
						coll.save(result);
						return null;
					}
					return "invalid email";
				}
				if( configType.equals( "userStatus" ) ) {
					String accountId = configData.getString("usertoken");
					if( accountId.equals( accountToken ) == false ) {
						searchQuery.put("_id", new ObjectId( accountId ));
						result = coll.findOne(searchQuery);
						if( result != null ) {
							String status = configData.getString("status");
							if( result.get("privs").equals( "admin" ) == false ) {
								result.put("privs", status );
								coll.save(result);
								return null;
							}
							else {
								return "cannot set status on admin";
							}
						}
					}
					else {
						return "cannot set status on self";
					}
				}
				if( configType.equals( "gameConfig" ) ) {
					return science.db.games.writeGameSettings( userid, configData, false );
				}
				if( configType.equals( "actionConfig" ) ) {
					return science.db.actions.writeActionSettings( configData );
				}
				if( configType.equals( "questConfig" ) ) {
					return science.db.quests.writeQuestSettings( configData );
				}
				if( configType.equals( "devConfig" ) ) {
					return writeDevSettings( userid, configData );
				}
				if( configType.equals( "siteConfig" ) ) {
					return writeSiteSettings( configData );
				}
			}
			return null;
		}
		return "MongoDB not connected.";
	}
}
