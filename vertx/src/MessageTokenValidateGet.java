import org.vertx.java.core.http.HttpServerRequest;

public class MessageTokenValidateGet {
	public MessageTokenValidateGet ( HttpServerRequest req, String[] myArguments )  {
		try { 
			String token = "";
			String privs = "";
			for( int i = 0; i < myArguments.length; i ++ ) {
				if( myArguments[i].contains( "privs=") ) {
					privs = myArguments[i].split("=")[1];
				}
				if( myArguments[i].contains( "token=") ) {
					token = myArguments[i].split("=")[1];
				}
			}
			boolean validation = science.db.accounts.validateAccountTokenByType( token, privs );
			
			req.response().end( "" + validation );
		}
		catch( org.json.JSONException e ) {
			e.printStackTrace();
			req.response().setStatusCode(400);
			req.response().end( "{ jsonfailure: true }");
		}
	}

}
