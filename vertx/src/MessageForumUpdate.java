import org.json.JSONArray;
import org.json.JSONObject;
import org.vertx.java.core.buffer.Buffer;
import org.vertx.java.core.http.HttpServerRequest;

public class MessageForumUpdate {
	private String validateMessage( JSONObject jsonRequestBody ) {
		if( jsonRequestBody.has("accounttoken") ) {
			if( jsonRequestBody.has("state") ) {
				if( jsonRequestBody.has( "forumid" ) ) {
					return null;
				}
				else {
					return "needs forum id";
				}					
			}
			else {
				return "needs state visible or hidden";
			}
		}
		else {
			return "Missing account token";
		}
	}

	public MessageForumUpdate( HttpServerRequest req, Buffer body )  {
		String errorInfo = null;
		try { 
			JSONObject jsonRequest = new JSONObject( body.toString() ); 
			JSONObject jsonReplyBody = new JSONObject();
			JSONObject jsonRequestBody = jsonRequest.getJSONObject( "body" );
			if( jsonRequestBody.has( "_t") && jsonRequestBody.getString( "_t" ).equals( "forum.update" ) ) {
				errorInfo = validateMessage( jsonRequestBody );
				if( errorInfo == null ) {
					String accountToken = jsonRequestBody.getString( "accounttoken" );
					String forumId = "";
					String state = "";
					String privileges = science.db.accounts.getAccountPrivs( accountToken );
					state = jsonRequestBody.getString( "state" );
					if( state.equals( "visible" ) || state.equals( "hidden" ) ) {
						forumId = jsonRequestBody.getString( "forumid" );
						JSONObject forum = science.db.forums.updateForum( accountToken, privileges, forumId, state );
						jsonReplyBody.put( "forum", forum );
						// String[] properties = { "accounttoken" };
						// science.analytics.trackEvent( accountToken, "Write Settings", timeStamp, FieldMapper.getFields(jsonRequestBody, properties) );
						jsonReplyBody.put( "_t", "forum.update" );
					}
					else {
						errorInfo = "state must be hidden or visible, not " + state;
					}
				}
			}
			else {
				errorInfo = "Missing _t";
			}
			if( errorInfo != null && errorInfo.length() > 0 ) {
				req.response().setStatusCode(400);
				Logs.LogEvent( errorInfo );
				jsonReplyBody.put( "_t", "forum.update_error" );
				jsonReplyBody.put( "info", errorInfo );
			}
			JSONObject jsonReply = new JSONObject().put( "_t", "msg" );
			JSONObject jsonHeader = jsonRequest.getJSONObject( "header" );
			jsonReply.put( "header", jsonHeader );
			jsonReply.put( "body", jsonReplyBody );
			Logs.LogEvent( "JSON Reply is " + jsonReply.toString() );
			req.response().headers().set("Content-Type", "application/json; charset=UTF-8");
			req.response().end(jsonReply.toString());
		}
		catch( org.json.JSONException e ) {
			e.printStackTrace();
			Logs.LogEvent( "JSON error is " + errorInfo );
			req.response().setStatusCode(400);
			req.response().end( "{ jsonfailure: true }");
		}
	}


}
