import org.json.JSONObject;
import org.vertx.java.core.buffer.Buffer;
import org.vertx.java.core.http.HttpServerRequest;

public class MessageQuestCreate {
	private String validateMessage( JSONObject jsonRequestBody )
	{
		if( jsonRequestBody.has("time") ) {
			if( jsonRequestBody.has("gameid") ) {
				if( jsonRequestBody.has("accounttoken") ) {
					for( int i = 0; i < ContainerParams.myQuestFields.length; i ++ ) {
						String field = ContainerParams.myQuestFields[i];
						if( jsonRequestBody.has( field ) == false ) {
							return "missing quest setting for  " + field;
						}
					}
					return null;
				}
				else {
					return "Missing account token";
				}
			}
			else {
				return "Missing gameid";
			}
		}
		else {
			return "Missing time";
		}
	}
	
	public MessageQuestCreate( HttpServerRequest req, Buffer body )  {
		Logs.LogEvent("Create Activity" );
		String errorInfo = null;
		try { 
			JSONObject jsonRequest = new JSONObject( body.toString() ); 
			JSONObject jsonReplyBody = new JSONObject();
			JSONObject jsonRequestBody = jsonRequest.getJSONObject( "body" );
			if( jsonRequestBody.has( "_t") && jsonRequestBody.getString( "_t" ).equals( "quest.create" ) ) {
				errorInfo = validateMessage( jsonRequestBody );
				if( errorInfo == null ) {
					String timeStamp = "";
					String accountToken = "";
					timeStamp = jsonRequestBody.getString( "time" );
					accountToken = jsonRequestBody.getString( "accounttoken" );
					if( science.db.accounts.validateAccountTokenByType( accountToken, "dev" ) == true ) {
						JSONObject newQuest = science.db.quests.createQuest( accountToken, jsonRequestBody );
						if( newQuest.keySet().isEmpty() == false ) {
							//String[] properties = { "accounttoken", "activity" };
							//science.analytics.trackEvent( accountToken, "Activity", timeStamp, FieldMapper.getFields(jsonRequestBody, properties ) );
							jsonReplyBody.put( "_t", "quest.created" );
							jsonReplyBody.put( "quest", newQuest );
						}
						else {
							errorInfo = "DB Failure";
						}
					}
					else {
						errorInfo = "token invalid";
					}
				}
			}
			else {
				errorInfo = "Missing _t";
			}
			if( errorInfo != null && errorInfo.length() > 0 ) {
				req.response().setStatusCode(400);
				Logs.LogEvent( errorInfo );
				jsonReplyBody.put( "_t", "quest.create_error" );
				jsonReplyBody.put( "info", errorInfo );
			}
			JSONObject jsonReply = new JSONObject().put( "_t", "msg" );
			JSONObject jsonHeader = jsonRequest.getJSONObject( "header" );
			jsonReply.put( "header", jsonHeader );
			jsonReply.put( "body", jsonReplyBody );
			Logs.LogEvent( "JSON Reply is " + jsonReply.toString() );
			req.response().end(jsonReply.toString());
		}
		catch( org.json.JSONException e ) {
			e.printStackTrace();
			Logs.LogEvent( "JSON error is " + errorInfo );
			req.response().setStatusCode(400);
			req.response().end( "{ jsonfailure: true }");
		}
	}

}
