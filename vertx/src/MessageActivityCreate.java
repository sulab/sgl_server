import org.json.JSONObject;
import org.vertx.java.core.buffer.Buffer;
import org.vertx.java.core.http.HttpServerRequest;

public class MessageActivityCreate {
	private String validateMessage( JSONObject jsonRequestBody )
	{
		if( jsonRequestBody.has("time") ) {
			if( jsonRequestBody.has("details") ) {
				if( jsonRequestBody.has("accounttoken") ) {
					if( jsonRequestBody.has("name") ) {
						return null;
					}
					else {
						return "Missing activity name";					
					}
				}
				else {
					return "Missing account token";
				}
			}
			else {
				return "Missing details";
			}
		}
		else {
			return "Missing time";
		}
	}
	
	public MessageActivityCreate( HttpServerRequest req, Buffer body )  {
		Logs.LogEvent("Create Activity" );
		String errorInfo = null;
		try { 
			JSONObject jsonRequest = new JSONObject( body.toString() ); 
			JSONObject jsonReplyBody = new JSONObject();
			JSONObject jsonRequestBody = jsonRequest.getJSONObject( "body" );
			if( jsonRequestBody.has( "_t") && jsonRequestBody.getString( "_t" ).equals( "activity.create" ) ) {
				errorInfo = validateMessage( jsonRequestBody );
				if( errorInfo == null ) {
					String timeStamp = "";
					String accountToken = "";
					String name = "";
					String gameId = "0";
					JSONObject details;
					timeStamp = jsonRequestBody.getString( "time" );
					details = jsonRequestBody.getJSONObject( "details" );
					if( details.has( "gameId") ) {
						gameId = details.getString( "gameId" );
					}
					accountToken = jsonRequestBody.getString( "accounttoken" );
					name = jsonRequestBody.getString( "name" );
					if( science.db.accounts.validateUserAccountToken( accountToken ) == true ) {
						if( science.db.actions.createActivity( accountToken, name, gameId, details ) == true ) {
							String[] properties = { "accounttoken", "name" };
							science.analytics.trackEvent( accountToken, "Activity", timeStamp, FieldMapper.getFields(jsonRequestBody, properties ) );
							jsonReplyBody.put( "_t", "activity.created" );
							jsonReplyBody.put( "accounttoken", accountToken );
							JSONObject quests = science.db.quests.updateQuestsForAction( accountToken, name, gameId );
							if( quests.has( "updated") ) {
								jsonReplyBody.put( "updated", quests.getJSONArray( "updated") );
							}
							if( quests.has( "completed") ) {
								jsonReplyBody.put( "completed", quests.getJSONArray( "completed") );
							}
						}
						else {
							errorInfo = "DB Failure";
						}
					}
					else {
						errorInfo = "token invalid";
					}
				}
			}
			else {
				errorInfo = "Missing _t";
			}
			if( errorInfo != null && errorInfo.length() > 0 ) {
				req.response().setStatusCode(400);
				Logs.LogEvent( errorInfo );
				jsonReplyBody.put( "_t", "activity.create_error" );
				jsonReplyBody.put( "info", errorInfo );
			}
			JSONObject jsonReply = new JSONObject().put( "_t", "msg" );
			JSONObject jsonHeader = jsonRequest.getJSONObject( "header" );
			jsonReply.put( "header", jsonHeader );
			jsonReply.put( "body", jsonReplyBody );
			Logs.LogEvent( "JSON Reply is " + jsonReply.toString() );
			req.response().end(jsonReply.toString());
		}
		catch( org.json.JSONException e ) {
			e.printStackTrace();
			Logs.LogEvent( "JSON error is " + errorInfo + " of type " + e.toString() );
			req.response().setStatusCode(400);
			req.response().end( "{ jsonfailure: true }");
		}
	}
}
