import org.vertx.java.core.http.HttpServerRequest;
import org.vertx.java.core.buffer.Buffer;
import org.json.*;

public class MessageUserCreateMobile {
	public MessageUserCreateMobile( HttpServerRequest req, Buffer body )  {
		Logs.LogEvent("Create Mobile User" );
		String errorInfo = null;
		try { 
			JSONObject jsonRequest = new JSONObject( body.toString() ); 
			JSONObject jsonReplyBody = new JSONObject();
			JSONObject jsonRequestBody = jsonRequest.getJSONObject( "body" );
			if( jsonRequestBody.has( "_t") && jsonRequestBody.getString( "_t" ).equals( "mobileuser.create" ) ) {
				String timeStamp = "";
				String deviceId = "";
				if( jsonRequestBody.has("time") ) {
					timeStamp = jsonRequestBody.getString( "time" );
				}
				if( jsonRequestBody.has("duid") ) {
					deviceId = jsonRequestBody.getString( "duid" );
				}
				else {
					errorInfo = "Missing deviceId";					
				}
				if( deviceId != null && deviceId.length() > 0 ) {
					String deviceToken = science.db.accounts.createDeviceToken( deviceId );
					String[] properties = { "duid" };
					science.analytics.trackEvent( deviceToken, "App Installed", timeStamp, FieldMapper.getFields(jsonRequestBody, properties) );
					String[] profilefields = new String[2];
					profilefields[0] = "name"; 
					profilefields[1] = "anon" + deviceToken.substring( deviceToken.length() - 6);
					science.analytics.profileUpdate( deviceToken, profilefields );
					if( deviceToken != null && deviceToken.length() > 0 ) {
						jsonReplyBody.put( "_t", "mobileuser.created" );
						jsonReplyBody.put( "dtoken", deviceToken );
					}
					else {
						errorInfo = "DB Failure";
					}
				}
			}
			else {
				errorInfo = "Missing _t";
			}
			if( errorInfo != null && errorInfo.length() > 0 ) {
				req.response().setStatusCode(400);
				Logs.LogEvent( errorInfo );
				jsonReplyBody.put( "_t", "mobileuser.create_error" );
				jsonReplyBody.put( "info", errorInfo );
			}
			JSONObject jsonReply = new JSONObject().put( "_t", "msg" );
			JSONObject jsonHeader = jsonRequest.getJSONObject( "header" );
			jsonReply.put( "header", jsonHeader );
			jsonReply.put( "body", jsonReplyBody );
			Logs.LogEvent( "JSON Reply is " + jsonReply.toString() );
			req.response().end(jsonReply.toString());
		}
		catch( org.json.JSONException e ) {
			e.printStackTrace();
			Logs.LogEvent( "JSON error is " + errorInfo );
			req.response().setStatusCode(400);
			req.response().end( "{ jsonfailure: true }");
		}
	}
}
