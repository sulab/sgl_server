import org.json.JSONObject;
import org.vertx.java.core.buffer.Buffer;
import org.vertx.java.core.http.HttpServerRequest;

public class MessageUserPromoteAnon {
	private String validateMessage( JSONObject jsonRequestBody )
	{
		if( jsonRequestBody.has("accountAlias") ) {
			if( jsonRequestBody.has("accountid") ) {
				if( jsonRequestBody.has("accountpassword") ) {
					if( jsonRequestBody.has("accounttoken") ) {
						return null;
					}
					else {
						return "Missing anonymous account token";
					}
				}
				else {
					return "Missing account password";
				}
			}
			else {
				return "Missing account id";
			}
		}
		else {
			return "Missing account alias";
		}
	}

	public MessageUserPromoteAnon( HttpServerRequest req, Buffer body )  {
		Logs.LogEvent("Promote Anon User" );
		String errorInfo = null;
		try { 
			JSONObject jsonRequest = new JSONObject( body.toString() ); 
			JSONObject jsonReplyBody = new JSONObject();
			JSONObject jsonRequestBody = jsonRequest.getJSONObject( "body" );
			if( jsonRequestBody.has( "_t") && jsonRequestBody.getString( "_t" ).equals( "anonuser.promote" ) ) {
				errorInfo = validateMessage( jsonRequestBody );
				if( errorInfo == null ) {
					String timeStamp = "";
					if( jsonRequestBody.has("time") ) {
						timeStamp = jsonRequestBody.getString( "time" );
					}
					String accountDOB = "1-1-1998";
					String accountNewsLetter = "1";
					String accountAlias = jsonRequestBody.getString( "accountAlias" );
					String accountId = jsonRequestBody.getString( "accountid" );
					String accountToken = jsonRequestBody.getString( "accounttoken" );
					String accountPassword = jsonRequestBody.getString( "accountpassword" );
					if( jsonRequestBody.has("accountdob") ) {
						accountDOB = jsonRequestBody.getString( "accountdob" );
					}
					if( jsonRequestBody.has("accountnewsletter") ) {
						accountNewsLetter = jsonRequestBody.getString( "accountnewsletter" );
					}
					if( science.db.accounts.upgradeAnonymousAccount( accountToken, accountId, accountPassword, accountAlias, accountDOB, accountNewsLetter ) == true ) {
						String[] properties = { "accountToken" };
						jsonRequestBody.put( "accountToken",  accountToken );
						science.analytics.trackEvent( accountToken, "Promoted Anon Account", timeStamp, FieldMapper.getFields(jsonRequestBody, properties) );
						if( accountToken != null && accountToken.length() > 0 ) {
							jsonReplyBody.put( "_t", "anonuser.promoted" );
							jsonReplyBody.put( "accounttoken", accountToken );
							jsonReplyBody.put("privs", "user");
						}
						else {
							errorInfo = "DB Failure";
						}
					}
				}
			}
			else {
				errorInfo = "Missing _t";
			}
			if( errorInfo != null && errorInfo.length() > 0 ) {
				req.response().setStatusCode(400);
				Logs.LogEvent( errorInfo );
				jsonReplyBody.put( "_t", "anonuser.promote_error" );
				jsonReplyBody.put( "info", errorInfo );
			}
			JSONObject jsonReply = new JSONObject().put( "_t", "msg" );
			JSONObject jsonHeader = jsonRequest.getJSONObject( "header" );
			jsonReply.put( "header", jsonHeader );
			jsonReply.put( "body", jsonReplyBody );
			Logs.LogEvent( "JSON Reply is " + jsonReply.toString() );
			req.response().end(jsonReply.toString());
		}
		catch( org.json.JSONException e ) {
			e.printStackTrace();
			Logs.LogEvent( "JSON error is " + errorInfo );
			req.response().setStatusCode(400);
			req.response().end( "{ jsonfailure: true }");
		}
	}

}
