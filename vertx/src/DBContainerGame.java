import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.bson.types.ObjectId;
import org.json.JSONArray;
import org.json.JSONObject;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

public class DBContainerGame {
	private DB mDB;

	public DBContainerGame() {
	}

	public void initialize( DB db )  {
		Logs.LogEvent("Initializing Game DB");
		mDB = db;
	}

	public boolean validateDevGameChangeState( String accountToken, String gameToken, String newGameState ) {
		DBCollection coll = mDB.getCollection("gameconfig");
		BasicDBObject searchQuery = new BasicDBObject();
		try {
			searchQuery.put("_id", new ObjectId( gameToken ));
			DBObject result = coll.findOne(searchQuery);
			if( result != null ) {
				String oldGameState = "";
				if( result.get("devid").equals(accountToken) ) {
					if( result.keySet().contains("gamestate") ) {
						oldGameState = result.get("gamestate").toString();
					}
					if( oldGameState.equals( "banned") ) {
						return false;
					}
					else if( oldGameState.equals( "live") ) {
						if( newGameState.equals( "inactive") ) {
							return true;
						}
						else {
							return false;
						}
					}
					else if( oldGameState.equals( "inactive") ) {
						if( newGameState.equals( "live") ) {
							return true;
						}
						else {
							return false;
						}
					}
					else if( oldGameState.equals( "stage") ) {
						if( newGameState.equals( "pending") ) {
							return true;
						}
						else if( newGameState.equals( "deleted") ) {
							return true;
						}
						else {
							return false;
						}					
					}
					else if( oldGameState.equals( "pending") ) {
						if( newGameState.equals( "stage") ) {
							return true;
						}
						else if( newGameState.equals( "deleted") ) {
							return true;
						}
						else {
							return false;
						}					
					}
				}
				return false;
			}
		}
		catch( Exception e ) {
			return false;
		}
		return false;
	}
	
	public boolean validateGameChangeState( String accountToken, String gameToken, String gameState ) {
		DBCollection coll = mDB.getCollection("accounts");
		BasicDBObject searchQuery = new BasicDBObject();
		try {
			searchQuery.put("_id", new ObjectId( accountToken ));
			DBObject result = coll.findOne(searchQuery);
			if( result != null ) {
				if( science.db.accounts.validateAccountTokenByType( accountToken, "admin" ) == true ) {
					return true;
				}
				else if( science.db.accounts.validateAccountTokenByType( accountToken, "dev" ) == true ) {
					return validateDevGameChangeState( accountToken, gameToken, gameState );
				}
				return true;
			}
		}
		catch( Exception e ) {
			return false;
		}
		return false;		
	}
	
	public String updateGameState( String accountToken, String gameToken, String gameState ) {
		DBCollection coll = mDB.getCollection("gameconfig");
		BasicDBObject searchQuery = new BasicDBObject();
		try {
			searchQuery.put("_id", new ObjectId( gameToken ));
			DBObject result = coll.findOne(searchQuery);
			if( result != null ) {
				if( ContainerParams.validateGameState( gameState )  ){
					result.put("gamestate", gameState);
					coll.save( result );
					return null;
				}
			}
			else {
				return "could not find game";
			}
		}
		catch( Exception e ) {
			return "error " + e.toString();
		}
			
		return "could not set to state " + gameState;
	}

	boolean showGameInfo( String gameStatus, String anonData, String devId, String accountToken, String privileges ) {
		if( privileges == null || privileges == "" || privileges.equals( "anon" ) ) {
			if( anonData.equals( "1" ) ) {
				if( gameStatus.equals( "live") ) {
					return true;
				}
			}
		}
		else if( privileges.equals( "user" ) ) {
			if( gameStatus.equals( "live") ) {
				return true;
			}
		}
		else if( privileges.equals( "dev" ) ) {
			if( gameStatus.equals( "live" ) || 
				gameStatus.equals( "sandbox" ) ) {
				return true;
			}
			else if( gameStatus.equals( "stage" ) || 
				gameStatus.equals( "inactive" ) ||
				gameStatus.equals( "pending" ) ) {
				if( devId.equals( accountToken ) ) {
					return true;
				}
			}
		}
		else if( privileges.equals( "admin" ) ) {
			return true;
		}
		return false;
	}

	public String writeGameSettings( String id, JSONObject configData, boolean createNew ) {
		int index = Integer.parseInt( configData.getString( "index") );
		DBCollection coll = mDB.getCollection("gameconfig");
		DBObject result;
		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put( "devid", id );
		searchQuery.put( "index", "" + index );
		result = coll.findOne( searchQuery);
		if( result != null ) {
			String anonymousPlay = "0";
			if( result.keySet().contains( "anonymousplay") ) {
				anonymousPlay = result.get( "anonymousplay" ).toString();
			}
			if( science.db.games.showGameInfo( result.get( "gamestate" ).toString(), anonymousPlay, result.get( "devid" ).toString(), id, science.db.accounts.getAccountPrivs(id) ) ) {
				for( int i = 0; i < ContainerParams.myGameFields.length; i ++ ) {
					if( ContainerParams.myGameFields[i].equals( "tags" ) ) {
						JSONArray myTags = configData.getJSONArray( "tags" );
						for( int j = 0; j < myTags.length(); j ++ ) {
							if( science.db.validateTag( myTags.getJSONObject( j ) ) == false ) {
								return "Cannot create game tag " + myTags.getJSONObject(j).toString();
							}
						}
						result.put( ContainerParams.myGameFields[i], configData.getJSONArray( ContainerParams.myGameFields[i] ).toString() );
					}
					else if( ContainerParams.myGameFields[i].equals( "actionids" ) ) {
						if( configData.has( "actionids") ) {
							result.put( ContainerParams.myGameFields[i], configData.getJSONArray( ContainerParams.myGameFields[i] ).toString() );							
						}
						else {
							JSONArray myactionids = new JSONArray();
							result.put( ContainerParams.myGameFields[i], myactionids.toString() );
						}
					}
					else if( ContainerParams.myGameFields[i].equals( "leaderboard" ) ) {
						if( ContainerParams.validateLeaderboardSettings( configData.getString( ContainerParams.myGameFields[i] ) ) ) {
							result.put( ContainerParams.myGameFields[i], configData.getString( ContainerParams.myGameFields[i] ) );
						}
						else {
							return "invalid leaderboard settings -" + id + " : " + configData.getString( ContainerParams.myGameFields[i] );
						}
					}
					else {
						result.put( ContainerParams.myGameFields[i], configData.getString( ContainerParams.myGameFields[i] ) );
					}
				}
				coll.save( result );
			}
		}
		else {
			if( createNew ) {
				BasicDBObject newSettings = new BasicDBObject();
				for( int i = 0; i < ContainerParams.myGameFields.length; i ++ ) {
					if( ContainerParams.myGameFields[i].equals( "actions" ) ||
					    ContainerParams.myGameFields[i].equals( "tags" ) ) {
						newSettings.put( ContainerParams.myGameFields[i], configData.getJSONArray( ContainerParams.myGameFields[i] ).toString() );
					}
					else if( ContainerParams.myGameFields[i].equals( "actionids" ) ) {
						if( configData.has( "actionids") ) {
							newSettings.put( ContainerParams.myGameFields[i], configData.getJSONArray( ContainerParams.myGameFields[i] ).toString() );							
						}
						else {
							JSONArray myactionids = new JSONArray();
							newSettings.put( ContainerParams.myGameFields[i], myactionids.toString() );
						}
					}
					else {
						newSettings.put( ContainerParams.myGameFields[i], configData.getString( ContainerParams.myGameFields[i] ) );
					}
				}
				newSettings.put( "devid", id );
				newSettings.put( "index", "" + index );
				coll.insert(newSettings);
			}
			else {
				return "Cannot create new game";
			}
		}
		return null;
	}

	public String getGameName( String gameId ) {
		String response = "";
		DBCollection coll = mDB.getCollection("gameconfig");
		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put( "_id", new ObjectId( gameId ) );
		DBObject result = coll.findOne( searchQuery );
		if( result != null ) {
			response = result.get( "name" ).toString();
		}
		return response;
	}
	
	public JSONObject createGame( String developerToken ) {
		DBCollection coll = mDB.getCollection("gameconfig");
		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put( "devid", developerToken );
		DBCursor results = coll.find( searchQuery);
		int myCount = results.count();
		while( readGameSettings( developerToken, myCount ).length() != 0 ) {
			myCount++;
		}
		JSONObject gameData = ContainerParams.getDefaultGameSettings();
		gameData.put( "name",  "name " + myCount );
		gameData.put( "index",  "" + myCount );
		if( science.db.games.writeGameSettings( developerToken, gameData, true ) == null ) { 
			return readGameSettings( developerToken, myCount );
		}
		return null;
	}
	
	public JSONObject readGameSettings( String id, int index ) {
		JSONObject replyData = new JSONObject();
		DBCollection coll = mDB.getCollection("gameconfig");
		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put("devid", id);
		searchQuery.put("index", "" + index );
		DBObject result = coll.findOne(searchQuery);
		if( result != null ) {
			Iterator<String> entries = result.keySet().iterator();
			while( entries.hasNext() ) {
				String entry = entries.next().toString();
				if( entry.equals( "_id" ) == false ) {
					replyData.put( entry, result.get( entry ) );
					if( entry.equals( "actionids" ) ) {
						replyData.put( "actionids", result.get( entry ) );
						replyData.put( "actions", science.db.actions.getActions( result.get( entry ).toString() ) );
					}
				}
				else {
					replyData.put( "gameid", result.get( entry ).toString() );
				}
			}
			replyData.put( "quests", science.db.quests.readQuests( id ) );
		}
		return replyData;
	}

	JSONObject readGameInfo( String accountToken, String privileges, String itemId, JSONObject params ) {
		JSONObject replyData = new JSONObject();
		DBCollection coll = mDB.getCollection("gameconfig");
		boolean validated = true;
		BasicDBObject searchQuery = new BasicDBObject();
		if( itemId == null || itemId.length() == 0 ) {
			if( privileges == null || privileges == "" || privileges.equals( "user" ) ) {
				searchQuery.put("gamestate", "live" );
			}
			else if( privileges.equals( "dev" ) ) {
				List<BasicDBObject> obj = new ArrayList<BasicDBObject>();
				obj.add(new BasicDBObject("gamestate", "live" ));
				obj.add(new BasicDBObject("gamestate", "sandbox"));
				
				BasicDBObject compoundQuery = new BasicDBObject();
				List<BasicDBObject> compoundlist = new ArrayList<BasicDBObject>();
				BasicDBObject stateQuery = new BasicDBObject();
				List<BasicDBObject> statelist = new ArrayList<BasicDBObject>();
				
				statelist.add(new BasicDBObject("gamestate", "pending" ));
				statelist.add(new BasicDBObject("gamestate", "stage" ));
				statelist.add(new BasicDBObject("gamestate", "inactive" ));
				stateQuery.put( "$or", statelist );

				compoundlist.add( stateQuery );
				compoundlist.add( new BasicDBObject("devid", accountToken ) );
				compoundQuery.put( "$and", compoundlist );
				obj.add( compoundQuery );

				searchQuery.put("$or", obj);
			}
			DBCursor cursor = coll.find(searchQuery);
			JSONArray games = new JSONArray();
			while (cursor.hasNext()) {
				DBObject nextGame = cursor.next();
				JSONObject gameData = new JSONObject();
				Iterator<String> entries = nextGame.keySet().iterator();
				while( entries.hasNext() ) {
					String entry = entries.next().toString();
					if( entry.equals( "_id" ) == false ) {
						if(entry.equals("tags") ||
						   entry.equals("actions") ) {
							String value = nextGame.get( entry ).toString();
							gameData.put( entry, new JSONArray( value ) );
						}
						else if( entry.equals( "actionids" ) ) {
							gameData.put( "actionids", nextGame.get( entry ) );
							gameData.put( "actions", science.db.actions.getActions( nextGame.get( entry ).toString() ) );
						}
						else {
							gameData.put( entry, nextGame.get( entry ) );
						}
					}
					else {
						gameData.put( "gameid", nextGame.get( entry ).toString() );
					}
					gameData.put( "quests", science.db.quests.readQuests( nextGame.get( "_id" ).toString() ) );
				}
				validated = true;
				if( params.has( "filters") ) {
					JSONObject filters = params.getJSONObject( "filters" );
					Iterator<String> myKeys = filters.keys();
					while( myKeys.hasNext() && validated ) {
						String key = myKeys.next();
						if( gameData.keySet().contains(key) ) {
							if( gameData.get( key ).toString().equals(filters.get(key).toString() ) == false ) {
								validated = false;
							}
						}
					}
				}
				if ( validated ) {
					games.put( gameData );
				}
			}
			replyData.put( "games", games);
			
		}
		else {
			searchQuery.put("_id", new ObjectId( itemId ));
			DBObject result = coll.findOne(searchQuery);
			if( result != null ) {
				String anonymousPlay = "0";
				if( result.keySet().contains( "anonymousplay") ) {
					anonymousPlay = result.get( "anonymousplay" ).toString();
				}
				if( science.db.games.showGameInfo( result.get( "gamestate" ).toString(), anonymousPlay, result.get( "devid" ).toString(), accountToken, privileges ) ) {
					Iterator<String> entries = result.keySet().iterator();
					while( entries.hasNext() ) {
						String entry = entries.next().toString();
						if( entry.equals( "_id" ) == false ) {
							replyData.put( entry, result.get( entry ) );
						}
						else if( entry.equals( "actionids" ) ) {
							replyData.put( "actionids", result.get( entry ) );
							replyData.put( "actions", science.db.actions.getActions( result.get( entry ).toString() ) );
						}
						else {
							replyData.put( "gameid", result.get( entry ).toString() );
						}
					}
				}
			}
			
		}
		replyData.put( "quests", science.db.quests.readQuests( itemId ) );
		return replyData;
	}

}
