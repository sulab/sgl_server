import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.bson.types.ObjectId;
import org.json.JSONObject;
import org.vertx.java.core.json.JsonObject;
import org.json.JSONArray;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

public class DBContainerQuest {
	private DB mDB;

	public DBContainerQuest() {
	}

	public void initialize( DB db )  {
		Logs.LogEvent("Initializing Quest DB");
		mDB = db;
	}

	public DBObject findQuestDataForUser( String token, String quest )
	{
		DBCollection coll = mDB.getCollection("questdata");
		BasicDBObject searchQuery = new BasicDBObject();
		List<BasicDBObject> obj = new ArrayList<BasicDBObject>();
		obj.add(new BasicDBObject("userid", token));
		obj.add(new BasicDBObject("questid", quest));
		searchQuery.put("$and", obj);
		DBObject result = coll.findOne(searchQuery);
		if( result != null ) {
			return result;
		}
		return null;
	}
	
	public void updateCriteriaInQuestDataForUser( String token, String quest ) {
		DBCollection coll = mDB.getCollection("questdata");
		boolean modified = false;
		BasicDBObject searchQuery = new BasicDBObject();
		List<BasicDBObject> obj = new ArrayList<BasicDBObject>();
		obj.add(new BasicDBObject("userid", token));
		obj.add(new BasicDBObject("questid", quest));
		searchQuery.put("$and", obj);
		DBObject result = coll.findOne(searchQuery);
		if( result != null ) {
			if( result.keySet().contains( "progress") ) {
				JSONArray progressArray = new JSONArray( result.get("progress").toString() );
				JSONArray questCriteria = findQuestCriteria( quest );
				for( int i = 0; i < questCriteria.length(); i ++ ) {
					JSONObject progress = questCriteria.getJSONObject(i);
					if( questContains( progress.get("actionid").toString(), progressArray ) == false ) {
						modified = true;
						String criteriaId = progress.getString("actionid");
						JSONObject criteriaItem = new JSONObject();
						criteriaItem.put( "actionid",  criteriaId );
						criteriaItem.put( "progress", "0" );
						criteriaItem.put( "completed", "0" );
						progressArray.put( criteriaItem );
					}
				}			
				if( modified == true ) {
					result.put( "progress", progressArray.toString() );
					coll.save(result);				
				}
			}
		}		
	}
	
	public void removeOldQuestDataForUser( String token, String quest ) {
		DBCollection coll = mDB.getCollection("questdata");
		boolean modified = false;
		BasicDBObject searchQuery = new BasicDBObject();
		List<BasicDBObject> obj = new ArrayList<BasicDBObject>();
		obj.add(new BasicDBObject("userid", token));
		obj.add(new BasicDBObject("questid", quest));
		searchQuery.put("$and", obj);
		DBObject result = coll.findOne(searchQuery);
		if( result != null ) {
			if( result.keySet().contains( "progress") ) {
				JSONArray newProgress = new JSONArray();
				JSONArray progressArray = new JSONArray( result.get("progress").toString() );
				JSONArray questCriteria = findQuestCriteria( quest );
				for( int i = 0; i < progressArray.length(); i ++ ) {
					JSONObject progress = progressArray.getJSONObject(i);
					if( questContains( progress.get("actionid").toString(), questCriteria ) == false ) {
						modified = true;
					}
					else {
						newProgress.put( progress );
					}
				}
				if( modified == true ) {
					result.put( "progress", newProgress.toString() );
					coll.save(result);				
				}
			}
		}
	}
	
	public JSONArray findQuestCriteria( String actionId ) {
		JSONArray response = new JSONArray();
		DBCollection coll = mDB.getCollection("questconfig");
		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put("action", actionId );
		DBObject result = coll.findOne(searchQuery);
		if( result != null ) {
			if( result.keySet().contains("criteria") ) {
				response = new JSONArray( result.get("criteria").toString() );
			}
		}
		return response;
	}
	
	public boolean questContains( String actionId, JSONArray questCriteria ) {
		for( int j = 0; j < questCriteria.length(); j ++ ) {
			JSONObject questAction = questCriteria.getJSONObject(j); 
			if( questAction.getString("actionid").equals( actionId ) ) {
				return true;
			}
		}
		return false;
	}

	public JSONArray getQuestDataForUser( String token ) {
		DBCollection coll = mDB.getCollection("questdata");
		JSONArray quests = new JSONArray();
		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put("userid", token);
		DBCursor results = coll.find(searchQuery);
		while (results.hasNext()) {
			DBObject result = results.next();
			JSONObject quest = new JSONObject();
			Iterator<String> entries = result.keySet().iterator();
			while( entries.hasNext() ) {
				String entry = entries.next().toString();
				if( entry.equals( "progress" ) ) {
					quest.put( entry, new JSONArray( result.get( entry ).toString() ) );
				}
				else { quest.put( entry, result.get( entry ) );
				}
			}
			quests.put( quest );
		}
		return quests;
	}
	
	public void createQuestDataForUser( String token, String questId, DBObject quest )
	{
		DBCollection coll = mDB.getCollection("questdata");
		BasicDBObject questItem = new BasicDBObject();
		questItem.put( "userid", token );
		questItem.put( "questid", questId );
		questItem.put( "completed",  "0" );
		JSONArray criteria = new JSONArray( quest.get("criteria").toString() );
		JSONArray usercriteria = new JSONArray();
		for( int i = 0; i < criteria.length(); i ++ ) {
			JSONObject criteriaObject = criteria.getJSONObject(i);
			String criteriaId = criteriaObject.getString("actionid");
			JSONObject criteriaItem = new JSONObject();
			criteriaItem.put( "actionid",  criteriaId );
			criteriaItem.put( "progress", "0" );
			criteriaItem.put( "completed", "0" );
			usercriteria.put( criteriaItem );
		}
		questItem.put( "progress", usercriteria.toString() );
		coll.insert(questItem);
		Logs.LogEvent( "creating quest " + questId + " for " + token );
	}
	
	public JSONObject updateQuestForActionForUser( String token, DBObject quest, String actionId ) {
		JSONObject reply = new JSONObject();
		int myCriteriaCount = 0;
		if( quest.keySet().contains("criteria") ) {
			JSONArray myCriteria = new JSONArray( quest.get("criteria").toString() );
			myCriteriaCount = myCriteria.length();
		}
		DBObject questData;
		String questId = quest.get( "action" ).toString();
		Logs.LogEvent( "updating quest " + questId + " for " + token + " for action id " + actionId );
		removeOldQuestDataForUser( token, questId );
		updateCriteriaInQuestDataForUser( token, questId );
		questData = findQuestDataForUser( token, questId );
		if( questData == null ) {
			createQuestDataForUser( token, questId, quest );
			questData = findQuestDataForUser( token, questId );
		}
		if( questData != null ) {
			try {
				Logs.LogEvent( "found quest data" + questData );
				DBCollection coll = mDB.getCollection("questdata");
				if(questData.keySet().contains("progress")) {
					if(questData.get( "completed" ).toString().equals( "0") ) {
						JSONArray progress = new JSONArray( questData.get("progress").toString() );
						if( myCriteriaCount == 0 ) {
							myCriteriaCount = progress.length();
						}
						int completed = 0;
						boolean modified = false;
						int myActionIndex = -1;
						for( int i = 0; i < progress.length(); i ++ ) {
							JSONObject questUpdate = progress.getJSONObject(i);
							Logs.LogEvent( "updating quest data" + i + ":" + questUpdate );
							if( questUpdate.getString( "actionid" ).equals( actionId ) ) {
								if( questUpdate.getString( "completed").equals( "0" ) ) {
									myActionIndex = i;
									modified = true;
								}
							}
							else {
								if( questUpdate.getString( "completed").equals( "1" ) ) {
									Logs.LogEvent( "found completed =>" + completed );
									completed++;
								}
							}
						}
						if( myActionIndex > -1 && modified == true ) {
							JSONObject questUpdate = progress.getJSONObject(myActionIndex);
							questUpdate.remove( "progress" );
							questUpdate.put( "progress", "1" );
							questUpdate.remove( "completed" );
							questUpdate.put( "completed", "1" );
							progress.put( myActionIndex, questUpdate );
							questData.removeField( "progress" );
							questData.put( "progress",  progress.toString() );
							Logs.LogEvent( "updating quest data for quest " + myActionIndex + ":" + questUpdate );
							Logs.LogEvent( "updating quest data total as " + questData );
							completed++;
							Logs.LogEvent( "saving update =>" + completed );							
						}
						Logs.LogEvent( "completed is " + completed + " of " + myCriteriaCount );
						if( completed >= myCriteriaCount ) {
							modified = true;
							questData.removeField("completed");
							questData.put( "completed",  "1" ); 
							reply.put( "completed", questId );
						}
						else {
							reply.put( "updated", questId );
						}
						Logs.LogEvent( "updating quest data" + questData );
						if( modified == true ) {
							coll.save(questData);
						}
					}
				}
			}
			catch( Exception e ) {
				e.printStackTrace();
			}
		}
		else {
			Logs.LogEvent( "found no quest data for " + questId );
		}
		return reply;
	}
	
	public JSONObject updateQuestsForAction( String token, String activity, String gameId ) {
		DBCollection coll = mDB.getCollection("questconfig");
		JSONObject result = new JSONObject();
		JSONArray updated = new JSONArray();
		JSONArray completed = new JSONArray();
		JSONObject myActivity = science.db.actions.findAction( activity, gameId );
		if( myActivity.has( "_id") ) {
			Logs.LogEvent( "updating activity " + activity + " for " + token );
			String actionId = myActivity.get( "_id" ).toString();
			try {
				BasicDBObject searchQuery = new BasicDBObject();
				searchQuery.put("gameid", gameId );
				DBCursor results = coll.find(searchQuery);
				while (results.hasNext()) {
					DBObject quest = results.next();
					JSONArray criteria = new JSONArray( quest.get("criteria").toString() );
					for( int i = 0; i < criteria.length(); i ++ ) {
						JSONObject criteriaObject = criteria.getJSONObject(i);
						String criteriaItem = criteriaObject.getString("actionid");
						if( criteriaItem.equals( actionId ) ) {
							JSONObject response = updateQuestForActionForUser(token, quest, actionId );
							if( response.has( "updated") ) {
								updated.put( quest.get("action").toString() );
							}
							else if( response.has( "completed" ) ) {
								completed.put( quest.get("action").toString() );							
								JSONObject questAction = science.db.actions.getActionById( quest.get( "action").toString() );
								science.db.actions.createActivity( token, questAction.getString( "name" ), gameId, new JSONObject() );
								JSONObject quests = science.db.quests.updateQuestsForAction( token, questAction.getString( "name" ), gameId );
								if( quests.has( "updated") ) {
									JSONArray mergeUpdated = quests.getJSONArray( "updated");
									for( int j = 0; j < mergeUpdated.length(); j ++ ) {
										updated.put( mergeUpdated.getString(j) );
									}
								}
								if( quests.has( "completed") ) {
									JSONArray mergeCompleted = quests.getJSONArray( "completed");
									for( int j = 0; j < mergeCompleted.length(); j ++ ) {
										updated.put( mergeCompleted.getString(j) );
									}
								}
								
							}
						}
					}
				}
				result.put( "updated", updated );
				result.put( "completed", completed );
			}
			catch( Exception e ) {
				e.printStackTrace();
			}
		}
		return result;
	}
	
	public String writeQuest( JSONObject configData ) {
		DBCollection coll = mDB.getCollection("quest");
		try {
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put("_id", new ObjectId( configData.getString( "_id" ) ) );
			DBObject result = coll.findOne(searchQuery);
			if( result != null ) {
				for( int i = 0; i < ContainerParams.myQuestFields.length; i ++ ) {
					String field = ContainerParams.myQuestFields[i];
					if( configData.has( field ) ) {
						if( field.equals( "criteria" ) ) {
							result.put( field, configData.getJSONArray( field ).toString() );
						}
						else {
							result.put( field, configData.getString( field ) );
						}
					}
					else {
						return "missing setting for  " + field + " on action " + configData.getString( "_id" );
					}
				}
				coll.save( result );
				Logs.LogEvent( "writing " + result.toString() + " to settings " );
				return null;
			}
		}
		catch( Exception e ) {
			e.printStackTrace();
		}
		return "could not write action " + configData.getString( "_id" );
	}
	
	public JSONArray readQuests( String gameid ) {
		DBCollection coll = mDB.getCollection("questconfig");
		JSONArray quests = new JSONArray();
		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put("gameid", gameid );
		DBCursor results = coll.find(searchQuery);
		while( results.hasNext() ) {
			DBObject result = results.next();
			result.put( "id", result.get("_id").toString() );
			result.removeField( "_id" );
			quests.put( result );
		}
		return quests;
	}
	
	public JSONObject readQuest( String gameid, String name ) {
		DBCollection coll = mDB.getCollection("questconfig");
		JSONObject quest = new JSONObject();
		BasicDBObject searchQuery = new BasicDBObject();
		List<BasicDBObject> obj = new ArrayList<BasicDBObject>();
		obj.add(new BasicDBObject("gameid", gameid));
		obj.add(new BasicDBObject("name", name));
		searchQuery.put("$and", obj);
		DBObject result = coll.findOne(searchQuery);
		if( result != null ) {
			Iterator<String> entries = result.keySet().iterator();
			while( entries.hasNext() ) {
				String entry = entries.next().toString();
				quest.put( entry, result.get( entry ) );
			}			
		}
		else {
			Logs.LogEvent( "not found " + gameid + ", name " + name );
		}
		return quest;
	}
	
	public JSONObject createQuest( String token, JSONObject quest ) {
		JSONObject result = new JSONObject();
		JSONObject existsQuest;
		existsQuest = readQuest( quest.getString( "gameid" ), quest.getString( "name" ) );
		if( existsQuest.keySet().isEmpty() ) {
			for( int i = 0; i < ContainerParams.myQuestFields.length; i ++ ) {
				String field = ContainerParams.myQuestFields[i];
				if( quest.has( field ) == false ) {
					result.put( "missing field", "field" );
					return result;
				}
			}
			DBCollection coll = mDB.getCollection("questconfig");
			BasicDBObject document = new BasicDBObject();
			Iterator<String> entries = quest.keySet().iterator();
			while( entries.hasNext() ) {
				String entry = entries.next().toString();
				if( entry.equals( "_t" ) ) {
					// swallow this
				}
				else if( entry.equals( "criteria" ) ) {
					document.put( entry, quest.get( entry ).toString() );
				}
				else {
					document.put( entry, quest.get( entry ) );
				}
			}
			coll.insert( document );
			JSONObject gameActivity = readQuest( quest.getString( "gameid"), quest.getString( "name") );
			if( gameActivity.has( "_id" ) ) {
				result.put( "id", gameActivity.get("_id").toString() );
			}
		}
		return result;
	}

	public String writeQuestSettings( JSONObject configData ) {
		DBCollection coll = mDB.getCollection("questconfig");
		try {
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put("_id", new ObjectId( configData.getString( "_id" ) ) );
			DBObject result = coll.findOne(searchQuery);
			if( result != null ) {
				for( int i = 0; i < ContainerParams.myQuestFields.length; i ++ ) {
					String field = ContainerParams.myQuestFields[i];
					if( configData.has( field ) ) {
						if( field.equals( "criteria" ) ) {
							result.put( field, configData.getJSONArray( field ).toString() );
						}
						else {
							result.put( field, configData.getString( field ) );
						}
					}
					else {
						return "missing setting for  " + field + " on quiest " + configData.getString( "_id" );
					}
				}
				coll.save( result );
				Logs.LogEvent( "writing " + result.toString() + " to settings " );
				return null;
			}
		}
		catch( Exception e ) {
			e.printStackTrace();
		}
		return "could not write quest " + configData.getString( "_id" );
	}

	public JSONObject readQuestSettings( JSONObject configData ) {
		JSONObject replyData = new JSONObject();
		DBCollection coll = mDB.getCollection("questconfig");
		try {
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put("_id", new ObjectId( configData.getString( "_id" ) ) );
			DBObject result = coll.findOne(searchQuery);
			if( result != null ) {
				Iterator<String> entries = result.keySet().iterator();
				while( entries.hasNext() ) {
					String entry = entries.next().toString();
					if( entry.equals( "_id" ) == false ) {
						replyData.put( entry, result.get( entry ) );
					}
					else {
						replyData.put( "_id", result.get( entry ).toString() );
					}
				}
			}
		}
		catch( Exception e ) {
			e.printStackTrace();
		}
		return replyData;
	}	
}
