import org.json.JSONObject;
import org.vertx.java.core.buffer.Buffer;
import org.vertx.java.core.http.HttpServerRequest;

public class MessageSGLAccountCreate {
	public MessageSGLAccountCreate( HttpServerRequest req, Buffer body )  {
		Logs.LogEvent("Create Web User" );
		String errorInfo = null;
		try { 
			JSONObject jsonRequest = new JSONObject( body.toString() ); 
			JSONObject jsonReplyBody = new JSONObject();
			JSONObject jsonRequestBody = jsonRequest.getJSONObject( "body" );
			if( jsonRequestBody.has( "_t") && jsonRequestBody.getString( "_t" ).equals( "sglaccount.create" ) ) {
				String timeStamp = "";
				String accountId = "";
				String accountAlias = "";
				String accountPassword = "";
				String accountDOB = "1-1-1998";
				String accountNewsLetter = "1";
				String developerId = "";
				String developerSecret = "";
				if( jsonRequestBody.has("time") ) {
					timeStamp = jsonRequestBody.getString( "time" );
				}
				if( jsonRequestBody.has("accountdob") ) {
					accountDOB = jsonRequestBody.getString( "accountdob" );
				}
				if( jsonRequestBody.has("accountnewsletter") ) {
					accountNewsLetter = jsonRequestBody.getString( "accountnewsletter" );
				}
				if( jsonRequestBody.has("accountid") ) {
					accountId = jsonRequestBody.getString( "accountid" );
				}
				else {
					errorInfo = "Missing account id";					
				}
				if( jsonRequestBody.has("accountAlias") ) {
					accountAlias = jsonRequestBody.getString( "accountAlias" );
				}
				else {
					errorInfo = "Missing account alias";					
				}
				if( jsonRequestBody.has("accountpassword") ) {
					accountPassword = jsonRequestBody.getString( "accountpassword" );
				}
				else {
					errorInfo = "Missing account password";					
				}
				if( jsonRequestBody.has("developerid") ) {
					developerId = jsonRequestBody.getString( "developerid" );
				}
				else {
					errorInfo = "Missing developer id";					
				}
				if( jsonRequestBody.has("developersecret") ) {
					developerSecret = jsonRequestBody.getString( "developersecret" );
				}
				else {
					errorInfo = "Missing developer id";					
				}
				if( errorInfo == null && accountId != null && accountId.length() > 0 && accountAlias.length() > 0 ) {
					if( science.db.validateDeveloperSecret( developerId, developerSecret ) ) {
						if( science.db.accounts.accountExists( accountId ) == false ) {
							String accountToken = science.db.accounts.createAccountToken( accountId, accountPassword, accountAlias, accountDOB, accountNewsLetter );
							String[] properties = { "accountid", "accountalias", "accountdob", "accountnewsletter" };
							science.analytics.trackEvent( accountToken, "New Web Account", timeStamp, FieldMapper.getFields(jsonRequestBody, properties) );
							String[] profilefields = new String[2];
							profilefields[0] = "name"; 
							profilefields[1] = accountToken;
							science.analytics.profileUpdate( accountToken, profilefields );
							if( accountToken != null && accountToken.length() > 0 ) {
								jsonReplyBody.put( "_t", "sglaccount.created" );
								jsonReplyBody.put( "accounttoken", accountToken );
								jsonReplyBody.put("privs", "user");
							}
							else {
								errorInfo = "DB Failure";
							}
						}
						else {
							errorInfo = "account exists";
						}						
					}
					else {
						errorInfo = "verify your developer credentials";
					}
				}
				else {
					if( errorInfo == null ) {
						errorInfo = "account missing";
					}
				}
			}
			else {
				errorInfo = "Missing _t";
			}
			if( errorInfo != null && errorInfo.length() > 0 ) {
				req.response().setStatusCode(400);
				Logs.LogEvent( errorInfo );
				jsonReplyBody.put( "_t", "sglaccount.create_error" );
				jsonReplyBody.put( "info", errorInfo );
			}
			JSONObject jsonReply = new JSONObject().put( "_t", "msg" );
			JSONObject jsonHeader = jsonRequest.getJSONObject( "header" );
			jsonReply.put( "header", jsonHeader );
			jsonReply.put( "body", jsonReplyBody );
			Logs.LogEvent( "JSON Reply is " + jsonReply.toString() );
			req.response().end(jsonReply.toString());
		}
		catch( org.json.JSONException e ) {
			e.printStackTrace();
			Logs.LogEvent( "JSON error is " + errorInfo );
			req.response().setStatusCode(400);
			req.response().end( "{ jsonfailure: true }");
		}
	}

}
