import org.json.JSONObject;
import org.vertx.java.core.buffer.Buffer;
import org.vertx.java.core.http.HttpServerRequest;

public class MessageUserCreateAnon {
	public MessageUserCreateAnon( HttpServerRequest req, Buffer body )  {
		Logs.LogEvent("Create Anon User" );
		String errorInfo = null;
		try { 
			JSONObject jsonRequest = new JSONObject( body.toString() ); 
			JSONObject jsonReplyBody = new JSONObject();
			JSONObject jsonRequestBody = jsonRequest.getJSONObject( "body" );
			if( jsonRequestBody.has( "_t") && jsonRequestBody.getString( "_t" ).equals( "anonuser.create" ) ) {
				String timeStamp = "";
				if( jsonRequestBody.has("time") ) {
					timeStamp = jsonRequestBody.getString( "time" );
				}
				if( errorInfo == null ) {
					String accountToken = science.db.accounts.createAnonAccountToken();
					String[] properties = { "accountToken" };
					jsonRequestBody.put( "accountToken",  accountToken );
					science.analytics.trackEvent( accountToken, "New Anon Account", timeStamp, FieldMapper.getFields(jsonRequestBody, properties) );
					if( accountToken != null && accountToken.length() > 0 ) {
						jsonReplyBody.put( "_t", "anonuser.created" );
						jsonReplyBody.put( "accounttoken", accountToken );
						jsonReplyBody.put("privs", "anon");
					}
					else {
						errorInfo = "DB Failure";
					}
				}
			}
			else {
				errorInfo = "Missing _t";
			}
			if( errorInfo != null && errorInfo.length() > 0 ) {
				req.response().setStatusCode(400);
				Logs.LogEvent( errorInfo );
				jsonReplyBody.put( "_t", "anonuser.create_error" );
				jsonReplyBody.put( "info", errorInfo );
			}
			JSONObject jsonReply = new JSONObject().put( "_t", "msg" );
			JSONObject jsonHeader = jsonRequest.getJSONObject( "header" );
			jsonReply.put( "header", jsonHeader );
			jsonReply.put( "body", jsonReplyBody );
			Logs.LogEvent( "JSON Reply is " + jsonReply.toString() );
			req.response().end(jsonReply.toString());
		}
		catch( org.json.JSONException e ) {
			e.printStackTrace();
			Logs.LogEvent( "JSON error is " + errorInfo );
			req.response().setStatusCode(400);
			req.response().end( "{ jsonfailure: true }");
		}
	}
}
