import org.apache.commons.codec.binary.Base64;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.io.*;

public class ContainerAnalytics {
	public static String myToken;
	public static int myCount = 0;
	public static int myPercentage = 100;
	public ContainerAnalytics() {
	}

	public void initialize()  {
		myPercentage = Integer.parseInt( ContainerConfig.getProperty( "mixpercent" ) );
		myToken = ContainerConfig.getProperty( "mixtoken" );
		Logs.LogEvent("Initializing Mixpanel token " + myToken );
	}

	public void trackEvent( String myId, String myMessage, String myTimeStamp, String[] events )
	{
		if( myCount < myPercentage && myToken.length() > 0 ) {
			String encodedMessage;
			long currentTime = (System.currentTimeMillis()/1000L);
			if( events == null ) {
				encodedMessage = "{\"event\": \"" + myMessage + "\",\"properties\": {\"distinct_id\": \"" + myId + "\",\"token\": \"" + myToken + "\",\"time\": \"" + currentTime + "\" }}";
			}
			else {
				String myEvents = "";
				if( events.length > 0 ) {
					 myEvents = ", ";
					 for( int i = 0; i < events.length ; i += 2 ) {
						myEvents += "\"" + events[i] + "\": \"" + events[i+1] + "\"";
						if( i+2 < events.length) {
							myEvents += ",";
						}
					}
				}
				encodedMessage = "{\"event\": \"" + myMessage + "\",\"properties\": {\"distinct_id\": \"" + myId + "\",\"token\": \"" + myToken + "\",\"time\": \"" + currentTime + "\" " + myEvents + " }}";			
			}
			if( sendMessage( "http://api.mixpanel.com/track/?data=", encodedMessage ) == false ) {
				Logs.LogEvent( "Error in analytics call! " + myId + " -> " + myMessage + ":" + encodedMessage );		
			}
		}
		myCount++;
		if( myCount > 99 ) {
			myCount = 0;
		}
	}
	
	public boolean sendMessage( String myUrl, String message ) {
		Base64 myBase64 = new Base64();
		URL url;
		boolean result = true;

		String convertedMessage = myBase64.encodeToString(message.toString().getBytes());
		try {
			url = new URL( myUrl + convertedMessage );
			InputStream is = url.openStream();
			int i;
			char c;
	  		try {
				 while((i=is.read())!=-1)
				 {
					// converts integer to character
					c=(char)i;
					if( c != '1' ) {
						if( result == true ) {
							Logs.LogEvent( "Error in analytics call! Encoded = " + convertedMessage );
						}
						result = false;
						Logs.LogEvent( "" + c );
					}
				 }
			 } finally {
			  is.close();
			}		
		} catch ( Exception e ) {
			Logs.LogEvent( "Exception! " + e.toString() );
			e.printStackTrace();
		}
		return result;
	}
	
	public void profileTransaction( String myId, String amount ) {
		String encodedMessage;
		Date javaUtilDate= new Date();  
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");  
		String timeString = formatter.format(javaUtilDate);
		try {
			float transactionAmount = Float.parseFloat( amount );
			String appendData = " \"$append\": { \"$transactions\": { \"$time\" :\""+ timeString + "\", \"$amount\":" + transactionAmount + " } }";
			encodedMessage = "{ \"$distinct_id\": \"" + myId + "\",\"$token\": \"" + myToken + "\", " + appendData + " }";
			if( sendMessage( "http://api.mixpanel.com/engage/?data=", encodedMessage ) == false ) {
				Logs.LogEvent( "Error in analytics call! " + myId + " ->  profileAppend:" + encodedMessage );		
			}
		}
		catch( Exception e ) {
			e.printStackTrace();			
		}
	}
	
	public void profileAppend( String myId, String[] events) {
		String encodedMessage;

		if( events != null ) {
			String appendData = " \"$append\": { ";
			for( int i = 0; i < events.length ; i += 2 ) {
				appendData += "\"" + events[i] + "\": \"" + events[i+1] + "\"";
				if( i+2 < events.length) {
					appendData += ",";
				}
			}
			appendData += "}";
			encodedMessage = "{ \"$distinct_id\": \"" + myId + "\",\"$token\": \"" + myToken + "\", " + appendData + " }";
			if( sendMessage( "http://api.mixpanel.com/engage/?data=", encodedMessage ) == false ) {
				Logs.LogEvent( "Error in analytics call! " + myId + " ->  profileAppend:" + encodedMessage );		
			}
		}
	}
	
	public void profileUpdate( String myId, String[] events) {
		String encodedMessage;

		if( events != null ) {
			String setData = " \"$set\": { ";
			for( int i = 0; i < events.length ; i += 2 ) {
				setData += "\"" + events[i] + "\": \"" + events[i+1] + "\"";
				if( i+2 < events.length) {
					setData += ",";
				}
			}
			setData += "}";
			encodedMessage = "{ \"$distinct_id\": \"" + myId + "\",\"$token\": \"" + myToken + "\", " + setData + " }";
			if( sendMessage( "http://api.mixpanel.com/engage/?&data=", encodedMessage ) == false ) {
				Logs.LogEvent( "Error in analytics call! " + myId + " ->  profileUpdate:" + encodedMessage );		
			}
		}
	}
}
