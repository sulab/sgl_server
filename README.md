# README #

This is the readme for the Science Game Lab assets repository!  

Installation instructions

*******************************
* Common application elements *
*******************************

you may have to add "sudo" in front of everything

apt-get update
apt-get install git
apt-get install mongodb
apt-get install unzip
apt-add-repository ppa:webupd8team/java
apt-get update
apt-get install oracle-java8-installer
apt-get install php5-curl
service apache2 restart

cd /usr/local
mkdir vertx
cd vertx
wget http://dl.bintray.com/vertx/downloads/vert.x-2.1M2.zip
unzip vert.x-2.1M2.zip


sudo vi /etc/profile

add these lines:
    # add VertX to the generic path
    export PATH=/usr/local/vertx/vert.x-2.1M2/bin:$PATH

*******************************
* Adding the app from a repo  *
*******************************

mkdir /var/apps/sci
cd /var/apps/sci

sudo git clone https://username@bitbucket.org/sulab/sgl_server.git


cd /usr/local/vertx/vert.x-2.1M2/lib

sudo cp /var/apps/sci/sgl_server/vertx/lib/commons-codec-1.9.jar .
sudo cp /var/apps/sci/sgl_server/vertx/lib/mongo-2.10.1.jar .

cd /var/apps/sci/sgl_server/vertx/src

vertx run science.java &

*******************************
* Server reboot               *
*******************************

sudo reboot

(wait for reboot)

cd /var/apps/sci/sgl_server/vertx/src
vertx run science.java &
sudo ntpd

*******************************
* Starting/Stopping vertx     *
*******************************

to restart the server you need to find the instance and kill it.


you will need to do

ps -ef | grep vertx

then

kill -9 [pid]

followed by doing
cd /var/apps/sci/sgl_server/vertx/src

vertx run science.java &


*******************************
* installing apache2 for http *
*******************************

sudo apt-get install php5 libapache2-mod-php5 php5-mcrypt
sudo apt-get install apache2 dhcpd
sudo service apache2 start

*******************************
* update web from repo        *
*******************************

cd /var/www

sudo git clone https://username@bitbucket.org/sulab/sgl_web.git html

after making the first account on the system via console, it has to be hand tagged as admin

from inside the mongo shell:

db.accounts.update( { "accountId" : "test1" }, { $set: { "privs":"admin" } } );

updating all fields on an item:

db.actionconfig.update({},{$set : {"active":"1"}},false,true)

*******************************
* updating ssl keys           *
*******************************
Installing ssl into apache2:

sudo a2enmod ssl
sudo a2ensite default-ssl
sudo /etc/init.d/apache2 restart

the keys will need to be regenerated every year and placed into the server at:

/etc/apache2/ssl/crt 

there is a domain key file there domain.key

these files are referenced in the apache2 server:

/etc/apache2/sites-available/000-default.conf

And the relevant fields are:
        SSLCertificateFile /etc/apache2/ssl/crt/domain.crt
        SSLCertificateKeyFile /etc/apache2/ssl/crt/domain.key
        SSLCertificateChainFile /etc/apache2/ssl/crt/intermediate.crt
        SSLEngine on
        SSLProtocol all


the real problem is that we need to create a separate keytool based ssl key for java's server

and that you cannot easily import a .key file into keytool

so in order for this to work:

sudo keytool -importkeystore -deststorepass science -destkeypass science -destkeystore keystore.jks -srckeystore keystore.p12 -srcstoretype PKCS12 -srcstorepass science -alias domain

then this file needs to be copied into /var/apps/sci/sgl_server/vertx/src

and then the server rebooted

The SSL in the mail sender class needs to support ssl also:

whereis java
ls -l /usr/bin/java

gives a symbolic dir to

ls -l /etc/alternatives/java

and we need something in the jre lib cert directory

/usr/lib/jvm/java-8-oracle/jre/lib/security

sudo keytool -import -trustcacerts -file $HOME/sciencegamelab_server_cert.txt -alias CompanyCert -keystore cacerts

SSL Redirects need to be added:

<VirtualHost *:80>
    ServerName www.sciencegamelab.org
    Redirect "/" "https://www.sciencegamelab.org/"
</VirtualHost>

<VirtualHost *:443>
 # rest of the file is as is, with the SLL tags uncommented

 8/22/2016

 mongo db update:
 
 mongo
 use sciencdb;
 db.userconfig.update({}, {$rename:{"username":"accountAlias"}}, false, true);
